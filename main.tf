
provider "velocloud" {
  host     = "vco22-fra1.velocloud.net"
  username = ""
  password = ""
  operator = false
}

data "velocloud_enterprise" "tf_customer" {
  name     = "PenTest"
  username = ""
  password = ""
  email    = ""
}

data "velocloud_configuration" "tf_config" {
  name         = "tf_profile2"
  enterpriseid = 123
}

resource "velocloud_qos_rule" "tf_qos_rule" {
  enterpriseid    = 123
  configurationid = "${data.velocloud_configuration.tf_config.id}"
  qosmodule       = "${data.velocloud_configuration.tf_config.qosmodule}"

  rule {
    name            = "My Video App"
    dip             = "192.168.1.0"
    dmask           = "255.255.255.0"
    dport           = 1234
    proto           = 17
    bandwidthpct    = 15
    class           = "realtime"
  }

  rule {
    name            = "My Web App"
    dip             = "192.168.1.2"
    dmask           = "255.255.255.255"
    dport           = 80
    proto           = 6
    #bandwidthpct    = 10
    class           = "transactional"
    servicegroup    = "ALL"
  }

  rule {
    name            = "My test App"
    class           = "bulk"
  }
  
}

resource "velocloud_fw_rule" "tf_fw_rules" { 
  enterpriseid    = 123
  configurationid = "${data.velocloud_configuration.tf_config.id}"
  fwmodule        = "${data.velocloud_configuration.tf_config.firewall}"
  enabled         = false
  logging         = true
  
  rule {
    name            = "tf_rule"
    dip             = "192.168.1.0"
    dmask           = "255.255.255.0"
    dport           = 80
    proto           = 6
    action          = "allow"
  }
  
  rule {
    name            = "tf_rule2"
    sip             = "192.168.1.2"
    smask           = "255.255.255.255"
    dport           = 80
    proto           = 6
    action          = "allow"
  }
  
#  rule {
#    name            = "tf_rule3"
#    dip             = "192.168.1.3"
#    dmask           = "255.255.255.255"
#    dport           = 80
#    proto           = 6
#    action          = "deny"
#  }
}

#resource "velocloud_edge" "tf_edge" {
#  name            = "tf_edge"
#  model           = "virtual"
#  contact         = "adeleporte"
#  email           = "adeleporte@vmware.com"
#  enterpriseid    = "${velocloud_enterprise.tf_customer.id}"
#  configurationid = "${velocloud_configuration.tf_configuration.id}"
#}

#resource "velocloud_device_settings" "tf_device" {
#  enterpriseid    = "${velocloud_enterprise.tf_customer.id}"
#  configurationid = "${velocloud_configuration.tf_configuration.id}"
#  moduleid        = "${velocloud_edge.tf_edge.devicemodule}"
#  lan_cidr         = "192.168.1.1"
#  lan_prefix       = 24
#}

//resource "velocloud_edge" "tf_edge2" {
//  count           = 25
//  name            = "tf_edgeT_${count.index}"
//  model           = "virtual"
//  contact         = "adeleporte"
//  email           = "adeleporte@vmware.com"
//  enterpriseid    = "${velocloud_enterprise.tf_customer.id}"
//  configurationid = "${velocloud_configuration.tf_configuration.id}"
//}

//resource "velocloud_device_settings" "tf_device2" {
//  count           = 25
//  enterpriseid    = "${velocloud_enterprise.tf_customer.id}"
//  configurationid = "${velocloud_configuration.tf_configuration.id}"
//  moduleid        = "${element(velocloud_edge.tf_edge.*.devicemodule, count.index)}"
//  lan_cidr         = "192.168.${count.index}.1"
//  lan_prefix       = 24
//}


#output "global_fw_module_id" {
#  value = "${velocloud_configuration.tf_configuration.firewall}"
#}

#output "global_qos_module_id" {
#  value = "${velocloud_configuration.tf_configuration.qosmodule}"
#}

#output "specific_device_module_id" {
#  value = "${velocloud_edge.tf_edge.devicemodule}"
#}

#output "enterprise_id" {
#  value = "${velocloud_enterprise.tf_customer.id}"
#}

#output "configuration_id" {
#  value = "${data.velocloud_configuration.tf_config.id}"
#}

#output "activation_key" {
#  value = ["${velocloud_edge.tf_edge.*.activationkey}"]
#}