package velocloud

import (
	"fmt"
	"log"
	"strconv"

	"github.com/hashicorp/terraform/helper/schema"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

func dataSourceConfiguration() *schema.Resource {
	return &schema.Resource{
		Read: dataSourceConfigurationRead,

		Schema: map[string]*schema.Schema{
			"enterpriseid": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"devicesettings": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"firewall": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"qosmodule": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"wan": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
		},
	}
}

func dataSourceConfigurationRead(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************READ*****************************************************************")

	client := m.(*velocloud.APIClient)

	id, err := strconv.Atoi(d.Get("enterpriseid").(string))
	if err != nil {
		return fmt.Errorf("Error obtaining Enterprise id")
	}

	body := velocloud.EnterpriseGetEnterpriseConfigurations{
		EnterpriseId: int32(id),
		With:         []string{"modules"},
	}

	results, _, err := client.EnterpriseApi.EnterpriseGetEnterpriseConfigurations(context.Background(), body)

	index := -1
	for i, v := range results {
		if v.Name == d.Get("name") {
			index = i
			break
		}
	}

	if index == -1 {
		log.Println("******************************Configuration is not found**********************************************")
		d.SetId("")
		return nil
	}

	d.SetId(fmt.Sprint(results[index].Id))
	d.Set("devicesettings", results[index].Modules[0].Id)
	d.Set("firewall", results[index].Modules[1].Id)
	d.Set("qosmodule", results[index].Modules[2].Id)
	d.Set("wan", results[index].Modules[3].Id)

	return nil
}
