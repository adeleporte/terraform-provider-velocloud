package velocloud

import (
	"fmt"
	"log"

	//"strconv"

	"github.com/hashicorp/terraform/helper/schema"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

func dataSourceEnterprise() *schema.Resource {
	return &schema.Resource{
		Read: dataSourceEnterpriseRead,

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"username": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"password": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"email": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
		},
	}
}

func dataSourceEnterpriseRead(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************READ*****************************************************************")

	client := m.(*velocloud.APIClient)

	body := velocloud.NetworkGetNetworkEnterprises{
		NetworkId: 1,
	}

	results, _, _ := client.NetworkApi.NetworkGetNetworkEnterprises(context.Background(), body)

	for _, v := range results {
		if v.Name == d.Get("name").(string) {
			d.SetId(fmt.Sprint(v.Id))
			return nil
		}
	}

	d.SetId("")
	return nil
}
