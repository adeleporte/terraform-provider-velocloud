package velocloud

import (
	"fmt"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/terraform"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

// Provider Terraform
func Provider() terraform.ResourceProvider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"host": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("VCO_HOST", nil),
			},
			"username": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("VCO_USERNAME", nil),
			},
			"password": &schema.Schema{
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("VCO_PASSWORD", nil),
			},
			"operator": &schema.Schema{
				Type:     schema.TypeBool,
				Optional: true,
				Default:  true,
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"velocloud_enterprise":      resourceEnterprise(),
			"velocloud_configuration":   resourceConfiguration(),
			"velocloud_qos_rule":        resourceQoSRule(),
			"velocloud_fw_rule":         resourceFirewallRule(),
			"velocloud_device_settings": resourceDeviceSettings(),
			"velocloud_edge":            resourceEdge(),
			"velocloud_property":        resourceProperty(),
			"velocloud_gateway":         resourceGateway(),
		},
		DataSourcesMap: map[string]*schema.Resource{
			"velocloud_enterprise":    dataSourceEnterprise(),
			"velocloud_configuration": dataSourceConfiguration(),
		},

		ConfigureFunc: providerConfigure,
	}
}

func providerConfigure(d *schema.ResourceData) (interface{}, error) {
	host := d.Get("host").(string)
	username := d.Get("username").(string)
	password := d.Get("password").(string)

	cfg := velocloud.Configuration{
		BasePath:  "https://" + host + "/portal/rest",
		Host:      host,
		Scheme:    "https",
		UserAgent: "terraform-provider-velocloud/1.0",
	}

	client := velocloud.NewAPIClient(&cfg)

	auth := velocloud.AuthObject{
		Username: username,
		Password: password,
	}

	// Connect and Get Cookie
	if d.Get("operator") == true {
		resp, err := client.LoginApi.LoginOperatorLogin(context.Background(), auth)
		if err != nil {
			return fmt.Println("Velocloud provider connectivity check failed")
		}

		//		TODO : Detect Login Error
		//		log.Println(resp)
		//		return nil, fmt.Errorf("Operator Login Failed")

		client.AddCookie(string(resp.Cookies()[1].Raw))

	} else {
		resp, err := client.LoginApi.LoginEnterpriseLogin(context.Background(), auth)
		if err != nil {
			return fmt.Println("Velocloud provider connectivity check failed")
		}

		//		log.Println(resp)
		client.AddCookie(string(resp.Cookies()[1].Raw))
	}

	return client, nil
}
