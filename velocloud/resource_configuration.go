package velocloud

import (
	"fmt"
	"log"
	"strconv"

	"github.com/hashicorp/terraform/helper/schema"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

func resourceConfiguration() *schema.Resource {
	return &schema.Resource{
		Create: resourceConfigurationCreate,
		Read:   resourceConfigurationRead,
		Update: resourceConfigurationUpdate,
		Delete: resourceConfigurationDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"enterpriseid": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"devicesettings": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"firewall": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"qosmodule": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"wan": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
		},
	}
}

func resourceConfigurationCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************CREATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	id, err := strconv.Atoi(d.Get("enterpriseid").(string))
	if err != nil {
		return fmt.Errorf("Error obtaining Enterprise id")
	}

	body := velocloud.ConfigurationCloneEnterpriseTemplate{
		EnterpriseId: int32(id),
		Name:         d.Get("name").(string),
	}
	client.ConfigurationApi.ConfigurationCloneEnterpriseTemplate(context.Background(), body)

	return resourceConfigurationRead(d, m)
}

func resourceConfigurationRead(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************READ*****************************************************************")

	client := m.(*velocloud.APIClient)

	id, err := strconv.Atoi(d.Get("enterpriseid").(string))
	if err != nil {
		return fmt.Errorf("Error obtaining Enterprise id")
	}

	ege := velocloud.EnterpriseGetEnterpriseConfigurations{
		EnterpriseId: int32(id),
		With:         []string{"modules"},
	}

	result, _, err := client.EnterpriseApi.EnterpriseGetEnterpriseConfigurations(context.Background(), ege)

	index := -1
	for i, v := range result {
		log.Println(v.Name)
		log.Println(d.Get("name"))
		if v.Name == d.Get("name") {
			index = i
			break
		}
	}

	if index == -1 {
		log.Println("******************************Configuration is not found**********************************************")
		d.SetId("")
		return nil
	}

	d.SetId(fmt.Sprint(result[index].Id))
	d.Set("devicesettings", result[index].Modules[0].Id)
	d.Set("firewall", result[index].Modules[1].Id)
	d.Set("qosmodule", result[index].Modules[2].Id)
	d.Set("wan", result[index].Modules[3].Id)

	return nil
}

func resourceConfigurationUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************UPDATE*****************************************************************")

	return resourceConfigurationRead(d, m)
}

func resourceConfigurationDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************DELETE*****************************************************************")
	client := m.(*velocloud.APIClient)

	id, err := strconv.Atoi(d.Id())
	log.Println(fmt.Sprint(id))
	if err != nil {
		return fmt.Errorf("Error obtaining Configuration id")
	}

	body := velocloud.ConfigurationDeleteConfiguration{
		Id: int32(id),
	}

	_, _, err = client.ConfigurationApi.ConfigurationDeleteConfiguration(context.Background(), body)

	log.Println(fmt.Sprint(id))
	if err != nil {
		return fmt.Errorf("Error deleting configuration")
	}

	return nil
}
