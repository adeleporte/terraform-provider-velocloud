package velocloud

import (
	"fmt"
	"log"


	"github.com/hashicorp/terraform/helper/schema"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

func resourceDeviceSettings() *schema.Resource {
	return &schema.Resource{
		Create: resourceDeviceSettingsCreate,
		Read:   resourceDeviceSettingsRead,
		Update: resourceDeviceSettingsUpdate,
		Delete: resourceDeviceSettingsDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"enterpriseid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"moduleid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"configurationid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"lan_cidr": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default:  "1.1.1.1",
			},
			"lan_prefix": &schema.Schema{
				Type:     schema.TypeInt,
				Optional: true,
				Default:  24,
			},
		},
	}
}

func resourceDeviceSettingsCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************DEVICE CREATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	enterpriseid := d.Get("enterpriseid").(int)
	configurationid := d.Get("configurationid").(int)
	moduleid := d.Get("moduleid").(int)

	log.Println(fmt.Sprint(enterpriseid))
	log.Println(fmt.Sprint(configurationid))
	log.Println(fmt.Sprint(moduleid))

	netflowcollector := velocloud.EdgeDeviceSettingsDataNetflowCollectors{
		Address: "172.18.28.1",
		Port:    4739,
	}
	netflowcollectors := make([]velocloud.EdgeDeviceSettingsDataNetflowCollectors, 0)
	netflowcollectors = append(netflowcollectors, netflowcollector)

	network := velocloud.EdgeDeviceSettingsDataLanNetworks{
		Advertise: true,
		Cost:      10,
		Dhcp: &velocloud.EdgeDeviceSettingsDataLanDhcp{
			Enabled:          true,
			LeaseTimeSeconds: 86400,
		},
		StaticReserved: 10,
		Netmask:        "255.255.255.0",
		CidrPrefix:     int32(d.Get("lan_prefix").(int)),
		CidrIp:         d.Get("lan_cidr").(string),
		BaseDhcpAddr:   13,
		NumDhcpAddr:    87,
		Name:           "Corporate",
		Interfaces:     []string{"GE1"},
		VlanId:         1,
		Disabled:       false,
		SegmentId:      0,
		Ospf: velocloud.EdgeOspf{
			Area:             "",
			Enabled:          false,
			PassiveInterface: true,
		},
	}
	networks := make([]velocloud.EdgeDeviceSettingsDataLanNetworks, 0)
	networks = append(networks, network)

	ge2 := velocloud.EdgeDeviceSettingsDataRoutedInterfaces{
		Name:      "GE2",
		SegmentId: -1,
		Disabled:  false,
		Addressing: &velocloud.EdgeDeviceSettingsDataAddressing{
			Type_: "DHCP",
		},
		EncryptOverlay: true,
		L2: &velocloud.EdgeDeviceSettingsDataL2{
			Autonegotiation: true,
			Speed:           "100M",
			Duplex:          "FULL",
			MTU:             1500,
		},
		NatDirect:  true,
		WanOverlay: "AUTO_DISCOVERED",
		Override:   true,
		Ospf: &velocloud.EdgeDeviceSettingsDataOspf{
			MTU:            1380,
			Area:           0,
			AuthId:         0,
			AuthPassphrase: "",
			Authentication: false,
			Cost:           1,
			DeadTimer:      40,
			Enabled:        false,
			HelloTimer:     10,
			InboundRouteLearning: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "LEARN",
				Filters:       nil,
			},
			Md5Authentication: false,
			OutboundRouteAdvertisement: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "IGNORE",
				Filters:       nil,
			},
			Passive: false,
		},
	}

	ge3 := velocloud.EdgeDeviceSettingsDataRoutedInterfaces{
		Name:      "GE3",
		SegmentId: -1,
		Disabled:  false,
		Addressing: &velocloud.EdgeDeviceSettingsDataAddressing{
			Type_:      "STATIC",
			CidrIp:     "172.31.1.100",
			CidrPrefix: 24,
			Netmask:    "255.255.255.0",
		},
		EncryptOverlay: true,
		L2: &velocloud.EdgeDeviceSettingsDataL2{
			Autonegotiation: true,
			Speed:           "100M",
			Duplex:          "FULL",
			MTU:             1500,
		},
		NatDirect:  false,
		WanOverlay: "DISABLED",
		Override:   true,
		Advertise:  true,
		Ospf: &velocloud.EdgeDeviceSettingsDataOspf{
			MTU:            1380,
			Area:           0,
			AuthId:         0,
			AuthPassphrase: "",
			Authentication: false,
			Cost:           1,
			DeadTimer:      40,
			Enabled:        false,
			HelloTimer:     10,
			InboundRouteLearning: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "LEARN",
				Filters:       nil,
			},
			Md5Authentication: false,
			OutboundRouteAdvertisement: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "IGNORE",
				Filters:       nil,
			},
			Passive: false,
		},
	}
	ge4 := velocloud.EdgeDeviceSettingsDataRoutedInterfaces{
		Name:      "GE4",
		SegmentId: -1,
		Disabled:  false,
		Addressing: &velocloud.EdgeDeviceSettingsDataAddressing{
			Type_: "DHCP",
		},
		EncryptOverlay: true,
		L2: &velocloud.EdgeDeviceSettingsDataL2{
			Autonegotiation: true,
			Speed:           "100M",
			Duplex:          "FULL",
			MTU:             1500,
		},
		NatDirect:  true,
		WanOverlay: "AUTO_DISCOVERED",
		Ospf: &velocloud.EdgeDeviceSettingsDataOspf{
			MTU:            1380,
			Area:           0,
			AuthId:         0,
			AuthPassphrase: "",
			Authentication: false,
			Cost:           1,
			DeadTimer:      40,
			Enabled:        false,
			HelloTimer:     10,
			InboundRouteLearning: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "LEARN",
				Filters:       nil,
			},
			Md5Authentication: false,
			OutboundRouteAdvertisement: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "IGNORE",
				Filters:       nil,
			},
			Passive: false,
		},
	}
	ge5 := velocloud.EdgeDeviceSettingsDataRoutedInterfaces{
		Name:      "GE5",
		SegmentId: -1,
		Disabled:  false,
		Addressing: &velocloud.EdgeDeviceSettingsDataAddressing{
			Type_: "DHCP",
		},
		EncryptOverlay: true,
		L2: &velocloud.EdgeDeviceSettingsDataL2{
			Autonegotiation: true,
			Speed:           "100M",
			Duplex:          "FULL",
			MTU:             1500,
		},
		NatDirect:  true,
		WanOverlay: "AUTO_DISCOVERED",
		Ospf: &velocloud.EdgeDeviceSettingsDataOspf{
			MTU:            1380,
			Area:           0,
			AuthId:         0,
			AuthPassphrase: "",
			Authentication: false,
			Cost:           1,
			DeadTimer:      40,
			Enabled:        false,
			HelloTimer:     10,
			InboundRouteLearning: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "LEARN",
				Filters:       nil,
			},
			Md5Authentication: false,
			OutboundRouteAdvertisement: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "IGNORE",
				Filters:       nil,
			},
			Passive: false,
		},
	}
	ge6 := velocloud.EdgeDeviceSettingsDataRoutedInterfaces{
		Name:      "GE6",
		SegmentId: -1,
		Disabled:  false,
		Addressing: &velocloud.EdgeDeviceSettingsDataAddressing{
			Type_: "DHCP",
		},
		EncryptOverlay: true,
		L2: &velocloud.EdgeDeviceSettingsDataL2{
			Autonegotiation: true,
			Speed:           "100M",
			Duplex:          "FULL",
			MTU:             1500,
		},
		NatDirect:  true,
		WanOverlay: "AUTO_DISCOVERED",
		Ospf: &velocloud.EdgeDeviceSettingsDataOspf{
			MTU:            1380,
			Area:           0,
			AuthId:         0,
			AuthPassphrase: "",
			Authentication: false,
			Cost:           1,
			DeadTimer:      40,
			Enabled:        false,
			HelloTimer:     10,
			InboundRouteLearning: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "LEARN",
				Filters:       nil,
			},
			Md5Authentication: false,
			OutboundRouteAdvertisement: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "IGNORE",
				Filters:       nil,
			},
			Passive: false,
		},
	}
	ge7 := velocloud.EdgeDeviceSettingsDataRoutedInterfaces{
		Name:      "GE7",
		SegmentId: -1,
		Disabled:  false,
		Addressing: &velocloud.EdgeDeviceSettingsDataAddressing{
			Type_: "DHCP",
		},
		EncryptOverlay: true,
		L2: &velocloud.EdgeDeviceSettingsDataL2{
			Autonegotiation: true,
			Speed:           "100M",
			Duplex:          "FULL",
			MTU:             1500,
		},
		NatDirect:  true,
		WanOverlay: "AUTO_DISCOVERED",
		Ospf: &velocloud.EdgeDeviceSettingsDataOspf{
			MTU:            1380,
			Area:           0,
			AuthId:         0,
			AuthPassphrase: "",
			Authentication: false,
			Cost:           1,
			DeadTimer:      40,
			Enabled:        false,
			HelloTimer:     10,
			InboundRouteLearning: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "LEARN",
				Filters:       nil,
			},
			Md5Authentication: false,
			OutboundRouteAdvertisement: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "IGNORE",
				Filters:       nil,
			},
			Passive: false,
		},
	}
	ge8 := velocloud.EdgeDeviceSettingsDataRoutedInterfaces{
		Name:      "GE8",
		SegmentId: -1,
		Disabled:  false,
		Addressing: &velocloud.EdgeDeviceSettingsDataAddressing{
			Type_: "DHCP",
		},
		EncryptOverlay: true,
		L2: &velocloud.EdgeDeviceSettingsDataL2{
			Autonegotiation: true,
			Speed:           "100M",
			Duplex:          "FULL",
			MTU:             1500,
		},
		NatDirect:  true,
		WanOverlay: "AUTO_DISCOVERED",
		Ospf: &velocloud.EdgeDeviceSettingsDataOspf{
			MTU:            1380,
			Area:           0,
			AuthId:         0,
			AuthPassphrase: "",
			Authentication: false,
			Cost:           1,
			DeadTimer:      40,
			Enabled:        false,
			HelloTimer:     10,
			InboundRouteLearning: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "LEARN",
				Filters:       nil,
			},
			Md5Authentication: false,
			OutboundRouteAdvertisement: &velocloud.EdgeDeviceSettingsDataOspfInboundRouteLearning{
				DefaultAction: "IGNORE",
				Filters:       nil,
			},
			Passive: false,
		},
	}

	routedinterfaces := make([]velocloud.EdgeDeviceSettingsDataRoutedInterfaces, 0)
	routedinterfaces = append(routedinterfaces, ge2)
	routedinterfaces = append(routedinterfaces, ge3)
	routedinterfaces = append(routedinterfaces, ge4)
	routedinterfaces = append(routedinterfaces, ge5)
	routedinterfaces = append(routedinterfaces, ge6)
	routedinterfaces = append(routedinterfaces, ge7)
	routedinterfaces = append(routedinterfaces, ge8)

	icmpProbes := make([]interface{}, 0)
	icmpResponders := make([]interface{}, 0)
	statics := make([]velocloud.EdgeDeviceSettingsDataRoutesStatic, 0)

	routes := velocloud.EdgeDeviceSettingsDataRoutes{
		IcmpProbes:     icmpProbes,
		IcmpResponders: icmpResponders,
		Static:         statics,
	}
	segment := velocloud.EdgeQosDataSegment{
		Name:      "Global Segment",
		SegmentId: 0,
		Type:      "REGULAR"}

	var vdata interface{}
	vdata = velocloud.EdgeDeviceSettingsData{
		Lan: &velocloud.EdgeDeviceSettingsDataLan{
			Networks: networks,
		},
		RoutedInterfaces: routedinterfaces,
		Netflow: &velocloud.EdgeDeviceSettingsDataNetflow{
			Enabled:    true,
			Version:    10,
			Collectors: netflowcollectors,
		},
		Segments: []velocloud.Segments{{routes, segment}},
	}
	/*
		vsegment := make(map[string][]interface{}, 1)
		vsegment["segments"] = append(vsegment["segments"], vdata)

		var vsegment2 interface{}
		vsegment2 = vdata
	*/
	confmodule := velocloud.ConfigurationUpdateConfigurationModule{
		EnterpriseId: int32(enterpriseid),
		Id:           int32(moduleid),
		Update: &velocloud.ConfigurationModule{
			Name:  "deviceSettings",
			Type_: "ENTERPRISE",
			Data:  &vdata,
		},
	}

	result, code, err := client.ConfigurationApi.ConfigurationUpdateConfigurationModule(context.Background(), confmodule)

	log.Println(result)
	log.Println(code)
	log.Println(err)

	return resourceDeviceSettingsRead(d, m)
}

func resourceDeviceSettingsRead(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************DEVICE READ*****************************************************************")

	client := m.(*velocloud.APIClient)

	enterpriseid := d.Get("enterpriseid").(int)
	configurationid := d.Get("configurationid").(int)
	moduleid := d.Get("moduleid").(int)

	log.Println(fmt.Sprint(enterpriseid))
	log.Println(fmt.Sprint(configurationid))
	log.Println(fmt.Sprint(moduleid))

	body := velocloud.ConfigurationGetConfigurationModules{
		EnterpriseId:    int32(enterpriseid),
		ConfigurationId: int32(configurationid),
		Modules:         []string{"deviceSettings"},
	}

	results, _, _ := client.ConfigurationApi.ConfigurationGetConfigurationModules(context.Background(), body)

	if results == nil {
		d.SetId("")
		return nil
	}
	d.SetId(fmt.Sprint(results[0].Id))

	return nil
}

func resourceDeviceSettingsUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************DEVICE UPDATE*****************************************************************")

	return resourceDeviceSettingsCreate(d, m)
}

func resourceDeviceSettingsDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************DEVICE DELETE*****************************************************************")
	//client := m.(*velocloud.APIClient)

	return nil
}
