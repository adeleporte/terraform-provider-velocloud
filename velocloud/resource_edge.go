package velocloud

import (
	"fmt"
	"log"

	"strconv"

	"github.com/hashicorp/terraform/helper/schema"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

func resourceEdge() *schema.Resource {
	return &schema.Resource{
		Create: resourceEdgeCreate,
		Read:   resourceEdgeRead,
		Update: resourceEdgeUpdate,
		Delete: resourceEdgeDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"model": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"contact": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"email": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"lat": &schema.Schema{
				Type:     schema.TypeFloat,
				Optional: true,
				Default:  nil,
			},
			"lon": &schema.Schema{
				Type:     schema.TypeFloat,
				Optional: true,
				Default:  nil,
			},
			"activationkey": &schema.Schema{
				Type:     schema.TypeString,
				Computed: true,
			},
			"enterpriseid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"configurationid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"specificid": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"devicemodule": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"qosmodule": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
			"firewallmodule": &schema.Schema{
				Type:     schema.TypeInt,
				Computed: true,
			},
		},
	}
}

func resourceEdgeCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************EDGE CREATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	body := velocloud.EdgeEdgeProvision{
		EnterpriseId:    int32(d.Get("enterpriseid").(int)),
		ConfigurationId: int32(d.Get("configurationid").(int)),
		Name:            d.Get("name").(string),
		ModelNumber:     d.Get("model").(string),
		Site: &velocloud.SiteObject{
			ContactName:  d.Get("contact").(string),
			ContactEmail: d.Get("email").(string),
			Lat:          float32(d.Get("lat").(float64)),
			Lon:          float32(d.Get("lon").(float64)),
		},
	}

	result, _, _ := client.EdgeApi.EdgeEdgeProvision(context.Background(), body)
	log.Println("****************************************************************************************************************")
	log.Println("******************************* Edge ID CREATED  is : ", fmt.Sprint(result.Id))

	d.SetId(fmt.Sprint(result.Id))
	d.Set("activationkey", result.ActivationKey)

	return resourceEdgeRead(d, m)
}

func resourceEdgeRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** EDGE READ*****************************************************************")

	client := m.(*velocloud.APIClient)

	body := velocloud.EnterpriseGetEnterpriseEdges{
		EnterpriseId: int32(d.Get("enterpriseid").(int)),
	}
	results, _, _ := client.EnterpriseApi.EnterpriseGetEnterpriseEdges(context.Background(), body)

	log.Println(results[0].Id)
	for _, v := range results {
		if v.Name == d.Get("name").(string) {
			d.SetId(fmt.Sprint(v.Id))
			log.Println("Edge found")

			body := velocloud.EdgeGetEdgeConfigurationStack{
				EnterpriseId: int32(d.Get("enterpriseid").(int)),
				EdgeId:       int32(v.Id),
			}
			results, _, _ := client.EdgeApi.EdgeGetEdgeConfigurationStack(context.Background(), body)
			log.Println(results[0].Name)
			log.Println(results[0].Id)
			d.Set("specificid", results[0].Id)

			var modules []velocloud.ConfigurationModule
			modules = results[0].Modules

			for i, v := range modules {
				if v.Name == "deviceSettings" {
					d.Set("devicemodule", modules[i].Id)
				}
				if v.Name == "QOS" {
					d.Set("qosmodule", modules[i].Id)
				}
				if v.Name == "firewall" {
					d.Set("firewallmodule", modules[i].Id)
				}
			}

			return nil
		}
	}

	d.SetId("")

	return nil
}

func resourceEdgeUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************EDGE UPDATE*****************************************************************")

	client := m.(*velocloud.APIClient)
	id, _ := strconv.Atoi(d.Id())

	body := velocloud.EdgeUpdateEdgeAttributes{
		Id: int32(id),
		EnterpriseId: int32(d.Get("enterpriseid").(int)),
		Update: &velocloud.EdgeupdateEdgeAttributesUpdate{
			Name: d.Get("name").(string),
			Site: &velocloud.SiteObject{
				ContactName:  d.Get("contact").(string),
				ContactEmail: d.Get("email").(string),
				Lat:          float32(d.Get("lat").(float64)),
				Lon:          float32(d.Get("lon").(float64)),
			},
		},
	}

	client.EdgeApi.EdgeUpdateEdgeAttributes(context.Background(), body)

	return resourceEdgeRead(d, m)
}

func resourceEdgeDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************EDGE DELETE*****************************************************************")

	client := m.(*velocloud.APIClient)
	id, _ := strconv.Atoi(d.Id())

	var ids []int32
	ids = append(ids, int32(id))

	body := velocloud.EdgeDeleteEdge{
		EnterpriseId: int32(d.Get("enterpriseid").(int)),
		Ids:          ids,
	}

	client.EdgeApi.EdgeDeleteEdge(context.Background(), body)

	return nil
}
