package velocloud

import (
	"fmt"
	"log"
	"strconv"

	"github.com/hashicorp/terraform/helper/schema"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

func resourceEnterprise() *schema.Resource {
	return &schema.Resource{
		Create: resourceEnterpriseCreate,
		Read:   resourceEnterpriseRead,
		Update: resourceEnterpriseUpdate,
		Delete: resourceEnterpriseDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"username": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"password": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"email": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
		},
	}
}

func resourceEnterpriseCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************CREATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	auth := velocloud.AuthObject{
		Username: d.Get("username").(string),
		Password: d.Get("password").(string),
		Email:    d.Get("email").(string),
	}

	ege2 := velocloud.EnterpriseInsertEnterprise{
		Name:            d.Get("name").(string),
		User:            &auth,
		NetworkId:       1,
		ConfigurationId: 6,
		GatewayPoolId:   1,
		EnableEnterpriseDelegationToOperator: true,
	}

	result, _, _ := client.EnterpriseApi.EnterpriseInsertEnterprise(context.Background(), ege2)
	log.Println("****************************************************************************************************************")
	log.Println("******************************* Edge ID CREATED  is : ", fmt.Sprint(result.Id))

	d.SetId(fmt.Sprint(result.Id))

	return resourceEnterpriseRead(d, m)
}

func resourceEnterpriseRead(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************READ*****************************************************************")

	client := m.(*velocloud.APIClient)

	id, err := strconv.Atoi(d.Id())
	if err != nil {
		return fmt.Errorf("Error obtaining Enterprise id")
	}

	ege := velocloud.EnterpriseGetEnterprise{
		EnterpriseId: int32(id),
	}

	result, _, err := client.EnterpriseApi.EnterpriseGetEnterprise(context.Background(), ege)

	if result.Id == 0 {
		log.Println("******************************Customer is not found**********************************************")
		d.SetId("")
		return nil
	}

	//d.SetId(fmt.Sprint(result.Id))
	return nil
}

func resourceEnterpriseUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************UPDATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	id, err := strconv.Atoi(d.Id())
	if err != nil {
		return fmt.Errorf("Error obtaining Enterprise id")
	}

	auth := velocloud.AuthObject{
		Username: d.Get("username").(string),
		Password: d.Get("password").(string),
	}

	enterprise := velocloud.EnterpriseObject{
		User: &auth,
	}

	ege := velocloud.EnterpriseUpdateEnterprise{
		EnterpriseId: int32(id),
		Update:       &enterprise,
	}

	_, _, err = client.EnterpriseApi.EnterpriseUpdateEnterprise(context.Background(), ege)

	return resourceEnterpriseRead(d, m)
}

func resourceEnterpriseDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************DELETE*****************************************************************")
	client := m.(*velocloud.APIClient)

	id, err := strconv.Atoi(d.Id())
	if err != nil {
		return fmt.Errorf("Error obtaining Enterprise id")
	}

	ege := velocloud.EnterpriseDeleteEnterprise{
		EnterpriseId: int32(id),
	}

	_, _, err = client.EnterpriseApi.EnterpriseDeleteEnterprise(context.Background(), ege)

	return nil
}
