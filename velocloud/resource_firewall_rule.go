package velocloud

import (
	"fmt"
	"log"

	//"strconv"
	//"github.com/jinzhu/copier"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/helper/validation"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

var firewallRuleActionValues = []string{"allow", "deny"}

func resourceFirewallRule() *schema.Resource {
	return &schema.Resource{
		Create: resourceFirewallRuleCreate,
		Read:   resourceFirewallRuleRead,
		Update: resourceFirewallRuleUpdate,
		Delete: resourceFirewallRuleDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"enterpriseid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"configurationid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"fwmodule": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"rule": getFirewallRulesSchema(),
			"enabled": &schema.Schema{
				Type:     schema.TypeBool,
				Default:  true,
				Optional: true,
			},
			"logging": &schema.Schema{
				Type:     schema.TypeBool,
				Default:  true,
				Optional: true,
			},
		},
	}
}

func getFirewallRulesSchema() *schema.Schema {
	return &schema.Schema{
		Type:        schema.TypeList,
		Description: "List of firewall rules in the section. Only homogeneous rules are supported",
		Optional:    true,
		Elem: &schema.Resource{
			Schema: map[string]*schema.Schema{
				"id": &schema.Schema{
					Type:     schema.TypeString,
					Computed: true,
				},
				"name": &schema.Schema{
					Type:     schema.TypeString,
					Required: true,
				},
				"sip": &schema.Schema{
					Type:     schema.TypeString,
					Default:  "any",
					Optional: true,
				},
				"dip": &schema.Schema{
					Type:     schema.TypeString,
					Default:  "any",
					Optional: true,
				},
				"smask": &schema.Schema{
					Type:     schema.TypeString,
					Default:  "255.255.255.255",
					Optional: true,
				},
				"dmask": &schema.Schema{
					Type:     schema.TypeString,
					Default:  "255.255.255.255",
					Optional: true,
				},
				"sport": &schema.Schema{
					Type:     schema.TypeInt,
					Default:  "-1",
					Optional: true,
				},
				"dport": &schema.Schema{
					Type:     schema.TypeInt,
					Default:  "-1",
					Optional: true,
				},
				"proto": &schema.Schema{
					Type:     schema.TypeInt,
					Default:  "-1",
					Optional: true,
				},
				"action": &schema.Schema{
					Type:         schema.TypeString,
					Required:     true,
					ValidateFunc: validation.StringInSlice(firewallRuleActionValues, false),
				},
			},
		},
	}
}

func getFirewallRulesFromSchema(d *schema.ResourceData) []velocloud.FirewallOutboundRule {
	rules := d.Get("rule").([]interface{})

	var ruleList []velocloud.FirewallOutboundRule
	for _, rule := range rules {
		data := rule.(map[string]interface{})
		elem := velocloud.FirewallOutboundRule{
			Name: data["name"].(string),
			Match: &velocloud.FirewallRuleMatch{
				Appid:     -1,
				Classid:   -1,
				Dip:       data["dip"].(string),
				DportHigh: int32(data["dport"].(int)),
				DportLow:  int32(data["dport"].(int)),
				Dscp:      -1,
				Dsm:       data["dmask"].(string),
				Dvlan:     -1,
				Hostname:  "",
				OsVersion: -1,
				Proto:     int32(data["proto"].(int)),
				Sip:       data["sip"].(string),
				SportHigh: int32(data["sport"].(int)),
				SportLow:  int32(data["sport"].(int)),
				Ssm:       data["smask"].(string),
				Svlan:     -1,
			},
			Action: &velocloud.FirewallOutboundRuleAction{
				AllowOrDeny: data["action"].(string),
			},
		}
		log.Println(fmt.Sprint(elem.Match.Dip))

		ruleList = append(ruleList, elem)
	}
	return ruleList
}

func resourceFirewallRuleCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************FW CREATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	enterpriseid := d.Get("enterpriseid").(int)
	configurationid := d.Get("configurationid").(int)
	moduleid := d.Get("fwmodule").(int)
	rules := getFirewallRulesFromSchema(d)

	log.Println(fmt.Sprint(enterpriseid))
	log.Println(fmt.Sprint(configurationid))
	log.Println(fmt.Sprint(moduleid))

	var vdata interface{}
	vdata = velocloud.FirewallData{
		FirewallEnabled:        d.Get("enabled").(bool),
		FirewallLoggingEnabled: d.Get("logging").(bool),
		Inbound:                nil,
		Outbound:               rules,
		Services:               nil,
		Segment: velocloud.EdgeQosDataSegment{
			Name:      "Global Segment",
			SegmentId: 0,
			Type:      "REGULAR",
		},
	}

	vsegment := make(map[string][]interface{}, 1)
	vsegment["segments"] = append(vsegment["segments"], vdata)

	var vsegment2 interface{}
	vsegment2 = vsegment

	confmodule := velocloud.ConfigurationUpdateConfigurationModule{
		EnterpriseId: int32(enterpriseid),
		Id:           int32(moduleid),
		Update: &velocloud.ConfigurationModule{
			Name:  "FIREWALL",
			Type_: "ENTERPRISE",
			Data:  &vsegment2,
		},
	}

	result, code, err := client.ConfigurationApi.ConfigurationUpdateConfigurationModule(context.Background(), confmodule)

	fmt.Println(result)
	fmt.Println(code)
	fmt.Println(err)

	d.SetId(fmt.Sprint(d.Get("fwmodule")))

	return resourceFirewallRuleRead(d, m)
}

func resourceFirewallRuleRead(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************FW READ*****************************************************************")
	client := m.(*velocloud.APIClient)

	enterpriseid := d.Get("enterpriseid").(int)
	configurationid := d.Get("configurationid").(int)
	moduleid := d.Get("fwmodule").(int)

	log.Println(fmt.Sprint(enterpriseid))
	log.Println(fmt.Sprint(configurationid))
	log.Println(fmt.Sprint(moduleid))

	body := velocloud.ConfigurationGetConfigurationModules{
		EnterpriseId:    int32(enterpriseid),
		ConfigurationId: int32(configurationid),
		Modules:         []string{"FIREWALL"},
	}

	var result []velocloud.ConfigurationGetConfigurationModulesResultItem
	result, _, _ = client.ConfigurationApi.ConfigurationGetConfigurationModules(context.Background(), body)

	if result == nil {
		d.SetId("")
		return nil
	}

	return nil
}

func resourceFirewallRuleUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************FW UPDATE*****************************************************************")

	return resourceFirewallRuleCreate(d, m)

}

func resourceFirewallRuleDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************FW DELETE*****************************************************************")
	client := m.(*velocloud.APIClient)

	enterpriseid := d.Get("enterpriseid").(int)
	configurationid := d.Get("configurationid").(int)
	moduleid := d.Get("fwmodule").(int)
	//rules := getFirewallRulesFromSchema(d)

	log.Println(fmt.Sprint(enterpriseid))
	log.Println(fmt.Sprint(configurationid))
	log.Println(fmt.Sprint(moduleid))

	var ruleList []velocloud.FirewallOutboundRule

	var vdata interface{}
	vdata = velocloud.FirewallData{
		FirewallEnabled:        d.Get("enabled").(bool),
		FirewallLoggingEnabled: d.Get("logging").(bool),
		Inbound:                nil,
		Outbound:               ruleList,
		Services:               nil,
		Segment: velocloud.EdgeQosDataSegment{
			Name:      "Global Segment",
			SegmentId: 0,
			Type:      "REGULAR",
		},
	}

	vsegment := make(map[string][]interface{}, 1)
	vsegment["segments"] = append(vsegment["segments"], vdata)

	var vsegment2 interface{}
	vsegment2 = vsegment

	confmodule := velocloud.ConfigurationUpdateConfigurationModule{
		EnterpriseId: int32(enterpriseid),
		Id:           int32(moduleid),
		Update: &velocloud.ConfigurationModule{
			Name:  "FIREWALL",
			Type_: "ENTERPRISE",
			Data:  &vsegment2,
		},
	}

	result, code, err := client.ConfigurationApi.ConfigurationUpdateConfigurationModule(context.Background(), confmodule)

	fmt.Println(result)
	fmt.Println(code)
	fmt.Println(err)

	d.SetId("")

	return nil
}
