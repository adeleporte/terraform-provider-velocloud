package velocloud

import (
	"fmt"
	"log"

	"strconv"

	"github.com/hashicorp/terraform/helper/schema"
	velocloud "velocloud"
	"golang.org/x/net/context"
)

func resourceGateway() *schema.Resource {
	return &schema.Resource{
		Create: resourceGatewayCreate,
		Read:   resourceGatewayRead,
		Update: resourceGatewayUpdate,
		Delete: resourceGatewayDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"contact": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"email": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"ip_address": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
		},
	}
}

func resourceGatewayCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Gateway CREATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	body := velocloud.GatewayGatewayProvision{
		NetworkId:     1,
		GatewayPoolId: 1,
		IpAddress:     d.Get("ip_address").(string),
		Name:          d.Get("name").(string),
		Site: &velocloud.SiteObject{
			ContactName:  d.Get("contact").(string),
			ContactEmail: d.Get("email").(string),
		},
	}

	result, _, _ := client.GatewayApi.GatewayGatewayProvision(context.Background(), body)
	log.Println("****************************************************************************************************************")
	log.Println("******************************* Gateway ID CREATED  is : ", fmt.Sprint(result.Id))

	d.SetId(fmt.Sprint(result.Id))
	d.Set("activationkey", result.ActivationKey)

	return resourceGatewayRead(d, m)
}

func resourceGatewayRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** Gateway READ*****************************************************************")

	client := m.(*velocloud.APIClient)

	body := velocloud.NetworkGetNetworkGateways{
		NetworkId: 1,
	}
	results, _, _ := client.NetworkApi.NetworkGetNetworkGateways(context.Background(), body)

	for _, v := range results {
		if v.Name == d.Get("name").(string) {
			d.SetId(fmt.Sprint(v.Id))
			log.Println("Gateway found")

			return nil
		}
	}

	d.SetId("")

	return nil
}

func resourceGatewayUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Gateway UPDATE*****************************************************************")
	return nil
}

func resourceGatewayDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Gateway DELETE*****************************************************************")

	client := m.(*velocloud.APIClient)
	id, _ := strconv.Atoi(d.Id())

	body := velocloud.GatewayDeleteGateway{
		Id: int32(id),
	}

	client.GatewayApi.GatewayDeleteGateway(context.Background(), body)

	return nil
}
