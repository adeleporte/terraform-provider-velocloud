package velocloud

import (
	"log"

	"github.com/hashicorp/terraform/helper/schema"
	velocloud "velocloud"
	"golang.org/x/net/context"
	"github.com/hashicorp/terraform/helper/validation"
)

var dataTypeValues = []string{"STRING", "BOOLEAN", "NUMBER", "JSON", "DATE", "DATETIME"}

func resourceProperty() *schema.Resource {
	return &schema.Resource{
		Create: resourcePropertyCreate,
		Read:   resourcePropertyRead,
		Update: resourcePropertyUpdate,
		Delete: resourcePropertyDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"name": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"value": &schema.Schema{
				Type:     schema.TypeString,
				Required: true,
			},
			"datatype": &schema.Schema{
				Type:     schema.TypeString,
				Optional: true,
				Default: "STRING",
				ValidateFunc: validation.StringInSlice(dataTypeValues, false),
			},
		},
	}
}


func resourcePropertyCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Property PROPERTY CREATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	name := d.Get("name").(string)
	value := d.Get("value").(string)
	datatype := d.Get("datatype").(string)

	property := velocloud.SystemPropertyUpdateSystemProperty {
		Name: name,
		Update : &velocloud.SystemPropertyupdateSystemPropertyUpdate {
			Name: name,
			Value: value,
			DataType: datatype,
		},
	}

	client.SystemPropertyApi.SystemPropertyUpdateSystemProperty(context.Background(), property)

	d.SetId(name)
	d.Set("value", value)

	return resourcePropertyRead(d, m)
}

func resourcePropertyRead(d *schema.ResourceData, m interface{}) error {
	log.Println("*********************************************** Property PROPERTY READ*****************************************************************")
	client := m.(*velocloud.APIClient)

	property := velocloud.SystemPropertyGetSystemProperty {
		Name: d.Get("name").(string),
	}

	result, _, err := client.SystemPropertyApi.SystemPropertyGetSystemProperty(context.Background(), property)

	if err != nil {
		d.SetId("")
	}
	
	d.SetId(result.Name)
	d.Set("value", result.Value)

	return nil
}

func resourcePropertyUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Property PROPERTY UPDATE*****************************************************************")
	return resourcePropertyCreate(d, m)

}

func resourcePropertyDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************Property PROPERTY DELETE*****************************************************************")

	client := m.(*velocloud.APIClient)

	name := d.Get("name").(string)
	value := ""

	property := velocloud.SystemPropertyUpdateSystemProperty {
		Name: name,
		Update : &velocloud.SystemPropertyupdateSystemPropertyUpdate {
			Name: name,
			Value: value,
		},
	}

	client.SystemPropertyApi.SystemPropertyUpdateSystemProperty(context.Background(), property)

	d.SetId("")

	return nil
}
