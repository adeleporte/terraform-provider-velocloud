package velocloud

import (
	"fmt"
	"log"

	//"strconv"
	//"github.com/jinzhu/copier"

	"github.com/hashicorp/terraform/helper/schema"
	"github.com/hashicorp/terraform/helper/validation"
	velocloud "velocloud"
	"golang.org/x/net/context"
)
// QoSRuleActionValues describes DMPO actions
var QoSRuleActionValues = []string{"edge2Cloud", "gateway"}

// QoSRuleClassValues describes QoS
var QoSRuleClassValues = []string{"realtime", "transactional", "bulk"}

var QoSServiceGroupValues = []string{"ALL", "PUBLIC_WIRED", "PRIVATE_WIRED"}

func resourceQoSRule() *schema.Resource {
	return &schema.Resource{
		Create: resourceQoSRuleCreate,
		Read:   resourceQoSRuleRead,
		Update: resourceQoSRuleUpdate,
		Delete: resourceQoSRuleDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"enterpriseid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"configurationid": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"qosmodule": &schema.Schema{
				Type:     schema.TypeInt,
				Required: true,
			},
			"rule": getQoSRulesSchema(),
		},
	}
}

func getQoSRulesSchema() *schema.Schema {
	return &schema.Schema{
		Type:        schema.TypeList,
		Description: "List of QoS rules",
		Optional:    true,
		Elem: &schema.Resource{
			Schema: map[string]*schema.Schema{
				"name": &schema.Schema{
					Type:     schema.TypeString,
					Required: true,
				},
				"dip": &schema.Schema{
					Type:     schema.TypeString,
					Default:  "any",
					Optional: true,
				},
				"dmask": &schema.Schema{
					Type:     schema.TypeString,
					Default:  "255.255.255.255",
					Optional: true,
				},
				"dport": &schema.Schema{
					Type:     schema.TypeInt,
					Default:  "-1",
					Optional: true,
				},
				"proto": &schema.Schema{
					Type:     schema.TypeInt,
					Default:  "-1",
					Optional: true,
				},
				"bandwidthpct": &schema.Schema{
					Type:     schema.TypeInt,
					Default:  "-1",
					Optional: true,
				},
				"routeaction": &schema.Schema{
					Type:         schema.TypeString,
					Default:     "edge2Cloud",
					ValidateFunc: validation.StringInSlice(QoSRuleActionValues, false),
					Optional: true,
				},
				"class": &schema.Schema{
					Type:         schema.TypeString,
					Default:     "bulk",
					ValidateFunc: validation.StringInSlice(QoSRuleClassValues, false),
					Optional: true,
				},
				"servicegroup": &schema.Schema{
					Type:         schema.TypeString,
					Default:     "ALL",
					ValidateFunc: validation.StringInSlice(QoSServiceGroupValues, false),
					Optional: true,
				},
			},
		},
	}
}

func getQoSRulesFromSchema(d *schema.ResourceData) []velocloud.EdgeQosDataRules {
	rules := d.Get("rule").([]interface{})

	var ruleList []velocloud.EdgeQosDataRules
	for _, rule := range rules {
		data := rule.(map[string]interface{})

		qos := SetQoS(d, data["bandwidthpct"].(int), data["class"].(string))
		sla := SetSLA(d)
		edge2cloudrouteaction := SetRouteAction(d, data["routeaction"].(string), data["servicegroup"].(string))
		edge2datacenterrouteaction := SetRouteAction(d, data["routeaction"].(string),  data["servicegroup"].(string))
		edge2edgerouteaction := SetRouteAction(d, data["routeaction"].(string), data["servicegroup"].(string))
		match := SetMatch(d, data["dip"].(string), data["dmask"].(string), data["dport"].(int), data["proto"].(int))

		elem := velocloud.EdgeQosDataRules{
			Name: data["name"].(string),
			Action: &velocloud.EdgeQosDataAction{
				QoS:                        &qos,
				Sla:                        &sla,
				RouteType:                  data["routeaction"].(string),
				Edge2CloudRouteAction:      &edge2cloudrouteaction,
				Edge2DataCenterRouteAction: &edge2datacenterrouteaction,
				Edge2EdgeRouteAction:       &edge2edgerouteaction,
			},
			Match: &match,
		}
		log.Println(elem)

		ruleList = append(ruleList, elem)
	}
	return ruleList
}

func getDefaultQoSRules(d *schema.ResourceData) []interface{} {

	qos := SetQoS(d, -1, "transactional")
	sla := SetSLA(d)
	edge2cloudrouteaction := SetRouteAction(d, "edge2Cloud", "ALL")
	edge2datacenterrouteaction := SetRouteAction(d, "edge2Cloud", "ALL")
	edge2edgerouteaction := SetRouteAction(d, "edge2Cloud", "ALL")
	match := SetMatch(d, "any", "255.255.255.255", -1, -1)

	elem := velocloud.EdgeQosDataRules{
		Name: "default",
		Action: &velocloud.EdgeQosDataAction{
			QoS:                        &qos,
			Sla:                        &sla,
			RouteType:                  "edge2Cloud",
			Edge2CloudRouteAction:      &edge2cloudrouteaction,
			Edge2DataCenterRouteAction: &edge2datacenterrouteaction,
			Edge2EdgeRouteAction:       &edge2edgerouteaction,
		},
		Match: &match,
	}

	vdefaults := make([]interface{}, 0)
	vdefaults = append(vdefaults, elem)

	return vdefaults
}

// SetQoS set the Class of Service
func SetQoS(d *schema.ResourceData, bandwidthpct int, class string) interface{} {
	var qos interface{}
	qos = velocloud.QoS{
		Type: class,
		RxScheduler: velocloud.Scheduler{
			Bandwidth:       -1,
			BandwidthCapPct: int32(bandwidthpct),
			Burst:           -1,
			Latency:         -1,
			Priority:        "high",
			QueueLen:        -1,
		},
		TxScheduler: velocloud.Scheduler{
			Bandwidth:       -1,
			BandwidthCapPct: int32(bandwidthpct),
			Burst:           -1,
			Latency:         -1,
			Priority:        "high",
			QueueLen:        -1,
		},
	}
	return qos
}

// SetSLA set SLA
func SetSLA(d *schema.ResourceData) interface{} {
	var sla interface{}
	sla = velocloud.WanDataStaticSla{
		LatencyMs: 0,
		JitterMs:  0,
		LossPct:   0,
	}
	return sla
}

// SetRouteAction choose between Local Breakout or send to cloud gateways
func SetRouteAction(d *schema.ResourceData, action string, sg string) velocloud.RouteActionObject {
	var obj velocloud.RouteActionObject
	actionString := "gateway"
	if action == "dmpo" {
		actionString = "edge2Any"
	}
	lpString := "auto"
	if sg != "ALL" {
		lpString = "fixed"
	}

	obj = velocloud.RouteActionObject{
		Interface_:            "auto",
		LinkCosLogicalId:      "",
		LinkInnerDscpTag:      "",
		LinkInternalLogicalId: "auto",
		LinkOuterDscpTag:      "",
		LinkPolicy:            lpString,
		RouteCfg:              nil,
		RoutePolicy:           actionString,
		ServiceGroup:          sg,
		VlanId:                -1,
		Wanlink:               "auto",
	}
	return obj
}

// SetMatch matches the traffic
func SetMatch(d *schema.ResourceData, dip string, dmask string, dport int, proto int) velocloud.EdgeQosDataMatch {
	var match velocloud.EdgeQosDataMatch
	match = velocloud.EdgeQosDataMatch{
		Appid:     -1,
		Classid:   -1,
		Dip:       dip,
		DportHigh: int32(dport),
		DportLow:  int32(dport),
		Dscp:      -1,
		Dsm:       dmask,
		Dvlan:     -1,
		Hostname:  "",
		OsVersion: -1,
		Proto:     int32(proto),
		Sip:       "any",
		SportHigh: -1,
		SportLow:  -1,
		Ssm:       "255.255.255.255",
		Svlan:     -1,
	}
	return match
}

// SetCos set cos values
func SetCos(d *schema.ResourceData) velocloud.EdgeQosDataCosMapping {
	var cos velocloud.EdgeQosDataCosMapping
	cos = velocloud.EdgeQosDataCosMapping{
		LsInputType: "weight",
		Bulk: &velocloud.CosMapping{
			High: &velocloud.CosMappingValue{
				Value:     15,
				Ratelimit: false,
			},
			Low: &velocloud.CosMappingValue{
				Value:     1,
				Ratelimit: false,
			},
			Normal: &velocloud.CosMappingValue{
				Value:     5,
				Ratelimit: false,
			},
		},
		Realtime: &velocloud.CosMapping{
			High: &velocloud.CosMappingValue{
				Value:     15,
				Ratelimit: false,
			},
			Low: &velocloud.CosMappingValue{
				Value:     1,
				Ratelimit: false,
			},
			Normal: &velocloud.CosMappingValue{
				Value:     5,
				Ratelimit: false,
			},
		},
		Transactional: &velocloud.CosMapping{
			High: &velocloud.CosMappingValue{
				Value:     15,
				Ratelimit: false,
			},
			Low: &velocloud.CosMappingValue{
				Value:     1,
				Ratelimit: false,
			},
			Normal: &velocloud.CosMappingValue{
				Value:     5,
				Ratelimit: false,
			},
		},
	}
	return cos
}

func resourceQoSRuleCreate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************QOS CREATE*****************************************************************")
	client := m.(*velocloud.APIClient)

	enterpriseid := d.Get("enterpriseid").(int)
	moduleid := d.Get("qosmodule").(int)
	vcos := SetCos(d)

	var vdata interface{}
	vdata = velocloud.EdgeQosData{
		Rules:            getQoSRulesFromSchema(d),
		Defaults:         getDefaultQoSRules(d),
		WebProxy:         nil,
		ServiceRateLimit: nil,
		CosMapping:       &vcos,
		Segment: velocloud.EdgeQosDataSegment{
			Name:      "Global Segment",
			SegmentId: 0,
			Type:      "REGULAR",
		},
	}

	vsegment := make(map[string][]interface{}, 1)
	vsegment["segments"] = append(vsegment["segments"], vdata)

	var vsegment2 interface{}
	vsegment2 = vsegment

	confmodule := velocloud.ConfigurationUpdateConfigurationModule{
		EnterpriseId: int32(enterpriseid),
		Id:           int32(moduleid),
		Update: &velocloud.ConfigurationModule{
			Name:  "QOS",
			Type_: "ENTERPRISE",
			Data:  &vsegment2,
		},
	}

	result, code, err := client.ConfigurationApi.ConfigurationUpdateConfigurationModule(context.Background(), confmodule)

	fmt.Println(result)
	fmt.Println(code)
	fmt.Println(err)

	return resourceQoSRuleRead(d, m)
}

func resourceQoSRuleRead(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************QOS READ*****************************************************************")
	client := m.(*velocloud.APIClient)

	enterpriseid := d.Get("enterpriseid").(int)
	configurationid := d.Get("configurationid").(int)
	qosmoduleid := d.Get("qosmodule").(int)

	log.Println(fmt.Sprint(enterpriseid))
	log.Println(fmt.Sprint(configurationid))
	log.Println(fmt.Sprint(qosmoduleid))

	body := velocloud.ConfigurationGetConfigurationModules{
		EnterpriseId:    int32(enterpriseid),
		ConfigurationId: int32(configurationid),
		Modules:         []string{"QOS"},
	}

	result2, _, _ := client.ConfigurationApi.ConfigurationGetConfigurationModules(context.Background(), body)

	if result2 == nil {
		d.SetId("")
		return nil
	}


	d.SetId(fmt.Sprint(d.Get("qosmodule")))
	return nil
}

func resourceQoSRuleUpdate(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************QOS UPDATE*****************************************************************")

	return resourceQoSRuleCreate(d, m)

}

func resourceQoSRuleDelete(d *schema.ResourceData, m interface{}) error {
	log.Println("***********************************************QOS DELETE*****************************************************************")
	client := m.(*velocloud.APIClient)

	enterpriseid := d.Get("enterpriseid").(int)
	moduleid := d.Get("qosmodule").(int)
	vcos := SetCos(d)



	var vdata interface{}
	vdata = velocloud.EdgeQosData{
		Rules:            []velocloud.EdgeQosDataRules {},
		Defaults:         getDefaultQoSRules(d),
		WebProxy:         nil,
		ServiceRateLimit: nil,
		CosMapping:       &vcos,
		Segment: velocloud.EdgeQosDataSegment{
			Name:      "Global Segment",
			SegmentId: 0,
			Type:      "REGULAR",
		},
	}

	vsegment := make(map[string][]interface{}, 1)
	vsegment["segments"] = append(vsegment["segments"], vdata)

	var vsegment2 interface{}
	vsegment2 = vsegment

	confmodule := velocloud.ConfigurationUpdateConfigurationModule{
		EnterpriseId: int32(enterpriseid),
		Id:           int32(moduleid),
		Update: &velocloud.ConfigurationModule{
			Name:  "QOS",
			Type_: "ENTERPRISE",
			Data:  &vsegment2,
		},
	}

	result, code, err := client.ConfigurationApi.ConfigurationUpdateConfigurationModule(context.Background(), confmodule)

	fmt.Println(result)
	fmt.Println(code)
	fmt.Println(err)
	return nil
}
