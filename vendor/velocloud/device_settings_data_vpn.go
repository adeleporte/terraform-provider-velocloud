/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type DeviceSettingsDataVpn struct {

	Enabled bool `json:"enabled,omitempty"`

	EdgeToDataCenter bool `json:"edgeToDataCenter,omitempty"`

	Ref string `json:"ref,omitempty"`

	EdgeToEdgeHub *DeviceSettingsDataVpnEdgeToEdgeHub `json:"edgeToEdgeHub,omitempty"`

	EdgeToEdge bool `json:"edgeToEdge,omitempty"`

	EdgeToEdgeDetail *DeviceSettingsDataVpnEdgeToEdgeDetail `json:"edgeToEdgeDetail,omitempty"`
}
