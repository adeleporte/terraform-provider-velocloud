/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type DnsData struct {

	Primary string `json:"primary,omitempty"`

	Secondary string `json:"secondary,omitempty"`

	IsPrivate bool `json:"isPrivate,omitempty"`

	Domains []DnsDataDomains `json:"domains,omitempty"`
}
