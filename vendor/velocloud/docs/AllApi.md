# \AllApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ConfigurationCloneAndConvertConfiguration**](AllApi.md#ConfigurationCloneAndConvertConfiguration) | **Post** /configuration/cloneAndConvertConfiguration | Clone and convert a network based profile configuration to segment based profile configuration
[**ConfigurationCloneConfiguration**](AllApi.md#ConfigurationCloneConfiguration) | **Post** /configuration/cloneConfiguration | Clone configuration profile
[**ConfigurationCloneEnterpriseTemplate**](AllApi.md#ConfigurationCloneEnterpriseTemplate) | **Post** /configuration/cloneEnterpriseTemplate | Clone default enterprise configuration profile
[**ConfigurationDeleteConfiguration**](AllApi.md#ConfigurationDeleteConfiguration) | **Post** /configuration/deleteConfiguration | Delete a configuration profile
[**ConfigurationGetConfiguration**](AllApi.md#ConfigurationGetConfiguration) | **Post** /configuration/getConfiguration | Get a configuration profile
[**ConfigurationGetConfigurationModules**](AllApi.md#ConfigurationGetConfigurationModules) | **Post** /configuration/getConfigurationModules | List the modules that compose a configuration profile
[**ConfigurationGetRoutableApplications**](AllApi.md#ConfigurationGetRoutableApplications) | **Post** /configuration/getRoutableApplications | Get first packet routable applications
[**ConfigurationInsertConfigurationModule**](AllApi.md#ConfigurationInsertConfigurationModule) | **Post** /configuration/insertConfigurationModule | Insert a new configuration module
[**ConfigurationUpdateConfigurationModule**](AllApi.md#ConfigurationUpdateConfigurationModule) | **Post** /configuration/updateConfigurationModule | Update a configuration module
[**DisasterRecoveryConfigureActiveForReplication**](AllApi.md#DisasterRecoveryConfigureActiveForReplication) | **Post** /disasterRecovery/configureActiveForReplication | Designate a standby Orchestrator for disaster recovery replication
[**DisasterRecoveryDemoteActive**](AllApi.md#DisasterRecoveryDemoteActive) | **Post** /disasterRecovery/demoteActive | Demote current server from active to zombie
[**DisasterRecoveryGetReplicationBlob**](AllApi.md#DisasterRecoveryGetReplicationBlob) | **Post** /disasterRecovery/getReplicationBlob | Get the blob needed to configure replication on the standby
[**DisasterRecoveryGetReplicationStatus**](AllApi.md#DisasterRecoveryGetReplicationStatus) | **Post** /disasterRecovery/getReplicationStatus | Get disaster recovery status
[**DisasterRecoveryPrepareForStandby**](AllApi.md#DisasterRecoveryPrepareForStandby) | **Post** /disasterRecovery/prepareForStandby | Prepare current Orchestrator to be configured as a standby system
[**DisasterRecoveryPromoteStandbyToActive**](AllApi.md#DisasterRecoveryPromoteStandbyToActive) | **Post** /disasterRecovery/promoteStandbyToActive | Promote the current server to take over as the single standalone VCO
[**DisasterRecoveryRemoveStandby**](AllApi.md#DisasterRecoveryRemoveStandby) | **Post** /disasterRecovery/removeStandby | Unconfigure disaster recovery on the current server
[**DisasterRecoveryTransitionToStandby**](AllApi.md#DisasterRecoveryTransitionToStandby) | **Post** /disasterRecovery/transitionToStandby | Configure current Orchestrator to transition to standby in disaster recovery active/standby pair.
[**EdgeDeleteEdge**](AllApi.md#EdgeDeleteEdge) | **Post** /edge/deleteEdge | Delete an edge
[**EdgeDeleteEdgeBgpNeighborRecords**](AllApi.md#EdgeDeleteEdgeBgpNeighborRecords) | **Post** /edge/deleteEdgeBgpNeighborRecords | Delete edge BGP neighbor records
[**EdgeEdgeCancelReactivation**](AllApi.md#EdgeEdgeCancelReactivation) | **Post** /edge/edgeCancelReactivation | Cancel a pending edge reactivation request
[**EdgeEdgeProvision**](AllApi.md#EdgeEdgeProvision) | **Post** /edge/edgeProvision | Provision an edge
[**EdgeEdgeRequestReactivation**](AllApi.md#EdgeEdgeRequestReactivation) | **Post** /edge/edgeRequestReactivation | Reactivate an edge
[**EdgeGetClientVisibilityMode**](AllApi.md#EdgeGetClientVisibilityMode) | **Post** /edge/getClientVisibilityMode | Get an edge&#39;s client visibility mode
[**EdgeGetEdge**](AllApi.md#EdgeGetEdge) | **Post** /edge/getEdge | Get edge
[**EdgeGetEdgeConfigurationStack**](AllApi.md#EdgeGetEdgeConfigurationStack) | **Post** /edge/getEdgeConfigurationStack | Get an edge&#39;s configuration stack
[**EdgeSetEdgeEnterpriseConfiguration**](AllApi.md#EdgeSetEdgeEnterpriseConfiguration) | **Post** /edge/setEdgeEnterpriseConfiguration | Apply an enterprise configuration to an Edge
[**EdgeSetEdgeHandOffGateways**](AllApi.md#EdgeSetEdgeHandOffGateways) | **Post** /edge/setEdgeHandOffGateways | Set an edge&#39;s on-premise hand off gateways
[**EdgeSetEdgeOperatorConfiguration**](AllApi.md#EdgeSetEdgeOperatorConfiguration) | **Post** /edge/setEdgeOperatorConfiguration | Apply an operator configuration to an Edge
[**EdgeUpdateEdgeAdminPassword**](AllApi.md#EdgeUpdateEdgeAdminPassword) | **Post** /edge/updateEdgeAdminPassword | Update edge&#39;s local UI authentication credentials
[**EdgeUpdateEdgeAttributes**](AllApi.md#EdgeUpdateEdgeAttributes) | **Post** /edge/updateEdgeAttributes | Update edge attributes
[**EdgeUpdateEdgeCredentialsByConfiguration**](AllApi.md#EdgeUpdateEdgeCredentialsByConfiguration) | **Post** /edge/updateEdgeCredentialsByConfiguration | Update edge UI credentials by configuration id
[**EnterpriseDeleteEnterprise**](AllApi.md#EnterpriseDeleteEnterprise) | **Post** /enterprise/deleteEnterprise | Delete an enterprise
[**EnterpriseDeleteEnterpriseGatewayRecords**](AllApi.md#EnterpriseDeleteEnterpriseGatewayRecords) | **Post** /enterprise/deleteEnterpriseGatewayRecords | Delete enterprise gateway record(s)
[**EnterpriseDeleteEnterpriseNetworkAllocation**](AllApi.md#EnterpriseDeleteEnterpriseNetworkAllocation) | **Post** /enterprise/deleteEnterpriseNetworkAllocation | Delete an enterprise network allocation
[**EnterpriseDeleteEnterpriseService**](AllApi.md#EnterpriseDeleteEnterpriseService) | **Post** /enterprise/deleteEnterpriseService | Delete an enterprise service
[**EnterpriseGetEnterprise**](AllApi.md#EnterpriseGetEnterprise) | **Post** /enterprise/getEnterprise | Get enterprise
[**EnterpriseGetEnterpriseAddresses**](AllApi.md#EnterpriseGetEnterpriseAddresses) | **Post** /enterprise/getEnterpriseAddresses | Get enterprise IP address information
[**EnterpriseGetEnterpriseAlertConfigurations**](AllApi.md#EnterpriseGetEnterpriseAlertConfigurations) | **Post** /enterprise/getEnterpriseAlertConfigurations | Get the enterprise alert configuration
[**EnterpriseGetEnterpriseAlerts**](AllApi.md#EnterpriseGetEnterpriseAlerts) | **Post** /enterprise/getEnterpriseAlerts | Get triggered enterprise alerts
[**EnterpriseGetEnterpriseAllAlertRecipients**](AllApi.md#EnterpriseGetEnterpriseAllAlertRecipients) | **Post** /enterprise/getEnterpriseAllAlertsRecipients | List recipients currently receiving ALL enterprise alerts
[**EnterpriseGetEnterpriseCapabilities**](AllApi.md#EnterpriseGetEnterpriseCapabilities) | **Post** /enterprise/getEnterpriseCapabilities | Get enterprise capabilities
[**EnterpriseGetEnterpriseConfigurations**](AllApi.md#EnterpriseGetEnterpriseConfigurations) | **Post** /enterprise/getEnterpriseConfigurations | Get enterprise configuration profiles
[**EnterpriseGetEnterpriseEdges**](AllApi.md#EnterpriseGetEnterpriseEdges) | **Post** /enterprise/getEnterpriseEdges | Get edges associated with an enterprise
[**EnterpriseGetEnterpriseGatewayHandoff**](AllApi.md#EnterpriseGetEnterpriseGatewayHandoff) | **Post** /enterprise/getEnterpriseGatewayHandoff | Get enterprise gateway handoff configuration
[**EnterpriseGetEnterpriseNetworkAllocation**](AllApi.md#EnterpriseGetEnterpriseNetworkAllocation) | **Post** /enterprise/getEnterpriseNetworkAllocation | Get an enterprise network allocation
[**EnterpriseGetEnterpriseNetworkAllocations**](AllApi.md#EnterpriseGetEnterpriseNetworkAllocations) | **Post** /enterprise/getEnterpriseNetworkAllocations | Get all network allocation objects defined on an enterprise
[**EnterpriseGetEnterpriseNetworkSegments**](AllApi.md#EnterpriseGetEnterpriseNetworkSegments) | **Post** /enterprise/getEnterpriseNetworkSegments | Get all network segment objects defined on an enterprise
[**EnterpriseGetEnterpriseProperty**](AllApi.md#EnterpriseGetEnterpriseProperty) | **Post** /enterprise/getEnterpriseProperty | Get enterprise property
[**EnterpriseGetEnterpriseRouteConfiguration**](AllApi.md#EnterpriseGetEnterpriseRouteConfiguration) | **Post** /enterprise/getEnterpriseRouteConfiguration | Get route advertisement and routing preferences policy
[**EnterpriseGetEnterpriseRouteTable**](AllApi.md#EnterpriseGetEnterpriseRouteTable) | **Post** /enterprise/getEnterpriseRouteTable | Get the enterprise route table
[**EnterpriseGetEnterpriseServices**](AllApi.md#EnterpriseGetEnterpriseServices) | **Post** /enterprise/getEnterpriseServices | Get enterprise network service detail
[**EnterpriseGetEnterpriseUsers**](AllApi.md#EnterpriseGetEnterpriseUsers) | **Post** /enterprise/getEnterpriseUsers | Get list of enterprise users by enterprise id
[**EnterpriseInsertEnterprise**](AllApi.md#EnterpriseInsertEnterprise) | **Post** /enterprise/insertEnterprise | Create enterprise
[**EnterpriseInsertEnterpriseNetworkAllocation**](AllApi.md#EnterpriseInsertEnterpriseNetworkAllocation) | **Post** /enterprise/insertEnterpriseNetworkAllocation | Insert an enterprise network allocation
[**EnterpriseInsertEnterpriseNetworkSegment**](AllApi.md#EnterpriseInsertEnterpriseNetworkSegment) | **Post** /enterprise/insertEnterpriseNetworkSegment | Insert an enterprise network segment
[**EnterpriseInsertEnterpriseService**](AllApi.md#EnterpriseInsertEnterpriseService) | **Post** /enterprise/insertEnterpriseService | Insert a new enterprise service
[**EnterpriseInsertEnterpriseUser**](AllApi.md#EnterpriseInsertEnterpriseUser) | **Post** /enterprise/insertEnterpriseUser | Insert an enterprise user
[**EnterpriseInsertOrUpdateEnterpriseAlertConfigurations**](AllApi.md#EnterpriseInsertOrUpdateEnterpriseAlertConfigurations) | **Post** /enterprise/insertOrUpdateEnterpriseAlertConfigurations | Insert, update, or delete enterprise alert configurations
[**EnterpriseInsertOrUpdateEnterpriseCapability**](AllApi.md#EnterpriseInsertOrUpdateEnterpriseCapability) | **Post** /enterprise/insertOrUpdateEnterpriseCapability | Insert or update an enterprise capability
[**EnterpriseInsertOrUpdateEnterpriseGatewayHandoff**](AllApi.md#EnterpriseInsertOrUpdateEnterpriseGatewayHandoff) | **Post** /enterprise/insertOrUpdateEnterpriseGatewayHandoff | Insert or update an enterprise gateway handoff configuration
[**EnterpriseInsertOrUpdateEnterpriseProperty**](AllApi.md#EnterpriseInsertOrUpdateEnterpriseProperty) | **Post** /enterprise/insertOrUpdateEnterpriseProperty | Insert or update an enterprise property
[**EnterpriseProxyDeleteEnterpriseProxyUser**](AllApi.md#EnterpriseProxyDeleteEnterpriseProxyUser) | **Post** /enterpriseProxy/deleteEnterpriseProxyUser | Delete an enterprise proxy admin user
[**EnterpriseProxyGetEnterpriseProxyEdgeInventory**](AllApi.md#EnterpriseProxyGetEnterpriseProxyEdgeInventory) | **Post** /enterpriseProxy/getEnterpriseProxyEdgeInventory | Get a list of all partner enterprises and edge inventory associated with each enterprise
[**EnterpriseProxyGetEnterpriseProxyEnterprises**](AllApi.md#EnterpriseProxyGetEnterpriseProxyEnterprises) | **Post** /enterpriseProxy/getEnterpriseProxyEnterprises | Get a list of all partner enterprises
[**EnterpriseProxyGetEnterpriseProxyGatewayPools**](AllApi.md#EnterpriseProxyGetEnterpriseProxyGatewayPools) | **Post** /enterpriseProxy/getEnterpriseProxyGatewayPools | Get list of gateway pools
[**EnterpriseProxyGetEnterpriseProxyOperatorProfiles**](AllApi.md#EnterpriseProxyGetEnterpriseProxyOperatorProfiles) | **Post** /enterpriseProxy/getEnterpriseProxyOperatorProfiles | Get the operator profiles associated with a partner
[**EnterpriseProxyGetEnterpriseProxyUser**](AllApi.md#EnterpriseProxyGetEnterpriseProxyUser) | **Post** /enterpriseProxy/getEnterpriseProxyUser | Get an enterprise proxy user
[**EnterpriseProxyGetEnterpriseProxyUsers**](AllApi.md#EnterpriseProxyGetEnterpriseProxyUsers) | **Post** /enterpriseProxy/getEnterpriseProxyUsers | Get all enterprise proxy admin users
[**EnterpriseProxyInsertEnterpriseProxyEnterprise**](AllApi.md#EnterpriseProxyInsertEnterpriseProxyEnterprise) | **Post** /enterpriseProxy/insertEnterpriseProxyEnterprise | Insert a new enterprise owned by an MSP
[**EnterpriseProxyInsertEnterpriseProxyUser**](AllApi.md#EnterpriseProxyInsertEnterpriseProxyUser) | **Post** /enterpriseProxy/insertEnterpriseProxyUser | Create a new partner admin user
[**EnterpriseProxyUpdateEnterpriseProxyUser**](AllApi.md#EnterpriseProxyUpdateEnterpriseProxyUser) | **Post** /enterpriseProxy/updateEnterpriseProxyUser | Update an enterprise proxy admin user
[**EnterpriseSetEnterpriseAllAlertRecipients**](AllApi.md#EnterpriseSetEnterpriseAllAlertRecipients) | **Post** /enterprise/setEnterpriseAllAlertsRecipients | Set the recipients who should receive all alerts for an enterprise
[**EnterpriseUpdateEnterprise**](AllApi.md#EnterpriseUpdateEnterprise) | **Post** /enterprise/updateEnterprise | Update an enterprise
[**EnterpriseUpdateEnterpriseNetworkAllocation**](AllApi.md#EnterpriseUpdateEnterpriseNetworkAllocation) | **Post** /enterprise/updateEnterpriseNetworkAllocation | Update an enterprise network allocation
[**EnterpriseUpdateEnterpriseNetworkSegment**](AllApi.md#EnterpriseUpdateEnterpriseNetworkSegment) | **Post** /enterprise/updateEnterpriseNetworkSegment | Update an enterprise network segment
[**EnterpriseUpdateEnterpriseRoute**](AllApi.md#EnterpriseUpdateEnterpriseRoute) | **Post** /enterprise/updateEnterpriseRoute | Update an enterprise route
[**EnterpriseUpdateEnterpriseRouteConfiguration**](AllApi.md#EnterpriseUpdateEnterpriseRouteConfiguration) | **Post** /enterprise/updateEnterpriseRouteConfiguration | Update enterprise routing configuration
[**EnterpriseUpdateEnterpriseSecurityPolicy**](AllApi.md#EnterpriseUpdateEnterpriseSecurityPolicy) | **Post** /enterprise/updateEnterpriseSecurityPolicy | Update enterprise security policy
[**EnterpriseUpdateEnterpriseService**](AllApi.md#EnterpriseUpdateEnterpriseService) | **Post** /enterprise/updateEnterpriseService | Update an enterprise service
[**EnterpriseUserDeleteEnterpriseUser**](AllApi.md#EnterpriseUserDeleteEnterpriseUser) | **Post** /enterpriseUser/deleteEnterpriseUser | Delete an enterprise user.
[**EnterpriseUserGetEnterpriseUser**](AllApi.md#EnterpriseUserGetEnterpriseUser) | **Post** /enterpriseUser/getEnterpriseUser | Get an enterprise user
[**EnterpriseUserUpdateEnterpriseUser**](AllApi.md#EnterpriseUserUpdateEnterpriseUser) | **Post** /enterpriseUser/updateEnterpriseUser | Update an enterprise user
[**EventGetEnterpriseEvents**](AllApi.md#EventGetEnterpriseEvents) | **Post** /event/getEnterpriseEvents | Get Edge events
[**EventGetOperatorEvents**](AllApi.md#EventGetOperatorEvents) | **Post** /event/getOperatorEvents | Get operator events
[**FirewallGetEnterpriseFirewallLogs**](AllApi.md#FirewallGetEnterpriseFirewallLogs) | **Post** /firewall/getEnterpriseFirewallLogs | Get enterprise firewall logs
[**GatewayDeleteGateway**](AllApi.md#GatewayDeleteGateway) | **Post** /gateway/deleteGateway | Delete a gateway
[**GatewayGatewayProvision**](AllApi.md#GatewayGatewayProvision) | **Post** /gateway/gatewayProvision | Provision a gateway
[**GatewayGetGatewayEdgeAssignments**](AllApi.md#GatewayGetGatewayEdgeAssignments) | **Post** /gateway/getGatewayEdgeAssignments | Get edge assignments for a gateway
[**GatewayUpdateGatewayAttributes**](AllApi.md#GatewayUpdateGatewayAttributes) | **Post** /gateway/updateGatewayAttributes | Update gateway attributes
[**LinkQualityEventGetLinkQualityEvents**](AllApi.md#LinkQualityEventGetLinkQualityEvents) | **Post** /linkQualityEvent/getLinkQualityEvents | Get link quality data
[**LoginEnterpriseLogin**](AllApi.md#LoginEnterpriseLogin) | **Post** /login/enterpriseLogin | Authentication for non-operator users
[**LoginOperatorLogin**](AllApi.md#LoginOperatorLogin) | **Post** /login/operatorLogin | Authentication for an operator user
[**Logout**](AllApi.md#Logout) | **Post** /logout | Deactivate a given authorization cookie
[**Meta**](AllApi.md#Meta) | **Post** /meta/{apiPath} | Get meta-data on any other API call
[**MetricsGetEdgeAppLinkMetrics**](AllApi.md#MetricsGetEdgeAppLinkMetrics) | **Post** /metrics/getEdgeAppLinkMetrics | Get flow metric aggregate data by link
[**MetricsGetEdgeAppLinkSeries**](AllApi.md#MetricsGetEdgeAppLinkSeries) | **Post** /metrics/getEdgeAppLinkSeries | Get flow metric time series data by link
[**MetricsGetEdgeAppMetrics**](AllApi.md#MetricsGetEdgeAppMetrics) | **Post** /metrics/getEdgeAppMetrics | Get flow metric aggregate data by application
[**MetricsGetEdgeAppSeries**](AllApi.md#MetricsGetEdgeAppSeries) | **Post** /metrics/getEdgeAppSeries | Get flow metric time series data by application
[**MetricsGetEdgeCategoryMetrics**](AllApi.md#MetricsGetEdgeCategoryMetrics) | **Post** /metrics/getEdgeCategoryMetrics | Get flow metric aggregate data by application category
[**MetricsGetEdgeCategorySeries**](AllApi.md#MetricsGetEdgeCategorySeries) | **Post** /metrics/getEdgeCategorySeries | Get flow metric time series data by application category
[**MetricsGetEdgeDestMetrics**](AllApi.md#MetricsGetEdgeDestMetrics) | **Post** /metrics/getEdgeDestMetrics | Get flow metric aggregate data by destination
[**MetricsGetEdgeDestSeries**](AllApi.md#MetricsGetEdgeDestSeries) | **Post** /metrics/getEdgeDestSeries | Get flow metric time series data by destination
[**MetricsGetEdgeDeviceMetrics**](AllApi.md#MetricsGetEdgeDeviceMetrics) | **Post** /metrics/getEdgeDeviceMetrics | Get flow metric aggregate data by client device
[**MetricsGetEdgeDeviceSeries**](AllApi.md#MetricsGetEdgeDeviceSeries) | **Post** /metrics/getEdgeDeviceSeries | Get flow metric time series data by client device
[**MetricsGetEdgeLinkMetrics**](AllApi.md#MetricsGetEdgeLinkMetrics) | **Post** /metrics/getEdgeLinkMetrics | Get advanced flow metric aggregate data by link
[**MetricsGetEdgeLinkSeries**](AllApi.md#MetricsGetEdgeLinkSeries) | **Post** /metrics/getEdgeLinkSeries | Get advanced flow metric time series data by link
[**MetricsGetEdgeOsMetrics**](AllApi.md#MetricsGetEdgeOsMetrics) | **Post** /metrics/getEdgeOsMetrics | Get flow metric aggregate data by client OS
[**MetricsGetEdgeOsSeries**](AllApi.md#MetricsGetEdgeOsSeries) | **Post** /metrics/getEdgeOsSeries | Get flow metric time series data by client OS
[**MetricsGetEdgeSegmentMetrics**](AllApi.md#MetricsGetEdgeSegmentMetrics) | **Post** /metrics/getEdgeSegmentMetrics | Get flow metric aggregate data by segment Id
[**MetricsGetEdgeSegmentSeries**](AllApi.md#MetricsGetEdgeSegmentSeries) | **Post** /metrics/getEdgeSegmentSeries | Get flow metric time series data by segment id
[**MonitoringGetAggregateEdgeLinkMetrics**](AllApi.md#MonitoringGetAggregateEdgeLinkMetrics) | **Post** /monitoring/getAggregateEdgeLinkMetrics | Get aggregate Edge link metrics across enterprises
[**MonitoringGetAggregateEnterpriseEvents**](AllApi.md#MonitoringGetAggregateEnterpriseEvents) | **Post** /monitoring/getAggregateEnterpriseEvents | Get events across all enterprises
[**MonitoringGetAggregates**](AllApi.md#MonitoringGetAggregates) | **Post** /monitoring/getAggregates | Get aggregate enterprise and edge information
[**MonitoringGetEnterpriseBgpPeerStatus**](AllApi.md#MonitoringGetEnterpriseBgpPeerStatus) | **Post** /monitoring/getEnterpriseBgpPeerStatus | Get gateway BGP peer status for all enterprise gateways
[**MonitoringGetEnterpriseEdgeBgpPeerStatus**](AllApi.md#MonitoringGetEnterpriseEdgeBgpPeerStatus) | **Post** /monitoring/getEnterpriseEdgeBgpPeerStatus | Get edge BGP peer status for all enterprise edges
[**MonitoringGetEnterpriseEdgeLinkStatus**](AllApi.md#MonitoringGetEnterpriseEdgeLinkStatus) | **Post** /monitoring/getEnterpriseEdgeLinkStatus | Get edge and link status data
[**NetworkDeleteNetworkGatewayPool**](AllApi.md#NetworkDeleteNetworkGatewayPool) | **Post** /network/deleteNetworkGatewayPool | Delete gateway pool
[**NetworkGetNetworkConfigurations**](AllApi.md#NetworkGetNetworkConfigurations) | **Post** /network/getNetworkConfigurations | Get operator configuration profiles
[**NetworkGetNetworkEnterprises**](AllApi.md#NetworkGetNetworkEnterprises) | **Post** /network/getNetworkEnterprises | Get a list of the enterprises on a network
[**NetworkGetNetworkGatewayPools**](AllApi.md#NetworkGetNetworkGatewayPools) | **Post** /network/getNetworkGatewayPools | Get list of gateway pools
[**NetworkGetNetworkGateways**](AllApi.md#NetworkGetNetworkGateways) | **Post** /network/getNetworkGateways | Get list of gateways
[**NetworkGetNetworkOperatorUsers**](AllApi.md#NetworkGetNetworkOperatorUsers) | **Post** /network/getNetworkOperatorUsers | Get list of operator users for a network
[**NetworkInsertNetworkGatewayPool**](AllApi.md#NetworkInsertNetworkGatewayPool) | **Post** /network/insertNetworkGatewayPool | Insert a gateway pool
[**NetworkUpdateNetworkGatewayPoolAttributes**](AllApi.md#NetworkUpdateNetworkGatewayPoolAttributes) | **Post** /network/updateNetworkGatwayPoolAttributes | Update gateway pool attributes
[**OperatorUserDeleteOperatorUser**](AllApi.md#OperatorUserDeleteOperatorUser) | **Post** /operatorUser/deleteOperatorUser | Delete an operator user
[**OperatorUserGetOperatorUser**](AllApi.md#OperatorUserGetOperatorUser) | **Post** /operatorUser/getOperatorUser | Get an operator user
[**OperatorUserInsertOperatorUser**](AllApi.md#OperatorUserInsertOperatorUser) | **Post** /operatorUser/insertOperatorUser | Insert an operator user
[**OperatorUserUpdateOperatorUser**](AllApi.md#OperatorUserUpdateOperatorUser) | **Post** /operatorUser/updateOperatorUser | Update an operator user
[**RoleCreateRoleCustomization**](AllApi.md#RoleCreateRoleCustomization) | **Post** /role/createRoleCustomization | Create a role customization
[**RoleDeleteRoleCustomization**](AllApi.md#RoleDeleteRoleCustomization) | **Post** /role/deleteRoleCustomization | Delete a role customization
[**RoleGetUserTypeRoles**](AllApi.md#RoleGetUserTypeRoles) | **Post** /role/getUserTypeRoles | Get the roles defined for a user type
[**RoleSetEnterpriseDelegatedToEnterpriseProxy**](AllApi.md#RoleSetEnterpriseDelegatedToEnterpriseProxy) | **Post** /role/setEnterpriseDelegatedToEnterpriseProxy | Grant enterprise access to partner
[**RoleSetEnterpriseDelegatedToOperator**](AllApi.md#RoleSetEnterpriseDelegatedToOperator) | **Post** /role/setEnterpriseDelegatedToOperator | Grant enterprise access to network operator
[**RoleSetEnterpriseProxyDelegatedToOperator**](AllApi.md#RoleSetEnterpriseProxyDelegatedToOperator) | **Post** /role/setEnterpriseProxyDelegatedToOperator | Grant enterprise proxy access to network operator
[**RoleSetEnterpriseUserManagementDelegatedToOperator**](AllApi.md#RoleSetEnterpriseUserManagementDelegatedToOperator) | **Post** /role/setEnterpriseUserManagementDelegatedToOperator | Grant enterprise user access to the network operator
[**SystemPropertyGetSystemProperties**](AllApi.md#SystemPropertyGetSystemProperties) | **Post** /systemProperty/getSystemProperties | Get all system properties
[**SystemPropertyGetSystemProperty**](AllApi.md#SystemPropertyGetSystemProperty) | **Post** /systemProperty/getSystemProperty | Get system property
[**SystemPropertyInsertOrUpdateSystemProperty**](AllApi.md#SystemPropertyInsertOrUpdateSystemProperty) | **Post** /systemProperty/insertOrUpdateSystemProperty | Insert or update a system property
[**SystemPropertyInsertSystemProperty**](AllApi.md#SystemPropertyInsertSystemProperty) | **Post** /systemProperty/insertSystemProperty | Insert a system property
[**SystemPropertyUpdateSystemProperty**](AllApi.md#SystemPropertyUpdateSystemProperty) | **Post** /systemProperty/updateSystemProperty | Update a system property
[**VcoInventoryAssociateEdge**](AllApi.md#VcoInventoryAssociateEdge) | **Post** /vcoInventory/associateEdge | Return inventory items available at this VCO
[**VcoInventoryGetInventoryItems**](AllApi.md#VcoInventoryGetInventoryItems) | **Post** /vcoInventory/getInventoryItems | Return inventory items available at this VCO


# **ConfigurationCloneAndConvertConfiguration**
> ConfigurationCloneAndConvertConfigurationResult ConfigurationCloneAndConvertConfiguration(ctx, body)
Clone and convert a network based profile configuration to segment based profile configuration

Clones an convert existing network configuration by configurationId. Accepts an enterpriseId or networkId to associate the new config with an enterprise or network. On success, returns an object the ID of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationCloneAndConvertConfiguration**](ConfigurationCloneAndConvertConfiguration.md)|  | 

### Return type

[**ConfigurationCloneAndConvertConfigurationResult**](configuration_clone_and_convert_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationCloneConfiguration**
> ConfigurationCloneConfigurationResult ConfigurationCloneConfiguration(ctx, body)
Clone configuration profile

Clones the specified configuration (by `configurationId`) and all associated configuration modules. Accepts an `enterpriseId` or `networkId` to associate the new configuration with an enterprise or network. Select modules may also be specified. On success, returns the `id` of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationCloneConfiguration**](ConfigurationCloneConfiguration.md)|  | 

### Return type

[**ConfigurationCloneConfigurationResult**](configuration_clone_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationCloneEnterpriseTemplate**
> ConfigurationCloneEnterpriseTemplateResult ConfigurationCloneEnterpriseTemplate(ctx, body)
Clone default enterprise configuration profile

Creates a new enterprise configuration from the enterprise default configuration. On success, returns the `id` of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationCloneEnterpriseTemplate**](ConfigurationCloneEnterpriseTemplate.md)|  | 

### Return type

[**ConfigurationCloneEnterpriseTemplateResult**](configuration_clone_enterprise_template_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationDeleteConfiguration**
> ConfigurationDeleteConfigurationResult ConfigurationDeleteConfiguration(ctx, body)
Delete a configuration profile

Delete an existing configuration profile. On success, returns an object indicating the number of rows deleted.  Privileges required:  `DELETE` `ENTERPRISE_PROFILE`, or  `DELETE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationDeleteConfiguration**](ConfigurationDeleteConfiguration.md)|  | 

### Return type

[**ConfigurationDeleteConfigurationResult**](configuration_delete_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationGetConfiguration**
> ConfigurationGetConfigurationResult ConfigurationGetConfiguration(ctx, body)
Get a configuration profile

Get a configuration profile, optionally with module detail.  Privileges required:  `READ` `ENTERPRISE_PROFILE`, or  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationGetConfiguration**](ConfigurationGetConfiguration.md)|  | 

### Return type

[**ConfigurationGetConfigurationResult**](configuration_get_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationGetConfigurationModules**
> []ConfigurationGetConfigurationModulesResultItem ConfigurationGetConfigurationModules(ctx, body)
List the modules that compose a configuration profile

Retrieve a list of the configuration modules that compose the given configuration profile.  Privileges required:  `READ` `ENTERPRISE_PROFILE`, or  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationGetConfigurationModules**](ConfigurationGetConfigurationModules.md)|  | 

### Return type

[**[]ConfigurationGetConfigurationModulesResultItem**](configuration_get_configuration_modules_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationGetRoutableApplications**
> ConfigurationGetRoutableApplicationsResult ConfigurationGetRoutableApplications(ctx, body)
Get first packet routable applications

Gets all applications that are first packet routable. If called from an operator or MSP context, then `enterpriseId` is required. Optionally, specify `edgeId` to get the map for a specific Edge.  Privileges required:  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationGetRoutableApplications**](ConfigurationGetRoutableApplications.md)|  | 

### Return type

[**ConfigurationGetRoutableApplicationsResult**](configuration_get_routable_applications_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationInsertConfigurationModule**
> ConfigurationInsertConfigurationModuleResult ConfigurationInsertConfigurationModule(ctx, body)
Insert a new configuration module

Insert a new configuration module into the given configuration profile.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`, or  `UPDATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationInsertConfigurationModule**](ConfigurationInsertConfigurationModule.md)|  | 

### Return type

[**ConfigurationInsertConfigurationModuleResult**](configuration_insert_configuration_module_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationUpdateConfigurationModule**
> ConfigurationUpdateConfigurationModuleResult ConfigurationUpdateConfigurationModule(ctx, body)
Update a configuration module

Update an existing configuration module with the data. module data contained in the _update object.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`, or  `UPDATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationUpdateConfigurationModule**](ConfigurationUpdateConfigurationModule.md)|  | 

### Return type

[**ConfigurationUpdateConfigurationModuleResult**](configuration_update_configuration_module_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryConfigureActiveForReplication**
> DisasterRecoveryConfigureActiveForReplicationResult DisasterRecoveryConfigureActiveForReplication(ctx, body)
Designate a standby Orchestrator for disaster recovery replication

Configure the current Orchestrator to be active and the specified Orchestrator to be standby for Orchestrator disaster recovery replication. Required attributes are 1) standbyList, a single-entry array containing the standbyAddress and standbyUuid, 2) drVCOUser, a Orchestrator super user available on both the active and standby VCOs, and 3) drVCOPassword, the password of drVCOUser on the standby Orchestrator (unless the autoConfigStandby option is specified as false). The call sets up the active Orchestrator to allow replication from the standby and then (unless autoConfigStandby is false) makes a transitionToStandby API call to the specified standby, expected to have been previously placed in STANDBY_CANDIDATE state via prepareForStandby.  After this call, the active and standby VCOs should be polled via getReplicationStatus until they  both reach STANDBY_RUNNING drState (or a configuration error is reported).  (Note: the drVCOPassword is not persisted.)  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryConfigureActiveForReplication**](DisasterRecoveryConfigureActiveForReplication.md)|  | 

### Return type

[**DisasterRecoveryConfigureActiveForReplicationResult**](disaster_recovery_configure_active_for_replication_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryDemoteActive**
> DisasterRecoveryDemoteActiveResult DisasterRecoveryDemoteActive(ctx, body)
Demote current server from active to zombie

No input parameters are required.  The active server is expected to be in the drState FAILURE_GET_STANDBY_STATUS or FAILURE_MYSQL_ACTIVE_STATUS, meaning that DR protection had been engaged (with the last successful replication status observed at lastDRProtectedTime) but that active failed a health check since that time.  If the active server is in the drState STANDBY_RUNNING, meaning that it has detected no problems in interacting with the standby server, the operator can force demotion of the active using the optional parameter force passed with value of true; in this case, the operator must ensure the standby server has already been successfully promoted.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryDemoteActive**](DisasterRecoveryDemoteActive.md)|  | 

### Return type

[**DisasterRecoveryDemoteActiveResult**](disaster_recovery_demote_active_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryGetReplicationBlob**
> DisasterRecoveryGetReplicationBlobResult DisasterRecoveryGetReplicationBlob(ctx, body)
Get the blob needed to configure replication on the standby

Get from the active Orchestrator the blob needed to configure replication on the standby. Only used when configureActiveForReplication was called with autoConfigStandby set to false [true by default].  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryGetReplicationBlob**](DisasterRecoveryGetReplicationBlob.md)|  | 

### Return type

[**DisasterRecoveryGetReplicationBlobResult**](disaster_recovery_get_replication_blob_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryGetReplicationStatus**
> DisasterRecoveryGetReplicationStatusResult DisasterRecoveryGetReplicationStatus(ctx, body)
Get disaster recovery status

Get disaster recovery replication status, optionally with client contact, state transition history, and storage information.  No input parameters are required.  Can optionally specify 1 or more of the following with parameters: clientContact,clientCount,stateHistory,storageInfo.  Privileges required:  `READ` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryGetReplicationStatus**](DisasterRecoveryGetReplicationStatus.md)|  | 

### Return type

[**DisasterRecoveryGetReplicationStatusResult**](disaster_recovery_get_replication_status_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryPrepareForStandby**
> DisasterRecoveryPrepareForStandbyResult DisasterRecoveryPrepareForStandby(ctx, body)
Prepare current Orchestrator to be configured as a standby system

Transitions the current Orchestrator to a quiesced state, ready to be configured as a standby system. No input parameters are required.  After this call, the Orchestrator will be restarted in standby mode. The caller should subsequently poll `getReplicationStatus` until `drState` is `STANDBY_CANDIDATE`.  This is the first step in configuring Orchestrator disaster recovery.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryPrepareForStandby**](DisasterRecoveryPrepareForStandby.md)|  | 

### Return type

[**DisasterRecoveryPrepareForStandbyResult**](disaster_recovery_prepare_for_standby_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryPromoteStandbyToActive**
> DisasterRecoveryPromoteStandbyToActiveResult DisasterRecoveryPromoteStandbyToActive(ctx, body)
Promote the current server to take over as the single standalone VCO

The current server is expected to be a standby in the drState FAILURE_MYSQL_STANDBY_STATUS, meaning that DR protection had been engaged (with the last successful replication status observed at lastDRProtectedTime) but that standby has been unable to replicate since that time. If the standby server is in the drState STANDBY_RUNNING, meaning that it has detected no problems in replicating from the active server, the operator can force promotion of the standby using the optional parameter force passed with value of true; in this case, the standby server will call demoteActive/force on the active.  The operator should, if possible, ensure the formerly active server is demoted by running demoteServer directly on that server if the standby server was unable to do so successfully.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryPromoteStandbyToActive**](DisasterRecoveryPromoteStandbyToActive.md)|  | 

### Return type

[**DisasterRecoveryPromoteStandbyToActiveResult**](disaster_recovery_promote_standby_to_active_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryRemoveStandby**
> DisasterRecoveryRemoveStandbyResult DisasterRecoveryRemoveStandby(ctx, body)
Unconfigure disaster recovery on the current server

Unconfigure disaster recovery on the current server.  Also, make a best-effort call to removeStandby on the paired DR server. No input parameters are required.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryRemoveStandby**](DisasterRecoveryRemoveStandby.md)|  | 

### Return type

[**DisasterRecoveryRemoveStandbyResult**](disaster_recovery_remove_standby_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryTransitionToStandby**
> DisasterRecoveryTransitionToStandbyResult DisasterRecoveryTransitionToStandby(ctx, body)
Configure current Orchestrator to transition to standby in disaster recovery active/standby pair.

Configure current Orchestrator to transition to standby in disaster recovery active/standby pair. Requires parameter activeAccessFromStandby, which contains the data needed to configure standby. This data is produced by configureActiveForReplication, which by default, automatically calls transitionToStandby; an explicit call is only needed, with a blob obtained from getReplicationBlob, if configureActiveForReplication is called with autoConfigStandby set false.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryTransitionToStandby**](DisasterRecoveryTransitionToStandby.md)|  | 

### Return type

[**DisasterRecoveryTransitionToStandbyResult**](disaster_recovery_transition_to_standby_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeDeleteEdge**
> []EdgeDeleteEdgeResultItem EdgeDeleteEdge(ctx, body)
Delete an edge

Delete an edge by id.  Privileges required:  `DELETE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeDeleteEdge**](EdgeDeleteEdge.md)|  | 

### Return type

[**[]EdgeDeleteEdgeResultItem**](edge_delete_edge_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeDeleteEdgeBgpNeighborRecords**
> EdgeDeleteEdgeBgpNeighborRecordsResult EdgeDeleteEdgeBgpNeighborRecords(ctx, body)
Delete edge BGP neighbor records

Deletes BGP record(s) matching the given record keys (neighborIp) on the edges with the given IDs, if they exist.  Privileges required:  `DELETE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeDeleteEdgeBgpNeighborRecords**](EdgeDeleteEdgeBgpNeighborRecords.md)|  | 

### Return type

[**EdgeDeleteEdgeBgpNeighborRecordsResult**](edge_delete_edge_bgp_neighbor_records_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeEdgeCancelReactivation**
> EdgeEdgeCancelReactivationResult EdgeEdgeCancelReactivation(ctx, body)
Cancel a pending edge reactivation request

Cancel a pending reactivation edge reactivation request.  Privileges required:  `CREATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeEdgeCancelReactivation**](EdgeEdgeCancelReactivation.md)|  | 

### Return type

[**EdgeEdgeCancelReactivationResult**](edge_edge_cancel_reactivation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeEdgeProvision**
> EdgeEdgeProvisionResult EdgeEdgeProvision(ctx, body)
Provision an edge

Provision an edge prior to activation.  Privileges required:  `CREATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeEdgeProvision**](EdgeEdgeProvision.md)|  | 

### Return type

[**EdgeEdgeProvisionResult**](edge_edge_provision_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeEdgeRequestReactivation**
> EdgeEdgeRequestReactivationResult EdgeEdgeRequestReactivation(ctx, body)
Reactivate an edge

Update activation state for an edge to REACTIVATION_PENDING.  Privileges required:  `CREATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeEdgeRequestReactivation**](EdgeEdgeRequestReactivation.md)|  | 

### Return type

[**EdgeEdgeRequestReactivationResult**](edge_edge_request_reactivation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeGetClientVisibilityMode**
> EdgeGetClientVisibilityModeResult EdgeGetClientVisibilityMode(ctx, body)
Get an edge's client visibility mode

Retrieve an edge's client visibility mode.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeGetClientVisibilityMode**](EdgeGetClientVisibilityMode.md)|  | 

### Return type

[**EdgeGetClientVisibilityModeResult**](edge_get_client_visibility_mode_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeGetEdge**
> EdgeGetEdgeResult EdgeGetEdge(ctx, body)
Get edge

Gets the specified Edge with optional link, site, configuration, certificate, or enterprise details. Supports queries by Edge `id`, `deviceId`, `activationKey`, and `logicalId`.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeGetEdge**](EdgeGetEdge.md)|  | 

### Return type

[**EdgeGetEdgeResult**](edge_get_edge_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeGetEdgeConfigurationStack**
> []EdgeGetEdgeConfigurationStackResultItem EdgeGetEdgeConfigurationStack(ctx, body)
Get an edge's configuration stack

Retrieve an edge's complete configuration profile, with all modules included.  Privileges required:  `READ` `EDGE`  `READ` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeGetEdgeConfigurationStack**](EdgeGetEdgeConfigurationStack.md)|  | 

### Return type

[**[]EdgeGetEdgeConfigurationStackResultItem**](edge_get_edge_configuration_stack_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeSetEdgeEnterpriseConfiguration**
> EdgeSetEdgeEnterpriseConfigurationResult EdgeSetEdgeEnterpriseConfiguration(ctx, body)
Apply an enterprise configuration to an Edge

Sets the enterprise configuration for the specified Edge (by `edgeId`).  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeSetEdgeEnterpriseConfiguration**](EdgeSetEdgeEnterpriseConfiguration.md)|  | 

### Return type

[**EdgeSetEdgeEnterpriseConfigurationResult**](edge_set_edge_enterprise_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeSetEdgeHandOffGateways**
> EdgeSetEdgeHandOffGatewaysResult EdgeSetEdgeHandOffGateways(ctx, body)
Set an edge's on-premise hand off gateways

Set an edge's on-premise hand off gateways. A primary and secondary gateway are defined, primary is required, secondary is optional. All existing edge-gateway hand off relationships are moved and are replaced by the the specified primary and secondary gateways.  Privileges required:  `UPDATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeSetEdgeHandOffGateways**](EdgeSetEdgeHandOffGateways.md)|  | 

### Return type

[**EdgeSetEdgeHandOffGatewaysResult**](edge_set_edge_hand_off_gateways_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeSetEdgeOperatorConfiguration**
> EdgeSetEdgeOperatorConfigurationResult EdgeSetEdgeOperatorConfiguration(ctx, body)
Apply an operator configuration to an Edge

Set an Edge's operator configuration. This overrides any enterprise-assigned operator configuration and the network default operator configuration.  Privileges required:  `UPDATE` `EDGE`  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeSetEdgeOperatorConfiguration**](EdgeSetEdgeOperatorConfiguration.md)|  | 

### Return type

[**EdgeSetEdgeOperatorConfigurationResult**](edge_set_edge_operator_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeUpdateEdgeAdminPassword**
> EdgeUpdateEdgeAdminPasswordResult EdgeUpdateEdgeAdminPassword(ctx, body)
Update edge's local UI authentication credentials

Request an update to the edge's local UI authentication credentials. On success, returns a JSON object with the ID of the action queued, status for which can be queried using the edgeAction/getEdgeAction API  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_KEYS`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeUpdateEdgeAdminPassword**](EdgeUpdateEdgeAdminPassword.md)|  | 

### Return type

[**EdgeUpdateEdgeAdminPasswordResult**](edge_update_edge_admin_password_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeUpdateEdgeAttributes**
> EdgeUpdateEdgeAttributesResult EdgeUpdateEdgeAttributes(ctx, body)
Update edge attributes

Update basic edge attributes, including edge name, description, site information, or serial number.  Privileges required:  `UPDATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeUpdateEdgeAttributes**](EdgeUpdateEdgeAttributes.md)|  | 

### Return type

[**EdgeUpdateEdgeAttributesResult**](edge_update_edge_attributes_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeUpdateEdgeCredentialsByConfiguration**
> EdgeUpdateEdgeCredentialsByConfigurationResult EdgeUpdateEdgeCredentialsByConfiguration(ctx, body)
Update edge UI credentials by configuration id

Request an update to the edge-local UI authentication credentials for all edges belonging to a configuration profile. On success, returns a JSON object containing a list of each of the action IDs queued.  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_KEYS`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeUpdateEdgeCredentialsByConfiguration**](EdgeUpdateEdgeCredentialsByConfiguration.md)|  | 

### Return type

[**EdgeUpdateEdgeCredentialsByConfigurationResult**](edge_update_edge_credentials_by_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseDeleteEnterprise**
> EnterpriseDeleteEnterpriseResult EnterpriseDeleteEnterprise(ctx, body)
Delete an enterprise

Delete the enterprise specified by the given id parameter. enterpriseId is also a valid alias for id.  Privileges required:  `DELETE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseDeleteEnterprise**](EnterpriseDeleteEnterprise.md)|  | 

### Return type

[**EnterpriseDeleteEnterpriseResult**](enterprise_delete_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseDeleteEnterpriseGatewayRecords**
> EnterpriseDeleteEnterpriseGatewayRecordsResult EnterpriseDeleteEnterpriseGatewayRecords(ctx, body)
Delete enterprise gateway record(s)

Delete the enterprise gateway record(s) matching the given gateway id(s) and neighbor IP addresses.  Privileges required:  `DELETE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseDeleteEnterpriseGatewayRecords**](EnterpriseDeleteEnterpriseGatewayRecords.md)|  | 

### Return type

[**EnterpriseDeleteEnterpriseGatewayRecordsResult**](enterprise_delete_enterprise_gateway_records_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseDeleteEnterpriseNetworkAllocation**
> EnterpriseDeleteEnterpriseNetworkAllocationResult EnterpriseDeleteEnterpriseNetworkAllocation(ctx, body)
Delete an enterprise network allocation

Delete an enterprise network allocation, by id.  Privileges required:  `DELETE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseDeleteEnterpriseNetworkAllocation**](EnterpriseDeleteEnterpriseNetworkAllocation.md)|  | 

### Return type

[**EnterpriseDeleteEnterpriseNetworkAllocationResult**](enterprise_delete_enterprise_network_allocation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseDeleteEnterpriseService**
> EnterpriseDeleteEnterpriseServiceResult EnterpriseDeleteEnterpriseService(ctx, body)
Delete an enterprise service

Delete an enterprise service, by id.  Privileges required:  `DELETE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseDeleteEnterpriseService**](EnterpriseDeleteEnterpriseService.md)|  | 

### Return type

[**EnterpriseDeleteEnterpriseServiceResult**](enterprise_delete_enterprise_service_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterprise**
> EnterpriseGetEnterpriseResult EnterpriseGetEnterprise(ctx, body)
Get enterprise

Retrieve enterprise data, with optional proxy (partner) detail.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterprise**](EnterpriseGetEnterprise.md)|  | 

### Return type

[**EnterpriseGetEnterpriseResult**](enterprise_get_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseAddresses**
> []EnterpriseGetEnterpriseAddressesResultItem EnterpriseGetEnterpriseAddresses(ctx, body)
Get enterprise IP address information

Retrieve the public IP address information for the management and control entities associated with this enterprise, including Orchestrator(s), Gateway(s), and datacenter(s).  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseAddresses**](EnterpriseGetEnterpriseAddresses.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseAddressesResultItem**](enterprise_get_enterprise_addresses_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseAlertConfigurations**
> []EnterpriseGetEnterpriseAlertConfigurationsResultItem EnterpriseGetEnterpriseAlertConfigurations(ctx, body)
Get the enterprise alert configuration

Get the alert configurations associated with an enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseAlertConfigurations**](EnterpriseGetEnterpriseAlertConfigurations.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseAlertConfigurationsResultItem**](enterprise_get_enterprise_alert_configurations_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseAlerts**
> EnterpriseGetEnterpriseAlertsResult EnterpriseGetEnterpriseAlerts(ctx, body)
Get triggered enterprise alerts

Gets past triggered alerts for the specified enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseAlerts**](EnterpriseGetEnterpriseAlerts.md)|  | 

### Return type

[**EnterpriseGetEnterpriseAlertsResult**](enterprise_get_enterprise_alerts_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseAllAlertRecipients**
> EnterpriseGetEnterpriseAllAlertRecipientsResult EnterpriseGetEnterpriseAllAlertRecipients(ctx, body)
List recipients currently receiving ALL enterprise alerts

List the recipients currently configured to receive all alerts for an enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseAllAlertRecipients**](EnterpriseGetEnterpriseAllAlertRecipients.md)|  | 

### Return type

[**EnterpriseGetEnterpriseAllAlertRecipientsResult**](enterprise_get_enterprise_all_alert_recipients_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseCapabilities**
> EnterpriseGetEnterpriseCapabilitiesResult EnterpriseGetEnterpriseCapabilities(ctx, body)
Get enterprise capabilities

Retrieve a list of the enterprise capabilities currently enabled/disabled on an enterprise (e.g. BGP, COS mapping, PKI, etc.)  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseCapabilities**](EnterpriseGetEnterpriseCapabilities.md)|  | 

### Return type

[**EnterpriseGetEnterpriseCapabilitiesResult**](enterprise_get_enterprise_capabilities_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseConfigurations**
> []EnterpriseGetEnterpriseConfigurationsResultItem EnterpriseGetEnterpriseConfigurations(ctx, body)
Get enterprise configuration profiles

Retrieve a list of configuration profiles existing on an enterprise, with optional edge and/or module detail.  Privileges required:  `READ` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseConfigurations**](EnterpriseGetEnterpriseConfigurations.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseConfigurationsResultItem**](enterprise_get_enterprise_configurations_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseEdges**
> []EnterpriseGetEnterpriseEdgesResultItem EnterpriseGetEnterpriseEdges(ctx, body)
Get edges associated with an enterprise

Gets all Edges associated with the specified enterprise, including optional site, link, and configuration details.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseEdges**](EnterpriseGetEnterpriseEdges.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseEdgesResultItem**](enterprise_get_enterprise_edges_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseGatewayHandoff**
> EnterpriseGetEnterpriseGatewayHandoffResult EnterpriseGetEnterpriseGatewayHandoff(ctx, body)
Get enterprise gateway handoff configuration

Get enterprise gateway handoff configuration.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseGatewayHandoff**](EnterpriseGetEnterpriseGatewayHandoff.md)|  | 

### Return type

[**EnterpriseGetEnterpriseGatewayHandoffResult**](enterprise_get_enterprise_gateway_handoff_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseNetworkAllocation**
> EnterpriseGetEnterpriseNetworkAllocationResult EnterpriseGetEnterpriseNetworkAllocation(ctx, body)
Get an enterprise network allocation

Retrieve a network allocation object by id.  Privileges required:  `READ` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseNetworkAllocation**](EnterpriseGetEnterpriseNetworkAllocation.md)|  | 

### Return type

[**EnterpriseGetEnterpriseNetworkAllocationResult**](enterprise_get_enterprise_network_allocation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseNetworkAllocations**
> []EnterpriseGetEnterpriseNetworkAllocationsResultItem EnterpriseGetEnterpriseNetworkAllocations(ctx, body)
Get all network allocation objects defined on an enterprise

Retrieve a list of all of the network allocations defined onthe given enterprise.  Privileges required:  `READ` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseNetworkAllocations**](EnterpriseGetEnterpriseNetworkAllocations.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseNetworkAllocationsResultItem**](enterprise_get_enterprise_network_allocations_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseNetworkSegments**
> []EnterpriseGetEnterpriseNetworkSegmentsResultItem EnterpriseGetEnterpriseNetworkSegments(ctx, body)
Get all network segment objects defined on an enterprise

Retrieve a list of all of the network segments defined forthe given enterprise.  Privileges required:  `READ` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseNetworkSegments**](EnterpriseGetEnterpriseNetworkSegments.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseNetworkSegmentsResultItem**](enterprise_get_enterprise_network_segments_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseProperty**
> EnterpriseGetEnterprisePropertyResult EnterpriseGetEnterpriseProperty(ctx, body)
Get enterprise property

Get a enterprise property by object id or other attribute.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseProperty**](EnterpriseGetEnterpriseProperty.md)|  | 

### Return type

[**EnterpriseGetEnterprisePropertyResult**](enterprise_get_enterprise_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseRouteConfiguration**
> EnterpriseGetEnterpriseRouteConfigurationResult EnterpriseGetEnterpriseRouteConfiguration(ctx, body)
Get route advertisement and routing preferences policy

Get enterprise route advertisement, routing peferences and OSPF, BGP advertisement policy as configured in the Overlay Flow Control table.  Privileges required:  `READ` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseRouteConfiguration**](EnterpriseGetEnterpriseRouteConfiguration.md)|  | 

### Return type

[**EnterpriseGetEnterpriseRouteConfigurationResult**](enterprise_get_enterprise_route_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseRouteTable**
> EnterpriseGetEnterpriseRouteTableResult EnterpriseGetEnterpriseRouteTable(ctx, body)
Get the enterprise route table

Get composite enterprise route table, optionally scoped by profile(s). The returned routes include static routes, directly connected routes and learned routes.  Privileges required:  `READ` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseRouteTable**](EnterpriseGetEnterpriseRouteTable.md)|  | 

### Return type

[**EnterpriseGetEnterpriseRouteTableResult**](enterprise_get_enterprise_route_table_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseServices**
> []EnterpriseGetEnterpriseServicesResultItem EnterpriseGetEnterpriseServices(ctx, body)
Get enterprise network service detail

Get the network service JSON objects defined for an enterprise.  Privileges required:  `READ` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseServices**](EnterpriseGetEnterpriseServices.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseServicesResultItem**](enterprise_get_enterprise_services_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseUsers**
> []EnterpriseGetEnterpriseUsersResultItem EnterpriseGetEnterpriseUsers(ctx, body)
Get list of enterprise users by enterprise id

undefined  Privileges required:  `READ` `ENTERPRISE`  `READ` `ENTERPRISE_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseUsers**](EnterpriseGetEnterpriseUsers.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseUsersResultItem**](enterprise_get_enterprise_users_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterprise**
> EnterpriseInsertEnterpriseResult EnterpriseInsertEnterprise(ctx, body)
Create enterprise

Creates a new enterprise, which is owned by the operator.  Privileges required:  `CREATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterprise**](EnterpriseInsertEnterprise.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseResult**](enterprise_insert_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterpriseNetworkAllocation**
> EnterpriseInsertEnterpriseNetworkAllocationResult EnterpriseInsertEnterpriseNetworkAllocation(ctx, body)
Insert an enterprise network allocation

Insert a new enterprise network allocation.  Privileges required:  `CREATE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterpriseNetworkAllocation**](EnterpriseInsertEnterpriseNetworkAllocation.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseNetworkAllocationResult**](enterprise_insert_enterprise_network_allocation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterpriseNetworkSegment**
> EnterpriseInsertEnterpriseNetworkSegmentResult EnterpriseInsertEnterpriseNetworkSegment(ctx, body)
Insert an enterprise network segment

Insert a new enterprise network segment.  Privileges required:  `CREATE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterpriseNetworkSegment**](EnterpriseInsertEnterpriseNetworkSegment.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseNetworkSegmentResult**](enterprise_insert_enterprise_network_segment_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterpriseService**
> EnterpriseInsertEnterpriseServiceResult EnterpriseInsertEnterpriseService(ctx, body)
Insert a new enterprise service

Insert a new enterprise service.  Privileges required:  `CREATE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterpriseService**](EnterpriseInsertEnterpriseService.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseServiceResult**](enterprise_insert_enterprise_service_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterpriseUser**
> EnterpriseInsertEnterpriseUserResult EnterpriseInsertEnterpriseUser(ctx, body)
Insert an enterprise user

Insert an enterprise user.  Privileges required:  `CREATE` `ENTERPRISE_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NewEnterpriseUser**](NewEnterpriseUser.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseUserResult**](enterprise_insert_enterprise_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertOrUpdateEnterpriseAlertConfigurations**
> EnterpriseInsertOrUpdateEnterpriseAlertConfigurationsResult EnterpriseInsertOrUpdateEnterpriseAlertConfigurations(ctx, body)
Insert, update, or delete enterprise alert configurations

Insert, update, or delete enterprise alert configurations. Returns the array of alert configurations submitted, with ids added for the entries that have been successfully inserted. If an entry is not successfully inserted or updated, an `error` property is included in the .  Privileges required:  `CREATE` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertOrUpdateEnterpriseAlertConfigurations**](EnterpriseInsertOrUpdateEnterpriseAlertConfigurations.md)|  | 

### Return type

[**EnterpriseInsertOrUpdateEnterpriseAlertConfigurationsResult**](enterprise_insert_or_update_enterprise_alert_configurations_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertOrUpdateEnterpriseCapability**
> EnterpriseInsertOrUpdateEnterpriseCapabilityResult EnterpriseInsertOrUpdateEnterpriseCapability(ctx, body)
Insert or update an enterprise capability

Insert or update an enterprise capability.  Privileges required:  `UPDATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertOrUpdateEnterpriseCapability**](EnterpriseInsertOrUpdateEnterpriseCapability.md)|  | 

### Return type

[**EnterpriseInsertOrUpdateEnterpriseCapabilityResult**](enterprise_insert_or_update_enterprise_capability_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertOrUpdateEnterpriseGatewayHandoff**
> EnterpriseInsertOrUpdateEnterpriseGatewayHandoffResult EnterpriseInsertOrUpdateEnterpriseGatewayHandoff(ctx, body)
Insert or update an enterprise gateway handoff configuration

Insert or update an enterprise gateway handoff configuration.  Privileges required:  `UPDATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertOrUpdateEnterpriseGatewayHandoff**](EnterpriseInsertOrUpdateEnterpriseGatewayHandoff.md)|  | 

### Return type

[**EnterpriseInsertOrUpdateEnterpriseGatewayHandoffResult**](enterprise_insert_or_update_enterprise_gateway_handoff_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertOrUpdateEnterpriseProperty**
> EnterpriseInsertOrUpdateEnterprisePropertyResult EnterpriseInsertOrUpdateEnterpriseProperty(ctx, body)
Insert or update an enterprise property

Insert a enterprise property. If property with the given name already exists, the property will be updated.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertOrUpdateEnterpriseProperty**](EnterpriseInsertOrUpdateEnterpriseProperty.md)|  | 

### Return type

[**EnterpriseInsertOrUpdateEnterprisePropertyResult**](enterprise_insert_or_update_enterprise_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyDeleteEnterpriseProxyUser**
> EnterpriseProxyDeleteEnterpriseProxyUserResult EnterpriseProxyDeleteEnterpriseProxyUser(ctx, body)
Delete an enterprise proxy admin user

Delete an enterprise proxy user by id or username. Note that `enterpriseProxyId` is a required parameter when invoking this method as an operator or partner user.  Privileges required:  `DELETE` `PROXY_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyDeleteEnterpriseProxyUser**](EnterpriseProxyDeleteEnterpriseProxyUser.md)|  | 

### Return type

[**EnterpriseProxyDeleteEnterpriseProxyUserResult**](enterprise_proxy_delete_enterprise_proxy_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyEdgeInventory**
> []EnterpriseProxyGetEnterpriseProxyEdgeInventoryResultItem EnterpriseProxyGetEnterpriseProxyEdgeInventory(ctx, body)
Get a list of all partner enterprises and edge inventory associated with each enterprise

Get  partner enterprises and their edge inventory.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyEdgeInventory**](EnterpriseProxyGetEnterpriseProxyEdgeInventory.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyEdgeInventoryResultItem**](enterprise_proxy_get_enterprise_proxy_edge_inventory_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyEnterprises**
> []EnterpriseProxyGetEnterpriseProxyEnterprisesResultItem EnterpriseProxyGetEnterpriseProxyEnterprises(ctx, body)
Get a list of all partner enterprises

Get all partner enterprises, optionally including all edges or edge counts.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyEnterprises**](EnterpriseProxyGetEnterpriseProxyEnterprises.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyEnterprisesResultItem**](enterprise_proxy_get_enterprise_proxy_enterprises_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyGatewayPools**
> []EnterpriseProxyGetEnterpriseProxyGatewayPoolsResultItem EnterpriseProxyGetEnterpriseProxyGatewayPools(ctx, body)
Get list of gateway pools

Get list of gateway pools associated with an enterprise proxy, optionally with lists of gateways or enterprises belonging to each pool.  Privileges required:  `READ` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyGatewayPools**](EnterpriseProxyGetEnterpriseProxyGatewayPools.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyGatewayPoolsResultItem**](enterprise_proxy_get_enterprise_proxy_gateway_pools_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyOperatorProfiles**
> []EnterpriseProxyGetEnterpriseProxyOperatorProfilesResultItem EnterpriseProxyGetEnterpriseProxyOperatorProfiles(ctx, body)
Get the operator profiles associated with a partner

Get the operator profiles associated with a proxy (MSP), as assigned by the operator.  Privileges required:  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyOperatorProfiles**](EnterpriseProxyGetEnterpriseProxyOperatorProfiles.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyOperatorProfilesResultItem**](enterprise_proxy_get_enterprise_proxy_operator_profiles_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyUser**
> EnterpriseProxyGetEnterpriseProxyUser EnterpriseProxyGetEnterpriseProxyUser(ctx, body)
Get an enterprise proxy user

Get an enterprise proxy user by id or username.  Privileges required:  `READ` `PROXY_USER`  `READ` `PROXY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyUser**](EnterpriseProxyGetEnterpriseProxyUser.md)|  | 

### Return type

[**EnterpriseProxyGetEnterpriseProxyUser**](enterprise_proxy_get_enterprise_proxy_user.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyUsers**
> []EnterpriseProxyGetEnterpriseProxyUsersResultItem EnterpriseProxyGetEnterpriseProxyUsers(ctx, body)
Get all enterprise proxy admin users

undefined  Privileges required:  `READ` `ENTERPRISE`  `READ` `PROXY_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyUsers**](EnterpriseProxyGetEnterpriseProxyUsers.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyUsersResultItem**](enterprise_proxy_get_enterprise_proxy_users_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyInsertEnterpriseProxyEnterprise**
> EnterpriseProxyInsertEnterpriseProxyEnterpriseResult EnterpriseProxyInsertEnterpriseProxyEnterprise(ctx, body)
Insert a new enterprise owned by an MSP

Insert an enterprise owned by an MSP. Whereas the `insertEnterprise` method will create an enterprise in the global or network context with no MSP association, this method will create one owned by an MSP, as determined by the credentials of the caller.  Privileges required:  `CREATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyInsertEnterpriseProxyEnterprise**](EnterpriseProxyInsertEnterpriseProxyEnterprise.md)|  | 

### Return type

[**EnterpriseProxyInsertEnterpriseProxyEnterpriseResult**](enterprise_proxy_insert_enterprise_proxy_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyInsertEnterpriseProxyUser**
> EnterpriseProxyInsertEnterpriseProxyUserResult EnterpriseProxyInsertEnterpriseProxyUser(ctx, body)
Create a new partner admin user

Create a new partner admin user  Privileges required:  `CREATE` `PROXY_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NewEnterpriseProxyUser**](NewEnterpriseProxyUser.md)|  | 

### Return type

[**EnterpriseProxyInsertEnterpriseProxyUserResult**](enterprise_proxy_insert_enterprise_proxy_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyUpdateEnterpriseProxyUser**
> EnterpriseProxyUpdateEnterpriseProxyUserResult EnterpriseProxyUpdateEnterpriseProxyUser(ctx, body)
Update an enterprise proxy admin user

Update an enterprise proxy admin user  Privileges required:  `UPDATE` `PROXY_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyUpdateEnterpriseProxyUser**](EnterpriseProxyUpdateEnterpriseProxyUser.md)|  | 

### Return type

[**EnterpriseProxyUpdateEnterpriseProxyUserResult**](enterprise_proxy_update_enterprise_proxy_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseSetEnterpriseAllAlertRecipients**
> EnterpriseSetEnterpriseAllAlertRecipientsResult EnterpriseSetEnterpriseAllAlertRecipients(ctx, body)
Set the recipients who should receive all alerts for an enterprise

Set the recipients who should receive all alerts for an enterprise.  Privileges required:  `UPDATE` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseSetEnterpriseAllAlertRecipients**](EnterpriseSetEnterpriseAllAlertRecipients.md)|  | 

### Return type

[**EnterpriseSetEnterpriseAllAlertRecipientsResult**](enterprise_set_enterprise_all_alert_recipients_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterprise**
> EnterpriseUpdateEnterpriseResult EnterpriseUpdateEnterprise(ctx, body)
Update an enterprise

Update an enterprise provided an object id or name, and an _update object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterprise**](EnterpriseUpdateEnterprise.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseResult**](enterprise_update_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseNetworkAllocation**
> EnterpriseUpdateEnterpriseNetworkAllocationResult EnterpriseUpdateEnterpriseNetworkAllocation(ctx, body)
Update an enterprise network allocation

Update an enterprise network allocation, provided an object id and an _update object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseNetworkAllocation**](EnterpriseUpdateEnterpriseNetworkAllocation.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseNetworkAllocationResult**](enterprise_update_enterprise_network_allocation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseNetworkSegment**
> EnterpriseUpdateEnterpriseNetworkSegmentResult EnterpriseUpdateEnterpriseNetworkSegment(ctx, body)
Update an enterprise network segment

Update an enterprise network segment.  Privileges required:  `UPDATE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterpriseNetworkSegment1**](EnterpriseInsertEnterpriseNetworkSegment1.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseNetworkSegmentResult**](enterprise_update_enterprise_network_segment_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseRoute**
> EnterpriseUpdateEnterpriseRouteResult EnterpriseUpdateEnterpriseRoute(ctx, body)
Update an enterprise route

Update an enterprise route, set advertisement and cost values. Required parameters include the original route, as returned by enterprise/getEnterpriseRouteTable and the updated route with modified advertisement and route preference ordering.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseRoute**](EnterpriseUpdateEnterpriseRoute.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseRouteResult**](enterprise_update_enterprise_route_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseRouteConfiguration**
> EnterpriseUpdateEnterpriseRouteConfigurationResult EnterpriseUpdateEnterpriseRouteConfiguration(ctx, body)
Update enterprise routing configuration

Update enterprise routing configuration, by configuration id or logicalId.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseRouteConfiguration**](EnterpriseUpdateEnterpriseRouteConfiguration.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseRouteConfigurationResult**](enterprise_update_enterprise_route_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseSecurityPolicy**
> EnterpriseUpdateEnterpriseSecurityPolicyResult EnterpriseUpdateEnterpriseSecurityPolicy(ctx, body)
Update enterprise security policy

Update enterprise security policy in accordance with to the passed ipsec settings.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseSecurityPolicy**](EnterpriseUpdateEnterpriseSecurityPolicy.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseSecurityPolicyResult**](enterprise_update_enterprise_security_policy_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseService**
> EnterpriseUpdateEnterpriseServiceResult EnterpriseUpdateEnterpriseService(ctx, body)
Update an enterprise service

Update the enterprise service with the given id according to the settings specified by the _update field.  Privileges required:  `UPDATE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseService**](EnterpriseUpdateEnterpriseService.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseServiceResult**](enterprise_update_enterprise_service_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUserDeleteEnterpriseUser**
> EnterpriseUserDeleteEnterpriseUserResult EnterpriseUserDeleteEnterpriseUser(ctx, body)
Delete an enterprise user.

Delete an enterprise user by id or username. Note that `enterpriseId` is a required parameter when invoking this method as an operator or partner user.  Privileges required:  `DELETE` `ENTERPRISE_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUserDeleteEnterpriseUser**](EnterpriseUserDeleteEnterpriseUser.md)|  | 

### Return type

[**EnterpriseUserDeleteEnterpriseUserResult**](enterprise_user_delete_enterprise_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUserGetEnterpriseUser**
> EnterpriseUserGetEnterpriseUserResult EnterpriseUserGetEnterpriseUser(ctx, body)
Get an enterprise user

Get an enterprise user by id or username.  Privileges required:  `READ` `ENTERPRISE_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUserGetEnterpriseUser**](EnterpriseUserGetEnterpriseUser.md)|  | 

### Return type

[**EnterpriseUserGetEnterpriseUserResult**](enterprise_user_get_enterprise_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUserUpdateEnterpriseUser**
> EnterpriseUserUpdateEnterpriseUserResult EnterpriseUserUpdateEnterpriseUser(ctx, body)
Update an enterprise user

Update an enterprise user provided an object `id` or other identifying attributes, and an `_update` object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `ENTERPRISE_USER`, or  `UPDATE` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUserUpdateEnterpriseUser**](EnterpriseUserUpdateEnterpriseUser.md)|  | 

### Return type

[**EnterpriseUserUpdateEnterpriseUserResult**](enterprise_user_update_enterprise_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EventGetEnterpriseEvents**
> EventGetEnterpriseEventsResult EventGetEnterpriseEvents(ctx, body)
Get Edge events

Gets Edge events in an enterprise or Edge context. Returns an array of Edge events sorted by `eventTime`.  Privileges required:  `READ` `ENTERPRISE_EVENT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EventGetEnterpriseEvents**](EventGetEnterpriseEvents.md)|  | 

### Return type

[**EventGetEnterpriseEventsResult**](event_get_enterprise_events_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EventGetOperatorEvents**
> EventGetOperatorEventsResult EventGetOperatorEvents(ctx, body)
Get operator events

Gets operator events by network ID (optional). If not specified, will be taken for the caller's security context. Optionally, use a filter object to limit the number of events returned. Additionally, specify a time interval with an interval object. If no end date is specified, then the default is the current date. Specify a `gatewayId` to filter events for the specified gateway.  Privileges required:  `READ` `OPERATOR_EVENT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EventGetOperatorEvents**](EventGetOperatorEvents.md)|  | 

### Return type

[**EventGetOperatorEventsResult**](event_get_operator_events_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **FirewallGetEnterpriseFirewallLogs**
> FirewallGetEnterpriseFirewallLogsResult FirewallGetEnterpriseFirewallLogs(ctx, body)
Get enterprise firewall logs

Gets firewall logs for the specified enterprise.  Privileges required:  `READ` `EDGE`  `VIEW_FIREWALL_LOGS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**FirewallGetEnterpriseFirewallLogs**](FirewallGetEnterpriseFirewallLogs.md)|  | 

### Return type

[**FirewallGetEnterpriseFirewallLogsResult**](firewall_get_enterprise_firewall_logs_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GatewayDeleteGateway**
> GatewayDeleteGatewayResult GatewayDeleteGateway(ctx, body)
Delete a gateway

Delete a gateway by id.  Privileges required:  `DELETE` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**GatewayDeleteGateway**](GatewayDeleteGateway.md)|  | 

### Return type

[**GatewayDeleteGatewayResult**](gateway_delete_gateway_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GatewayGatewayProvision**
> GatewayGatewayProvisionResult GatewayGatewayProvision(ctx, body)
Provision a gateway

Provision a gateway into an operator network.  Privileges required:  `CREATE` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**GatewayGatewayProvision**](GatewayGatewayProvision.md)|  | 

### Return type

[**GatewayGatewayProvisionResult**](gateway_gateway_provision_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GatewayGetGatewayEdgeAssignments**
> []GatewayGetGatewayEdgeAssignmentsResultItem GatewayGetGatewayEdgeAssignments(ctx, body)
Get edge assignments for a gateway

Get edge assignments for a gateway  Privileges required:  `READ` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**GatewayGetGatewayEdgeAssignments**](GatewayGetGatewayEdgeAssignments.md)|  | 

### Return type

[**[]GatewayGetGatewayEdgeAssignmentsResultItem**](gateway_get_gateway_edge_assignments_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GatewayUpdateGatewayAttributes**
> GatewayUpdateGatewayAttributesResult GatewayUpdateGatewayAttributes(ctx, body)
Update gateway attributes

Update gateway attributes (name, ipAddress, on-premise parametrization and description) and associated site attributes  Privileges required:  `UPDATE` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**GatewayUpdateGatewayAttributes**](GatewayUpdateGatewayAttributes.md)|  | 

### Return type

[**GatewayUpdateGatewayAttributesResult**](gateway_update_gateway_attributes_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **LinkQualityEventGetLinkQualityEvents**
> LinkQualityEventGetLinkQualityEventsResult LinkQualityEventGetLinkQualityEvents(ctx, body)
Get link quality data

Returns link quality scores per link for a particular edge within a time interval. Rolls up link quality events to provide an aggregate score for the edge. Returns an empty array if no link quality events are available in the given timeframe.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**LinkQualityEventGetLinkQualityEvents**](LinkQualityEventGetLinkQualityEvents.md)|  | 

### Return type

[**LinkQualityEventGetLinkQualityEventsResult**](link_quality_event_get_link_quality_events_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **LoginEnterpriseLogin**
> LoginEnterpriseLogin(ctx, authorization)
Authentication for non-operator users

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **authorization** | [**AuthObject**](AuthObject.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **LoginOperatorLogin**
> LoginOperatorLogin(ctx, authorization)
Authentication for an operator user

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **authorization** | [**AuthObject**](AuthObject.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Logout**
> Logout(ctx, )
Deactivate a given authorization cookie

### Required Parameters
This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Meta**
> InlineResponse200 Meta(ctx, apiPath)
Get meta-data on any other API call

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **apiPath** | **string**| the path to another api method, starting after /rest/ | 

### Return type

[**InlineResponse200**](inline_response_200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeAppLinkMetrics**
> []MetricsGetEdgeAppLinkMetricsResultItem MetricsGetEdgeAppLinkMetrics(ctx, body)
Get flow metric aggregate data by link

Fetch flow metric summaries for the given time interval by link. On success, this method returns an array of flow data where each entry corresponds to a link on the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeAppLinkMetrics**](MetricsGetEdgeAppLinkMetrics.md)|  | 

### Return type

[**[]MetricsGetEdgeAppLinkMetricsResultItem**](metrics_get_edge_app_link_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeAppLinkSeries**
> []MetricsGetEdgeAppLinkSeriesResultItem MetricsGetEdgeAppLinkSeries(ctx, body)
Get flow metric time series data by link

Fetch flow metric time series for the given time interval by link. On success, this method returns an array of flow data where each entry corresponds to a link on the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeAppLinkSeries**](MetricsGetEdgeAppLinkSeries.md)|  | 

### Return type

[**[]MetricsGetEdgeAppLinkSeriesResultItem**](metrics_get_edge_app_link_series_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeAppMetrics**
> []MetricsGetEdgeAppMetricsResultItem MetricsGetEdgeAppMetrics(ctx, body)
Get flow metric aggregate data by application

Fetch flow metric summaries for the given time interval by application. On success, this method returns an array of flow data where each entry corresponds to a single application. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeAppMetrics**](MetricsGetEdgeAppMetrics.md)|  | 

### Return type

[**[]MetricsGetEdgeAppMetricsResultItem**](metrics_get_edge_app_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeAppSeries**
> []MetricsGetEdgeAppSeriesResultItem MetricsGetEdgeAppSeries(ctx, body)
Get flow metric time series data by application

Fetch flow metric time series for the given time interval by application. On success, this method returns an array of flow data where each entry corresponds to a single application. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeAppSeries**](MetricsGetEdgeAppSeries.md)|  | 

### Return type

[**[]MetricsGetEdgeAppSeriesResultItem**](metrics_get_edge_app_series_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeCategoryMetrics**
> []MetricsGetEdgeCategoryMetricsResultItem MetricsGetEdgeCategoryMetrics(ctx, body)
Get flow metric aggregate data by application category

Fetch flow metric summaries for the given time interval by application category. On success, this method returns an array of flow data where each entry corresponds to a category of application traffic that has traversed the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeCategoryMetrics**](MetricsGetEdgeCategoryMetrics.md)|  | 

### Return type

[**[]MetricsGetEdgeCategoryMetricsResultItem**](metrics_get_edge_category_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeCategorySeries**
> []MetricsGetEdgeCategorySeriesResultItem MetricsGetEdgeCategorySeries(ctx, body)
Get flow metric time series data by application category

Fetch flow metric time series for the given time interval by application category. On success, this method returns an array of flow data where each entry corresponds to a category of application traffic that has traversed the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeCategorySeries**](MetricsGetEdgeCategorySeries.md)|  | 

### Return type

[**[]MetricsGetEdgeCategorySeriesResultItem**](metrics_get_edge_category_series_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeDestMetrics**
> []MetricsGetEdgeDestMetricsResultItem MetricsGetEdgeDestMetrics(ctx, body)
Get flow metric aggregate data by destination

Fetch flow metric summaries for the given time interval by destination. On success, this method returns an array of flow data where each entry corresponds to a distinct traffic destination. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeDestMetrics**](MetricsGetEdgeDestMetrics.md)|  | 

### Return type

[**[]MetricsGetEdgeDestMetricsResultItem**](metrics_get_edge_dest_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeDestSeries**
> []MetricsGetEdgeDestSeriesResultItem MetricsGetEdgeDestSeries(ctx, body)
Get flow metric time series data by destination

Fetch flow metric time series for the given time interval by destination. On success, this method returns an array of flow data where each entry corresponds to a distinct traffic destination. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeDestSeries**](MetricsGetEdgeDestSeries.md)|  | 

### Return type

[**[]MetricsGetEdgeDestSeriesResultItem**](metrics_get_edge_dest_series_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeDeviceMetrics**
> []MetricsGetEdgeDeviceMetricsResultItem MetricsGetEdgeDeviceMetrics(ctx, body)
Get flow metric aggregate data by client device

Fetch flow metric summaries for the given time interval by client device. On success, this method returns an array of flow data where each entry corresponds to a distinct device. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_USER_IDENTIFIABLE_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeDeviceMetrics**](MetricsGetEdgeDeviceMetrics.md)|  | 

### Return type

[**[]MetricsGetEdgeDeviceMetricsResultItem**](metrics_get_edge_device_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeDeviceSeries**
> []MetricsGetEdgeDeviceSeriesResultItem MetricsGetEdgeDeviceSeries(ctx, body)
Get flow metric time series data by client device

Fetch flow metric time series for the given time interval by client device. On success, this method returns an array of flow data where each entry corresponds to a distinct device. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_USER_IDENTIFIABLE_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeDeviceSeries**](MetricsGetEdgeDeviceSeries.md)|  | 

### Return type

[**[]MetricsGetEdgeDeviceSeriesResultItem**](metrics_get_edge_device_series_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeLinkMetrics**
> []MetricsGetEdgeLinkMetricsResultItem MetricsGetEdgeLinkMetrics(ctx, body)
Get advanced flow metric aggregate data by link

Fetch advanced flow metric summaries for the given time interval by link. On success, this method returns an array of flow data where each entry corresponds to a link on the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeLinkMetrics**](MetricsGetEdgeLinkMetrics.md)|  | 

### Return type

[**[]MetricsGetEdgeLinkMetricsResultItem**](metrics_get_edge_link_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeLinkSeries**
> []MetricsGetEdgeLinkSeriesResultItem MetricsGetEdgeLinkSeries(ctx, body)
Get advanced flow metric time series data by link

Fetch advanced flow metric time series for the given time interval by link. On success, this method returns an array of flow data where each entry corresponds to a link on the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeLinkSeries**](MetricsGetEdgeLinkSeries.md)|  | 

### Return type

[**[]MetricsGetEdgeLinkSeriesResultItem**](metrics_get_edge_link_series_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeOsMetrics**
> []MetricsGetEdgeOsMetricsResultItem MetricsGetEdgeOsMetrics(ctx, body)
Get flow metric aggregate data by client OS

Fetch flow metric summaries for the given time interval by client OS. On success, this method returns an array of flow data where each entry corresponds to a distinct OS on a client device. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeOsMetrics**](MetricsGetEdgeOsMetrics.md)|  | 

### Return type

[**[]MetricsGetEdgeOsMetricsResultItem**](metrics_get_edge_os_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeOsSeries**
> []MetricsGetEdgeOsSeriesResultItem MetricsGetEdgeOsSeries(ctx, body)
Get flow metric time series data by client OS

Fetch flow metric time series for the given time interval by client OS. On success, this method returns an array of flow data where each entry corresponds to a distinct OS on a client device. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeOsSeries**](MetricsGetEdgeOsSeries.md)|  | 

### Return type

[**[]MetricsGetEdgeOsSeriesResultItem**](metrics_get_edge_os_series_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeSegmentMetrics**
> []MetricsGetEdgeSegmentMetricsResultItem MetricsGetEdgeSegmentMetrics(ctx, body)
Get flow metric aggregate data by segment Id

Fetch flow metric summaries for the given time interval by segment id. On success, this method returns an array of flow data where each entry corresponds to a segment id traffic that has traversed the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeSegmentMetrics**](MetricsGetEdgeSegmentMetrics.md)|  | 

### Return type

[**[]MetricsGetEdgeSegmentMetricsResultItem**](metrics_get_edge_segment_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MetricsGetEdgeSegmentSeries**
> []MetricsGetEdgeSegmentSeriesResultItem MetricsGetEdgeSegmentSeries(ctx, body)
Get flow metric time series data by segment id

Fetch flow metric time series for the given time interval by segment id. On success, this method returns an array of flow data where each entry corresponds to a segment id of traffic that has traversed the given edge. In the request body, the `id` and `edgeId` property names are interchangeable. The `enterpriseId` property is required when this method is invoked in the operator context.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MetricsGetEdgeSegmentSeries**](MetricsGetEdgeSegmentSeries.md)|  | 

### Return type

[**[]MetricsGetEdgeSegmentSeriesResultItem**](metrics_get_edge_segment_series_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetAggregateEdgeLinkMetrics**
> []MonitoringGetAggregateEdgeLinkMetricsResultItem MonitoringGetAggregateEdgeLinkMetrics(ctx, body)
Get aggregate Edge link metrics across enterprises

Gets aggregate link metrics for the request interval for all active links across all enterprises, where a link is considered to be active if an Edge has reported any activity for it in the last 24 hours. On success, returns an array of network utilization metrics, one per link.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MonitoringGetAggregateEdgeLinkMetrics**](MonitoringGetAggregateEdgeLinkMetrics.md)|  | 

### Return type

[**[]MonitoringGetAggregateEdgeLinkMetricsResultItem**](monitoring_get_aggregate_edge_link_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetAggregateEnterpriseEvents**
> MonitoringGetAggregateEnterpriseEventsResult MonitoringGetAggregateEnterpriseEvents(ctx, body)
Get events across all enterprises

Gets events across all enterprises in a paginated list. When called in the MSP/Partner context, queries only enterprises managed by the MSP.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MonitoringGetAggregateEnterpriseEvents**](MonitoringGetAggregateEnterpriseEvents.md)|  | 

### Return type

[**MonitoringGetAggregateEnterpriseEventsResult**](monitoring_get_aggregate_enterprise_events_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetAggregates**
> MonitoringGetAggregatesResult MonitoringGetAggregates(ctx, body)
Get aggregate enterprise and edge information

Retrieve a comprehensive listing of all enterprises and edges on a network. Returns an object containing an aggregate `edgeCount`, a list (`enterprises`) containing enterprise objects, and a map (`edges`) which gives edge counts per enterprise.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MonitoringGetAggregates**](MonitoringGetAggregates.md)|  | 

### Return type

[**MonitoringGetAggregatesResult**](monitoring_get_aggregates_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetEnterpriseBgpPeerStatus**
> []MonitoringGetEnterpriseBgpPeerStatusResultItem MonitoringGetEnterpriseBgpPeerStatus(ctx, body)
Get gateway BGP peer status for all enterprise gateways

Returns an array where each entry corresponds to a gateway and contains an associated set of BGP peers with state records.  Privileges required:  `READ` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body**](Body.md)|  | 

### Return type

[**[]MonitoringGetEnterpriseBgpPeerStatusResultItem**](monitoring_get_enterprise_bgp_peer_status_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetEnterpriseEdgeBgpPeerStatus**
> []MonitoringGetEnterpriseEdgeBgpPeerStatusResultItem MonitoringGetEnterpriseEdgeBgpPeerStatus(ctx, body)
Get edge BGP peer status for all enterprise edges

Returns an array where each entry corresponds to an edge and contains an associated set of BGP peers and state records.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body1**](Body1.md)|  | 

### Return type

[**[]MonitoringGetEnterpriseEdgeBgpPeerStatusResultItem**](monitoring_get_enterprise_edge_bgp_peer_status_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetEnterpriseEdgeLinkStatus**
> []MonitoringGetEnterpriseEdgeLinkStatusResultItem MonitoringGetEnterpriseEdgeLinkStatus(ctx, body)
Get edge and link status data

Get current edge and edge-link status for all enterprise edges.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body2**](Body2.md)|  | 

### Return type

[**[]MonitoringGetEnterpriseEdgeLinkStatusResultItem**](monitoring_get_enterprise_edge_link_status_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NetworkDeleteNetworkGatewayPool**
> NetworkDeleteNetworkGatewayPoolResult NetworkDeleteNetworkGatewayPool(ctx, body)
Delete gateway pool

Deletes the specified gateway pool (by `id`).  Privileges required:  `DELETE` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NetworkDeleteNetworkGatewayPool**](NetworkDeleteNetworkGatewayPool.md)|  | 

### Return type

[**NetworkDeleteNetworkGatewayPoolResult**](network_delete_network_gateway_pool_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NetworkGetNetworkConfigurations**
> []NetworkGetNetworkConfigurationsResultItem NetworkGetNetworkConfigurations(ctx, body)
Get operator configuration profiles

Gets all operator configuration profiles associated with an operator's network. Optionally includes the modules associated with each profile. This call does not return templates.  Privileges required:  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NetworkGetNetworkConfigurations**](NetworkGetNetworkConfigurations.md)|  | 

### Return type

[**[]NetworkGetNetworkConfigurationsResultItem**](network_get_network_configurations_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NetworkGetNetworkEnterprises**
> []NetworkGetNetworkEnterprisesResultItem NetworkGetNetworkEnterprises(ctx, body)
Get a list of the enterprises on a network

Get the enterprises existing on a network, optionally including all edges or edge counts. The `edgeConfigUpdate` \"with\" option may also be passed to check whether application of configuration updates to edges is enabled for each enterprise.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NetworkGetNetworkEnterprises**](NetworkGetNetworkEnterprises.md)|  | 

### Return type

[**[]NetworkGetNetworkEnterprisesResultItem**](network_get_network_enterprises_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NetworkGetNetworkGatewayPools**
> []NetworkGetNetworkGatewayPoolsResultItem NetworkGetNetworkGatewayPools(ctx, body)
Get list of gateway pools

Get list of gateway pools associated with a network, optionally with the gateways or enterprises belonging to each pool.  Privileges required:  `READ` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NetworkGetNetworkGatewayPools**](NetworkGetNetworkGatewayPools.md)|  | 

### Return type

[**[]NetworkGetNetworkGatewayPoolsResultItem**](network_get_network_gateway_pools_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NetworkGetNetworkGateways**
> []NetworkGetNetworkGatewaysResultItem NetworkGetNetworkGateways(ctx, body)
Get list of gateways

Get list of gateways associated with a network.  Privileges required:  `READ` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NetworkGetNetworkGateways**](NetworkGetNetworkGateways.md)|  | 

### Return type

[**[]NetworkGetNetworkGatewaysResultItem**](network_get_network_gateways_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NetworkGetNetworkOperatorUsers**
> []NetworkGetNetworkOperatorUsersResultItem NetworkGetNetworkOperatorUsers(ctx, body)
Get list of operator users for a network

Get a list of all of the operator users associated with a network  Privileges required:  `READ` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NetworkGetNetworkOperatorUsers**](NetworkGetNetworkOperatorUsers.md)|  | 

### Return type

[**[]NetworkGetNetworkOperatorUsersResultItem**](network_get_network_operator_users_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NetworkInsertNetworkGatewayPool**
> NetworkInsertNetworkGatewayPoolResult NetworkInsertNetworkGatewayPool(ctx, body)
Insert a gateway pool

Insert a gateway pool, associated with a network.  Privileges required:  `CREATE` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NetworkInsertNetworkGatewayPool**](NetworkInsertNetworkGatewayPool.md)|  | 

### Return type

[**NetworkInsertNetworkGatewayPoolResult**](network_insert_network_gateway_pool_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **NetworkUpdateNetworkGatewayPoolAttributes**
> NetworkUpdateNetworkGatewayPoolAttributesResult NetworkUpdateNetworkGatewayPoolAttributes(ctx, body)
Update gateway pool attributes

Update the configurable attributes of a Gateway Pool. Configurarable attributes are `name`, `description`, and `handOffType`.  Privileges required:  `UPDATE` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NetworkUpdateNetworkGatewayPoolAttributes**](NetworkUpdateNetworkGatewayPoolAttributes.md)|  | 

### Return type

[**NetworkUpdateNetworkGatewayPoolAttributesResult**](network_update_network_gateway_pool_attributes_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **OperatorUserDeleteOperatorUser**
> OperatorUserDeleteOperatorUserResult OperatorUserDeleteOperatorUser(ctx, body)
Delete an operator user

Delete an operator user object by `id` or `username`.  Privileges required:  `DELETE` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**OperatorUserDeleteOperatorUser**](OperatorUserDeleteOperatorUser.md)|  | 

### Return type

[**OperatorUserDeleteOperatorUserResult**](operator_user_delete_operator_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **OperatorUserGetOperatorUser**
> OperatorUserGetOperatorUserResult OperatorUserGetOperatorUser(ctx, body)
Get an operator user

Get an operator user object by `id` or `username`.  Privileges required:  `READ` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**OperatorUserGetOperatorUser**](OperatorUserGetOperatorUser.md)|  | 

### Return type

[**OperatorUserGetOperatorUserResult**](operator_user_get_operator_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **OperatorUserInsertOperatorUser**
> OperatorUserGetOperatorUserResult OperatorUserInsertOperatorUser(ctx, body)
Insert an operator user

Insert an operator user and associate with an operator's network.  Privileges required:  `CREATE` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**OperatorUserInsertOperatorUser**](OperatorUserInsertOperatorUser.md)|  | 

### Return type

[**OperatorUserGetOperatorUserResult**](operator_user_get_operator_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **OperatorUserUpdateOperatorUser**
> OperatorUserUpdateOperatorUserResult OperatorUserUpdateOperatorUser(ctx, body)
Update an operator user

Update an operator user provided an object `id` or `username`, and an `_update` object containing the names and values, of columns to be updated.  Privileges required:  `UPDATE` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**OperatorUserUpdateOperatorUser**](OperatorUserUpdateOperatorUser.md)|  | 

### Return type

[**OperatorUserUpdateOperatorUserResult**](operator_user_update_operator_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleCreateRoleCustomization**
> RoleCreateRoleCustomizationResult RoleCreateRoleCustomization(ctx, body)
Create a role customization

Create a role customization given a roleId and an array of privilegeIds.  Privileges required:  `UPDATE` `NETWORK`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**RoleCreateRoleCustomization**](RoleCreateRoleCustomization.md)|  | 

### Return type

[**RoleCreateRoleCustomizationResult**](role_create_role_customization_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleDeleteRoleCustomization**
> RoleDeleteRoleCustomizationResult RoleDeleteRoleCustomization(ctx, body)
Delete a role customization

Delete a role customization, given a role customization name or forRoleId.  Privileges required:  `UPDATE` `NETWORK`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**RoleDeleteRoleCustomization**](RoleDeleteRoleCustomization.md)|  | 

### Return type

[**RoleDeleteRoleCustomizationResult**](role_delete_role_customization_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleGetUserTypeRoles**
> []RoleGetUserTypeRolesResultItem RoleGetUserTypeRoles(ctx, body)
Get the roles defined for a user type

Return the defined roles for a specified user type.  Privileges required:  `READ` `ENTERPRISE_USER`, or  `READ` `PROXY_USER`, or  `READ` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**RoleGetUserTypeRoles**](RoleGetUserTypeRoles.md)|  | 

### Return type

[**[]RoleGetUserTypeRolesResultItem**](role_get_user_type_roles_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleSetEnterpriseDelegatedToEnterpriseProxy**
> RoleSetEnterpriseDelegatedToEnterpriseProxyResult RoleSetEnterpriseDelegatedToEnterpriseProxy(ctx, body)
Grant enterprise access to partner

Grants enterprise access to the specified enterprise proxy (partner). When an enterprise is delegated to a proxy, proxy users are granted access to view, configure, and troubleshoot Edges owned by the enterprise. As a security consideration, proxy Support users cannot view personally identifiable information.  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body3**](Body3.md)|  | 

### Return type

[**RoleSetEnterpriseDelegatedToEnterpriseProxyResult**](role_set_enterprise_delegated_to_enterprise_proxy_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleSetEnterpriseDelegatedToOperator**
> RoleSetEnterpriseDelegatedToOperatorResult RoleSetEnterpriseDelegatedToOperator(ctx, body)
Grant enterprise access to network operator

Grants enterprise access to the network operator. When an enterprise is delegated to the operator, operator users are permitted to view, configure, and troubleshoot Edges owned by the enterprise. As a security consideration, operator users cannot view personally identifiable information.  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body4**](Body4.md)|  | 

### Return type

[**RoleSetEnterpriseDelegatedToOperatorResult**](role_set_enterprise_delegated_to_operator_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleSetEnterpriseProxyDelegatedToOperator**
> RoleSetEnterpriseProxyDelegatedToOperatorResult RoleSetEnterpriseProxyDelegatedToOperator(ctx, body)
Grant enterprise proxy access to network operator

Grants enterprise proxy access to the network operator. When an enterprise proxy is delegated to the operator, operator users are granted access to view, configure and troubleshoot objects owned by the proxy.  Privileges required:  `UPDATE` `ENTERPRISE_PROXY_DELEGATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body5**](Body5.md)|  | 

### Return type

[**RoleSetEnterpriseProxyDelegatedToOperatorResult**](role_set_enterprise_proxy_delegated_to_operator_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleSetEnterpriseUserManagementDelegatedToOperator**
> RoleSetEnterpriseUserManagementDelegatedToOperatorResult RoleSetEnterpriseUserManagementDelegatedToOperator(ctx, body)
Grant enterprise user access to the network operator

When enterprise user management is delegated to the operator, operator users are granted enterprise-level user management capabilities (user creation, password resets, etc.).  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body6**](Body6.md)|  | 

### Return type

[**RoleSetEnterpriseUserManagementDelegatedToOperatorResult**](role_set_enterprise_user_management_delegated_to_operator_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyGetSystemProperties**
> []SystemPropertyGetSystemPropertiesResultItem SystemPropertyGetSystemProperties(ctx, body)
Get all system properties

Get a list of all configured system properties.  Privileges required:  `READ` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyGetSystemProperties**](SystemPropertyGetSystemProperties.md)|  | 

### Return type

[**[]SystemPropertyGetSystemPropertiesResultItem**](system_property_get_system_properties_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyGetSystemProperty**
> SystemPropertyGetSystemPropertyResult SystemPropertyGetSystemProperty(ctx, body)
Get system property

Get a system property by object id or other attribute.  Privileges required:  `READ` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyGetSystemProperty**](SystemPropertyGetSystemProperty.md)|  | 

### Return type

[**SystemPropertyGetSystemPropertyResult**](system_property_get_system_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyInsertOrUpdateSystemProperty**
> SystemPropertyInsertOrUpdateSystemPropertyResult SystemPropertyInsertOrUpdateSystemProperty(ctx, body)
Insert or update a system property

Insert a system property. If property with the given name already exists, the property will be updated.  Privileges required:  `CREATE` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyInsertOrUpdateSystemProperty**](SystemPropertyInsertOrUpdateSystemProperty.md)|  | 

### Return type

[**SystemPropertyInsertOrUpdateSystemPropertyResult**](system_property_insert_or_update_system_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyInsertSystemProperty**
> SystemPropertyInsertSystemPropertyResult SystemPropertyInsertSystemProperty(ctx, body)
Insert a system property

Insert a new system property.  Privileges required:  `CREATE` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyInsertSystemProperty**](SystemPropertyInsertSystemProperty.md)|  | 

### Return type

[**SystemPropertyInsertSystemPropertyResult**](system_property_insert_system_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyUpdateSystemProperty**
> SystemPropertyUpdateSystemPropertyResult SystemPropertyUpdateSystemProperty(ctx, body)
Update a system property

Update an existing system property, provided an object `id` or other identifying attributes  Privileges required:  `UPDATE` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyUpdateSystemProperty**](SystemPropertyUpdateSystemProperty.md)|  | 

### Return type

[**SystemPropertyUpdateSystemPropertyResult**](system_property_update_system_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VcoInventoryAssociateEdge**
> InlineResponse2001 VcoInventoryAssociateEdge(ctx, body)
Return inventory items available at this VCO

Assigns an edge in the inventory to an Enterprise. To perform the action, the edge should already be in a STAGING state. The assignment can be done at an enterprise level, without selecting a destination Edge profile. In such a case, the inventory edge is assigned to a staging profile within the Enterprise. Optionally a profile or destination edge can be assigned to this inventory edge. The edge in the inventory can be assigned to any profile. The inventory edge can be assigned to an Enterprise edge only if it is in a PENDING/REACTIVATION_PENDING state.  Privileges required:  `CREATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**VcoInventoryAssociateEdge**](VcoInventoryAssociateEdge.md)|  | 

### Return type

[**InlineResponse2001**](inline_response_200_1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VcoInventoryGetInventoryItems**
> VcoInventoryGetInventoryItemsResult VcoInventoryGetInventoryItems(ctx, body)
Return inventory items available at this VCO

Retrieve all the inventory information available with this VCO. This method does not have required parameters. The optional parameters are  enterpriseId - Return inventory items belonging to that enterprise. If the caller context is an enterprise, this value will be taken from token itself. modifiedSince - Used to retrieve inventory items that have been modified in the last modifiedSince hours. with - an array containing the string \"edge\" to get details about details about the provisioned edge if any.  Privileges required:  `READ` `INVENTORY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**VcoInventoryGetInventoryItems**](VcoInventoryGetInventoryItems.md)|  | 

### Return type

[**VcoInventoryGetInventoryItemsResult**](vco_inventory_get_inventory_items_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

