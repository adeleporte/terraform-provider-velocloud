# Application

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Class** | **int32** |  | [default to null]
**Description** | **string** |  | [default to null]
**DisplayName** | **string** |  | [default to null]
**Id** | **int32** |  | [default to null]
**KnownIpPortMapping** | [***IpPortMapping**](ip_port_mapping.md) |  | [default to null]
**ProtocolPortMapping** | [***ProtocolPortMapping**](protocol_port_mapping.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


