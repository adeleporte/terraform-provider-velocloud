# ApplicationMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BlobId** | **int32** |  | [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [default to null]
**Description** | **string** |  | [default to null]
**FileName** | **string** |  | [default to null]
**Id** | **int32** |  | [default to null]
**LogicalId** | **string** |  | [default to null]
**Manifest** | [***ApplicationMetadataManifest**](application_metadata_manifest.md) |  | [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [default to null]
**Name** | **string** |  | [default to null]
**NetworkId** | **int32** |  | [default to null]
**Type_** | **string** |  | [default to null]
**UploadDetails** | [***ApplicationMetadataUploadDetails**](application_metadata_upload_details.md) |  | [default to null]
**Version** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


