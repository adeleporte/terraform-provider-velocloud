# BgpFilterRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Action** | [***BgpFilterRuleAction**](bgp_filter_rule_action.md) |  | [optional] [default to null]
**Match** | [***BgpFilterRuleMatch**](bgp_filter_rule_match.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


