# BgpFilterRuleAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type_** | **string** |  | [optional] [default to null]
**Values** | [**[]BgpFilterRuleActionValues**](bgp_filter_rule_action_values.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


