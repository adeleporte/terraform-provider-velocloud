# BgpPeerStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NeighborIp** | **string** |  | [optional] [default to null]
**NeighborAS** | **int32** |  | [optional] [default to null]
**Records** | [**[]BgpPeerStatusRecord**](bgp_peer_status_record.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


