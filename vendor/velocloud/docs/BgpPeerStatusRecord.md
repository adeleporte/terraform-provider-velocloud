# BgpPeerStatusRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Timestamp** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**State** | **string** |  | [optional] [default to null]
**MsgRcvd** | **int32** |  | [optional] [default to null]
**PfxRcvd** | **int32** |  | [optional] [default to null]
**MsgSent** | **int32** |  | [optional] [default to null]
**UpDownTime** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


