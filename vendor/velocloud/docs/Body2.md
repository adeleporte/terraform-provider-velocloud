# Body2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enterprises** | **[]int32** |  | [optional] [default to null]
**EnterpriseProxyId** | **int32** |  | [optional] [default to null]
**NetworkId** | **int32** |  | [optional] [default to null]
**Links** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


