# ClientDevice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [default to null]
**EnterpriseId** | **int32** |  | [default to null]
**MacAddress** | **string** |  | [default to null]
**HostName** | **string** |  | [default to null]
**IpAddress** | **string** |  | [default to null]
**Os** | **int32** |  | [default to null]
**OsName** | **string** |  | [default to null]
**OsVersion** | **string** |  | [default to null]
**DeviceType** | **string** |  | [default to null]
**DeviceModel** | **string** |  | [default to null]
**LastContact** | [**time.Time**](time.Time.md) |  | [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


