# ConfigEdgeBgpNeighbor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NeighborAS** | **string** |  | [optional] [default to null]
**NeighborIp** | **string** |  | [optional] [default to null]
**NeighborTag** | **string** |  | [optional] [default to null]
**InboundFilter** | [***ConfigEdgeBgpFilterSet**](config_edge_bgp_filter_set.md) |  | [optional] [default to null]
**OutboundFilter** | [***ConfigEdgeBgpFilterSet**](config_edge_bgp_filter_set.md) |  | [optional] [default to null]
**AllowAS** | **bool** |  | [optional] [default to null]
**Connect** | **string** |  | [optional] [default to null]
**DefaultRoute** | **bool** |  | [optional] [default to null]
**Holdtime** | **string** |  | [optional] [default to null]
**Keepalive** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


