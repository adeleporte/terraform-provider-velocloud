# \ConfigurationApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ConfigurationCloneAndConvertConfiguration**](ConfigurationApi.md#ConfigurationCloneAndConvertConfiguration) | **Post** /configuration/cloneAndConvertConfiguration | Clone and convert a network based profile configuration to segment based profile configuration
[**ConfigurationCloneConfiguration**](ConfigurationApi.md#ConfigurationCloneConfiguration) | **Post** /configuration/cloneConfiguration | Clone configuration profile
[**ConfigurationCloneEnterpriseTemplate**](ConfigurationApi.md#ConfigurationCloneEnterpriseTemplate) | **Post** /configuration/cloneEnterpriseTemplate | Clone default enterprise configuration profile
[**ConfigurationDeleteConfiguration**](ConfigurationApi.md#ConfigurationDeleteConfiguration) | **Post** /configuration/deleteConfiguration | Delete a configuration profile
[**ConfigurationGetConfiguration**](ConfigurationApi.md#ConfigurationGetConfiguration) | **Post** /configuration/getConfiguration | Get a configuration profile
[**ConfigurationGetConfigurationModules**](ConfigurationApi.md#ConfigurationGetConfigurationModules) | **Post** /configuration/getConfigurationModules | List the modules that compose a configuration profile
[**ConfigurationGetRoutableApplications**](ConfigurationApi.md#ConfigurationGetRoutableApplications) | **Post** /configuration/getRoutableApplications | Get first packet routable applications
[**ConfigurationInsertConfigurationModule**](ConfigurationApi.md#ConfigurationInsertConfigurationModule) | **Post** /configuration/insertConfigurationModule | Insert a new configuration module
[**ConfigurationUpdateConfigurationModule**](ConfigurationApi.md#ConfigurationUpdateConfigurationModule) | **Post** /configuration/updateConfigurationModule | Update a configuration module


# **ConfigurationCloneAndConvertConfiguration**
> ConfigurationCloneAndConvertConfigurationResult ConfigurationCloneAndConvertConfiguration(ctx, body)
Clone and convert a network based profile configuration to segment based profile configuration

Clones an convert existing network configuration by configurationId. Accepts an enterpriseId or networkId to associate the new config with an enterprise or network. On success, returns an object the ID of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationCloneAndConvertConfiguration**](ConfigurationCloneAndConvertConfiguration.md)|  | 

### Return type

[**ConfigurationCloneAndConvertConfigurationResult**](configuration_clone_and_convert_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationCloneConfiguration**
> ConfigurationCloneConfigurationResult ConfigurationCloneConfiguration(ctx, body)
Clone configuration profile

Clones the specified configuration (by `configurationId`) and all associated configuration modules. Accepts an `enterpriseId` or `networkId` to associate the new configuration with an enterprise or network. Select modules may also be specified. On success, returns the `id` of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationCloneConfiguration**](ConfigurationCloneConfiguration.md)|  | 

### Return type

[**ConfigurationCloneConfigurationResult**](configuration_clone_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationCloneEnterpriseTemplate**
> ConfigurationCloneEnterpriseTemplateResult ConfigurationCloneEnterpriseTemplate(ctx, body)
Clone default enterprise configuration profile

Creates a new enterprise configuration from the enterprise default configuration. On success, returns the `id` of the newly created configuration object.  Privileges required:  `CREATE` `ENTERPRISE_PROFILE`, or  `CREATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationCloneEnterpriseTemplate**](ConfigurationCloneEnterpriseTemplate.md)|  | 

### Return type

[**ConfigurationCloneEnterpriseTemplateResult**](configuration_clone_enterprise_template_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationDeleteConfiguration**
> ConfigurationDeleteConfigurationResult ConfigurationDeleteConfiguration(ctx, body)
Delete a configuration profile

Delete an existing configuration profile. On success, returns an object indicating the number of rows deleted.  Privileges required:  `DELETE` `ENTERPRISE_PROFILE`, or  `DELETE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationDeleteConfiguration**](ConfigurationDeleteConfiguration.md)|  | 

### Return type

[**ConfigurationDeleteConfigurationResult**](configuration_delete_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationGetConfiguration**
> ConfigurationGetConfigurationResult ConfigurationGetConfiguration(ctx, body)
Get a configuration profile

Get a configuration profile, optionally with module detail.  Privileges required:  `READ` `ENTERPRISE_PROFILE`, or  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationGetConfiguration**](ConfigurationGetConfiguration.md)|  | 

### Return type

[**ConfigurationGetConfigurationResult**](configuration_get_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationGetConfigurationModules**
> []ConfigurationGetConfigurationModulesResultItem ConfigurationGetConfigurationModules(ctx, body)
List the modules that compose a configuration profile

Retrieve a list of the configuration modules that compose the given configuration profile.  Privileges required:  `READ` `ENTERPRISE_PROFILE`, or  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationGetConfigurationModules**](ConfigurationGetConfigurationModules.md)|  | 

### Return type

[**[]ConfigurationGetConfigurationModulesResultItem**](configuration_get_configuration_modules_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationGetRoutableApplications**
> ConfigurationGetRoutableApplicationsResult ConfigurationGetRoutableApplications(ctx, body)
Get first packet routable applications

Gets all applications that are first packet routable. If called from an operator or MSP context, then `enterpriseId` is required. Optionally, specify `edgeId` to get the map for a specific Edge.  Privileges required:  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationGetRoutableApplications**](ConfigurationGetRoutableApplications.md)|  | 

### Return type

[**ConfigurationGetRoutableApplicationsResult**](configuration_get_routable_applications_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationInsertConfigurationModule**
> ConfigurationInsertConfigurationModuleResult ConfigurationInsertConfigurationModule(ctx, body)
Insert a new configuration module

Insert a new configuration module into the given configuration profile.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`, or  `UPDATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationInsertConfigurationModule**](ConfigurationInsertConfigurationModule.md)|  | 

### Return type

[**ConfigurationInsertConfigurationModuleResult**](configuration_insert_configuration_module_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ConfigurationUpdateConfigurationModule**
> ConfigurationUpdateConfigurationModuleResult ConfigurationUpdateConfigurationModule(ctx, body)
Update a configuration module

Update an existing configuration module with the data. module data contained in the _update object.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`, or  `UPDATE` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**ConfigurationUpdateConfigurationModule**](ConfigurationUpdateConfigurationModule.md)|  | 

### Return type

[**ConfigurationUpdateConfigurationModuleResult**](configuration_update_configuration_module_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

