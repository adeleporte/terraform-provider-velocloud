# ConfigurationCloneAndConvertConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConfigurationId** | **int32** |  | [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**GuestVLANSegmentObjectId** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


