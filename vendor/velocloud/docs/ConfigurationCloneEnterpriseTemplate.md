# ConfigurationCloneEnterpriseTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** | Required if called from the operator or MSP context, identifies the target enterprise of the API call. | [optional] [default to null]
**ConfigurationType** | **string** | If both network and segment based functionality is granted to the enterprise, chose which template type to clone. If not specified the type of the operator profile assigned to the enterprise will be used. | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


