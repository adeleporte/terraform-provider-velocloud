# ConfigurationDeleteConfigurationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** | The id of the deleted object. | [optional] [default to null]
**Error_** | **string** | An error message explaining why the method failed | [optional] [default to null]
**Rows** | **int32** | The number of rows modified | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


