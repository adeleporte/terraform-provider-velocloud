# ConfigurationGetRoutableApplicationsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Applications** | [**[]Application**](application.md) |  | [default to null]
**MetaData** | [***ApplicationMetadata**](application_metadata.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


