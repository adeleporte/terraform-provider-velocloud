# ConfigurationInsertConfigurationModule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Data** | [***interface{}**](interface{}.md) |  | [default to null]
**ConfigurationId** | **int32** |  | [default to null]
**Version** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


