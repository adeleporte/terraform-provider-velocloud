# ControlPlaneData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GatewaySelection** | [***ControlPlaneDataGatewaySelection**](control_plane_data_gatewaySelection.md) |  | [optional] [default to null]
**Vpn** | [***ControlPlaneDataVpn**](control_plane_data_vpn.md) |  | [optional] [default to null]
**IpsecGatewayDetail** | [**map[string]GatewayPoolGatewayIpsecGatewayDetail**](gateway_pool_gateway_ipsecGatewayDetail.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


