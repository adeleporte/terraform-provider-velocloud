# ControlPlaneDataGatewaySelection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Mode** | **string** |  | [optional] [default to null]
**Primary** | **string** |  | [optional] [default to null]
**PrimaryDetail** | [***ControlPlaneDataGatewaySelectionPrimaryDetail**](control_plane_data_gatewaySelection_primaryDetail.md) |  | [optional] [default to null]
**Secondary** | **string** |  | [optional] [default to null]
**SecondaryDetail** | [***ControlPlaneDataGatewaySelectionPrimaryDetail**](control_plane_data_gatewaySelection_primaryDetail.md) |  | [optional] [default to null]
**Super** | **string** |  | [optional] [default to null]
**SuperDetail** | [***ControlPlaneDataGatewaySelectionPrimaryDetail**](control_plane_data_gatewaySelection_primaryDetail.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


