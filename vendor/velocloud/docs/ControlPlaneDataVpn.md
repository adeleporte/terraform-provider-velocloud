# ControlPlaneDataVpn

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DataCenterEdges** | [**[]ControlPlaneDataVpnDataCenterEdges**](control_plane_data_vpn_dataCenterEdges.md) |  | [optional] [default to null]
**EdgeToDataCenter** | **bool** |  | [optional] [default to null]
**EdgeToEdge** | **bool** |  | [optional] [default to null]
**EdgeToEdgeDetail** | [***ControlPlaneDataVpnEdgeToEdgeDetail**](control_plane_data_vpn_edgeToEdgeDetail.md) |  | [optional] [default to null]
**EdgeToEdgeList** | [**[]ControlPlaneDataVpnEdgeToEdgeList**](control_plane_data_vpn_edgeToEdgeList.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


