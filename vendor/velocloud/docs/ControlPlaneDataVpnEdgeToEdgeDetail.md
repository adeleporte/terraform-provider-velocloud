# ControlPlaneDataVpnEdgeToEdgeDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Dynamic** | [***ControlPlaneDataVpnEdgeToEdgeDetailDynamic**](control_plane_data_vpn_edgeToEdgeDetail_dynamic.md) |  | [optional] [default to null]
**EncryptionProtocol** | **string** |  | [optional] [default to null]
**ProfileIsolation** | [***ControlPlaneDataVpnEdgeToEdgeDetailProfileIsolation**](control_plane_data_vpn_edgeToEdgeDetail_profileIsolation.md) |  | [optional] [default to null]
**UseCloudGateway** | **bool** |  | [optional] [default to null]
**VpnHubs** | [**[]interface{}**](interface{}.md) |  | [optional] [default to null]
**AutoSelectVpnHubs** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


