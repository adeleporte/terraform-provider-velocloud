# CosMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**High** | [***CosMappingValue**](cos_mapping_value.md) |  | [optional] [default to null]
**Normal** | [***CosMappingValue**](cos_mapping_value.md) |  | [optional] [default to null]
**Low** | [***CosMappingValue**](cos_mapping_value.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


