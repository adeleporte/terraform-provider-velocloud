# DeviceSettingsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Lan** | [***DeviceSettingsDataLan**](deviceSettingsData_lan.md) |  | [optional] [default to null]
**Ospf** | [***DeviceSettingsDataOspf**](deviceSettingsData_ospf.md) |  | [optional] [default to null]
**Bgp** | [***DeviceSettingsDataBgp**](deviceSettingsData_bgp.md) |  | [optional] [default to null]
**Dns** | [***EdgeDeviceSettingsDataDns**](edgeDeviceSettingsData_dns.md) |  | [optional] [default to null]
**Authentication** | [***EdgeDeviceSettingsDataDnsPrimaryProvider**](edgeDeviceSettingsData_dns_primaryProvider.md) |  | [optional] [default to null]
**SoftwareUpdate** | [***DeviceSettingsDataSoftwareUpdate**](deviceSettingsData_softwareUpdate.md) |  | [optional] [default to null]
**RadioSettings** | [***DeviceSettingsDataRadioSettings**](deviceSettingsData_radioSettings.md) |  | [optional] [default to null]
**Netflow** | [***DeviceSettingsDataNetflow**](deviceSettingsData_netflow.md) |  | [optional] [default to null]
**Vqm** | [***DeviceSettingsDataVqm**](deviceSettingsData_vqm.md) |  | [optional] [default to null]
**Snmp** | [***DeviceSettingsDataSnmp**](deviceSettingsData_snmp.md) |  | [optional] [default to null]
**MultiSourceQos** | [***DeviceSettingsDataMultiSourceQos**](deviceSettingsData_multiSourceQos.md) |  | [optional] [default to null]
**Models** | [***DeviceSettingsDataModels**](deviceSettingsData_models.md) |  | [optional] [default to null]
**Vpn** | [***DeviceSettingsDataVpn**](deviceSettingsData_vpn.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


