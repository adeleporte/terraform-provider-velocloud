# DeviceSettingsDataLanAllocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Ref** | **string** |  | [optional] [default to null]
**AssignableVlans** | **[]int32** |  | [optional] [default to null]
**ManagementVlans** | **[]int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


