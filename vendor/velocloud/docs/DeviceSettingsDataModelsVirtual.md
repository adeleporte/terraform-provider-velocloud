# DeviceSettingsDataModelsVirtual

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RoutedInterfaces** | [**[]DeviceSettingsDataModelsVirtualRoutedInterfaces**](deviceSettingsData_models_virtual_routedInterfaces.md) |  | [optional] [default to null]
**Lan** | [***DeviceSettingsDataModelsVirtualLan**](deviceSettingsData_models_virtual_lan.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


