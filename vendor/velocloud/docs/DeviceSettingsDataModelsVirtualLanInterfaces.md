# DeviceSettingsDataModelsVirtualLanInterfaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Space** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Cwp** | **bool** |  | [optional] [default to null]
**PortMode** | **string** |  | [optional] [default to null]
**UntaggedVlan** | **string** |  | [optional] [default to null]
**Disabled** | **bool** |  | [optional] [default to null]
**L2** | [***EdgeDeviceSettingsDataL2**](edgeDeviceSettingsData_l2.md) |  | [optional] [default to null]
**VlanIds** | **[]int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


