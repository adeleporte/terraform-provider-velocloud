# DeviceSettingsDataModelsVirtualRoutedInterfaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] [default to null]
**Disabled** | **bool** |  | [optional] [default to null]
**Addressing** | [***DeviceSettingsDataModelsVirtualAddressing**](deviceSettingsData_models_virtual_addressing.md) |  | [optional] [default to null]
**WanOverlay** | **string** |  | [optional] [default to null]
**NatDirect** | **bool** |  | [optional] [default to null]
**Ospf** | [***DeviceSettingsDataModelsVirtualOspf**](deviceSettingsData_models_virtual_ospf.md) |  | [optional] [default to null]
**VlanId** | **int32** |  | [optional] [default to null]
**L2** | [***EdgeDeviceSettingsDataL2**](edgeDeviceSettingsData_l2.md) |  | [optional] [default to null]
**UnderlayAccounting** | **bool** |  | [optional] [default to null]
**Trusted** | **bool** |  | [optional] [default to null]
**Rpf** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


