# DeviceSettingsDataMultiSourceQos

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**HighRatio** | **int32** |  | [optional] [default to null]
**NormalRatio** | **int32** |  | [optional] [default to null]
**LowRatio** | **int32** |  | [optional] [default to null]
**MaxCapThreshold** | **int32** |  | [optional] [default to null]
**MinCapThreshold** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


