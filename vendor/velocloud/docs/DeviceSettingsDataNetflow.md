# DeviceSettingsDataNetflow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**Version** | **int32** |  | [optional] [default to null]
**Collectors** | [**[]EdgeDeviceSettingsDataNetflowCollectors**](edgeDeviceSettingsData_netflow_collectors.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


