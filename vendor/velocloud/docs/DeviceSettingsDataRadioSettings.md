# DeviceSettingsDataRadioSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Country** | **string** |  | [optional] [default to null]
**Radios** | [**[]DeviceSettingsDataRadioSettingsRadios**](deviceSettingsData_radioSettings_radios.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


