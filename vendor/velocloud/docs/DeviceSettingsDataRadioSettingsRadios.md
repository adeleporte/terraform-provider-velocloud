# DeviceSettingsDataRadioSettingsRadios

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RadioId** | **int32** |  | [optional] [default to null]
**IsEnabled** | **bool** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Band** | **string** |  | [optional] [default to null]
**Channel** | **string** |  | [optional] [default to null]
**Width** | **string** |  | [optional] [default to null]
**Mode** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


