# DeviceSettingsDataSnmp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Port** | **int32** |  | [optional] [default to null]
**Snmpv2c** | [***DeviceSettingsDataSnmpSnmpv2c**](deviceSettingsData_snmp_snmpv2c.md) |  | [optional] [default to null]
**Snmpv3** | [***DeviceSettingsDataSnmpSnmpv3**](deviceSettingsData_snmp_snmpv3.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


