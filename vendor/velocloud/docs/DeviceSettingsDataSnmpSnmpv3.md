# DeviceSettingsDataSnmpSnmpv3

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**Users** | [**[]EdgeDeviceSettingsDataSnmpSnmpv3Users**](edgeDeviceSettingsData_snmp_snmpv3_users.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


