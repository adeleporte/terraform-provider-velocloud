# DeviceSettingsDataSoftwareUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Windowed** | **bool** |  | [optional] [default to null]
**Window** | [***DeviceSettingsDataSoftwareUpdateWindow**](deviceSettingsData_softwareUpdate_window.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


