# DeviceSettingsDataSoftwareUpdateWindow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Day** | **int32** |  | [optional] [default to null]
**BeginHour** | **int32** |  | [optional] [default to null]
**EndHour** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


