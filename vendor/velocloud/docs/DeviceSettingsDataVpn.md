# DeviceSettingsDataVpn

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**EdgeToDataCenter** | **bool** |  | [optional] [default to null]
**Ref** | **string** |  | [optional] [default to null]
**EdgeToEdgeHub** | [***DeviceSettingsDataVpnEdgeToEdgeHub**](deviceSettingsData_vpn_edgeToEdgeHub.md) |  | [optional] [default to null]
**EdgeToEdge** | **bool** |  | [optional] [default to null]
**EdgeToEdgeDetail** | [***DeviceSettingsDataVpnEdgeToEdgeDetail**](deviceSettingsData_vpn_edgeToEdgeDetail.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


