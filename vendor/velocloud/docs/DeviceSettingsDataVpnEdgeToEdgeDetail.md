# DeviceSettingsDataVpnEdgeToEdgeDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UseCloudGateway** | **bool** |  | [optional] [default to null]
**EncryptionProtocol** | **string** |  | [optional] [default to null]
**Dynamic** | [***DeviceSettingsDataVpnEdgeToEdgeDetailDynamic**](deviceSettingsData_vpn_edgeToEdgeDetail_dynamic.md) |  | [optional] [default to null]
**VpnHubs** | [**[]interface{}**](interface{}.md) |  | [optional] [default to null]
**AutoSelectVpnHubs** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


