# DeviceSettingsRefs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DeviceSettingssegment** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**DeviceSettingsdnsprimaryProvider** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**DeviceSettingsdnsbackupProvider** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**DeviceSettingsdnsprivateProviders** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**DeviceSettingslanallocation** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**DeviceSettingstacacs** | [***interface{}**](interface{}.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


