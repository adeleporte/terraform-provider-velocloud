# \DisasterRecoveryApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DisasterRecoveryConfigureActiveForReplication**](DisasterRecoveryApi.md#DisasterRecoveryConfigureActiveForReplication) | **Post** /disasterRecovery/configureActiveForReplication | Designate a standby Orchestrator for disaster recovery replication
[**DisasterRecoveryDemoteActive**](DisasterRecoveryApi.md#DisasterRecoveryDemoteActive) | **Post** /disasterRecovery/demoteActive | Demote current server from active to zombie
[**DisasterRecoveryGetReplicationBlob**](DisasterRecoveryApi.md#DisasterRecoveryGetReplicationBlob) | **Post** /disasterRecovery/getReplicationBlob | Get the blob needed to configure replication on the standby
[**DisasterRecoveryGetReplicationStatus**](DisasterRecoveryApi.md#DisasterRecoveryGetReplicationStatus) | **Post** /disasterRecovery/getReplicationStatus | Get disaster recovery status
[**DisasterRecoveryPrepareForStandby**](DisasterRecoveryApi.md#DisasterRecoveryPrepareForStandby) | **Post** /disasterRecovery/prepareForStandby | Prepare current Orchestrator to be configured as a standby system
[**DisasterRecoveryPromoteStandbyToActive**](DisasterRecoveryApi.md#DisasterRecoveryPromoteStandbyToActive) | **Post** /disasterRecovery/promoteStandbyToActive | Promote the current server to take over as the single standalone VCO
[**DisasterRecoveryRemoveStandby**](DisasterRecoveryApi.md#DisasterRecoveryRemoveStandby) | **Post** /disasterRecovery/removeStandby | Unconfigure disaster recovery on the current server
[**DisasterRecoveryTransitionToStandby**](DisasterRecoveryApi.md#DisasterRecoveryTransitionToStandby) | **Post** /disasterRecovery/transitionToStandby | Configure current Orchestrator to transition to standby in disaster recovery active/standby pair.
[**VcoInventoryAssociateEdge**](DisasterRecoveryApi.md#VcoInventoryAssociateEdge) | **Post** /vcoInventory/associateEdge | Return inventory items available at this VCO


# **DisasterRecoveryConfigureActiveForReplication**
> DisasterRecoveryConfigureActiveForReplicationResult DisasterRecoveryConfigureActiveForReplication(ctx, body)
Designate a standby Orchestrator for disaster recovery replication

Configure the current Orchestrator to be active and the specified Orchestrator to be standby for Orchestrator disaster recovery replication. Required attributes are 1) standbyList, a single-entry array containing the standbyAddress and standbyUuid, 2) drVCOUser, a Orchestrator super user available on both the active and standby VCOs, and 3) drVCOPassword, the password of drVCOUser on the standby Orchestrator (unless the autoConfigStandby option is specified as false). The call sets up the active Orchestrator to allow replication from the standby and then (unless autoConfigStandby is false) makes a transitionToStandby API call to the specified standby, expected to have been previously placed in STANDBY_CANDIDATE state via prepareForStandby.  After this call, the active and standby VCOs should be polled via getReplicationStatus until they  both reach STANDBY_RUNNING drState (or a configuration error is reported).  (Note: the drVCOPassword is not persisted.)  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryConfigureActiveForReplication**](DisasterRecoveryConfigureActiveForReplication.md)|  | 

### Return type

[**DisasterRecoveryConfigureActiveForReplicationResult**](disaster_recovery_configure_active_for_replication_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryDemoteActive**
> DisasterRecoveryDemoteActiveResult DisasterRecoveryDemoteActive(ctx, body)
Demote current server from active to zombie

No input parameters are required.  The active server is expected to be in the drState FAILURE_GET_STANDBY_STATUS or FAILURE_MYSQL_ACTIVE_STATUS, meaning that DR protection had been engaged (with the last successful replication status observed at lastDRProtectedTime) but that active failed a health check since that time.  If the active server is in the drState STANDBY_RUNNING, meaning that it has detected no problems in interacting with the standby server, the operator can force demotion of the active using the optional parameter force passed with value of true; in this case, the operator must ensure the standby server has already been successfully promoted.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryDemoteActive**](DisasterRecoveryDemoteActive.md)|  | 

### Return type

[**DisasterRecoveryDemoteActiveResult**](disaster_recovery_demote_active_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryGetReplicationBlob**
> DisasterRecoveryGetReplicationBlobResult DisasterRecoveryGetReplicationBlob(ctx, body)
Get the blob needed to configure replication on the standby

Get from the active Orchestrator the blob needed to configure replication on the standby. Only used when configureActiveForReplication was called with autoConfigStandby set to false [true by default].  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryGetReplicationBlob**](DisasterRecoveryGetReplicationBlob.md)|  | 

### Return type

[**DisasterRecoveryGetReplicationBlobResult**](disaster_recovery_get_replication_blob_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryGetReplicationStatus**
> DisasterRecoveryGetReplicationStatusResult DisasterRecoveryGetReplicationStatus(ctx, body)
Get disaster recovery status

Get disaster recovery replication status, optionally with client contact, state transition history, and storage information.  No input parameters are required.  Can optionally specify 1 or more of the following with parameters: clientContact,clientCount,stateHistory,storageInfo.  Privileges required:  `READ` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryGetReplicationStatus**](DisasterRecoveryGetReplicationStatus.md)|  | 

### Return type

[**DisasterRecoveryGetReplicationStatusResult**](disaster_recovery_get_replication_status_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryPrepareForStandby**
> DisasterRecoveryPrepareForStandbyResult DisasterRecoveryPrepareForStandby(ctx, body)
Prepare current Orchestrator to be configured as a standby system

Transitions the current Orchestrator to a quiesced state, ready to be configured as a standby system. No input parameters are required.  After this call, the Orchestrator will be restarted in standby mode. The caller should subsequently poll `getReplicationStatus` until `drState` is `STANDBY_CANDIDATE`.  This is the first step in configuring Orchestrator disaster recovery.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryPrepareForStandby**](DisasterRecoveryPrepareForStandby.md)|  | 

### Return type

[**DisasterRecoveryPrepareForStandbyResult**](disaster_recovery_prepare_for_standby_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryPromoteStandbyToActive**
> DisasterRecoveryPromoteStandbyToActiveResult DisasterRecoveryPromoteStandbyToActive(ctx, body)
Promote the current server to take over as the single standalone VCO

The current server is expected to be a standby in the drState FAILURE_MYSQL_STANDBY_STATUS, meaning that DR protection had been engaged (with the last successful replication status observed at lastDRProtectedTime) but that standby has been unable to replicate since that time. If the standby server is in the drState STANDBY_RUNNING, meaning that it has detected no problems in replicating from the active server, the operator can force promotion of the standby using the optional parameter force passed with value of true; in this case, the standby server will call demoteActive/force on the active.  The operator should, if possible, ensure the formerly active server is demoted by running demoteServer directly on that server if the standby server was unable to do so successfully.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryPromoteStandbyToActive**](DisasterRecoveryPromoteStandbyToActive.md)|  | 

### Return type

[**DisasterRecoveryPromoteStandbyToActiveResult**](disaster_recovery_promote_standby_to_active_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryRemoveStandby**
> DisasterRecoveryRemoveStandbyResult DisasterRecoveryRemoveStandby(ctx, body)
Unconfigure disaster recovery on the current server

Unconfigure disaster recovery on the current server.  Also, make a best-effort call to removeStandby on the paired DR server. No input parameters are required.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryRemoveStandby**](DisasterRecoveryRemoveStandby.md)|  | 

### Return type

[**DisasterRecoveryRemoveStandbyResult**](disaster_recovery_remove_standby_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DisasterRecoveryTransitionToStandby**
> DisasterRecoveryTransitionToStandbyResult DisasterRecoveryTransitionToStandby(ctx, body)
Configure current Orchestrator to transition to standby in disaster recovery active/standby pair.

Configure current Orchestrator to transition to standby in disaster recovery active/standby pair. Requires parameter activeAccessFromStandby, which contains the data needed to configure standby. This data is produced by configureActiveForReplication, which by default, automatically calls transitionToStandby; an explicit call is only needed, with a blob obtained from getReplicationBlob, if configureActiveForReplication is called with autoConfigStandby set false.  Privileges required:  `CREATE` `REPLICATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**DisasterRecoveryTransitionToStandby**](DisasterRecoveryTransitionToStandby.md)|  | 

### Return type

[**DisasterRecoveryTransitionToStandbyResult**](disaster_recovery_transition_to_standby_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **VcoInventoryAssociateEdge**
> InlineResponse2001 VcoInventoryAssociateEdge(ctx, body)
Return inventory items available at this VCO

Assigns an edge in the inventory to an Enterprise. To perform the action, the edge should already be in a STAGING state. The assignment can be done at an enterprise level, without selecting a destination Edge profile. In such a case, the inventory edge is assigned to a staging profile within the Enterprise. Optionally a profile or destination edge can be assigned to this inventory edge. The edge in the inventory can be assigned to any profile. The inventory edge can be assigned to an Enterprise edge only if it is in a PENDING/REACTIVATION_PENDING state.  Privileges required:  `CREATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**VcoInventoryAssociateEdge**](VcoInventoryAssociateEdge.md)|  | 

### Return type

[**InlineResponse2001**](inline_response_200_1.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

