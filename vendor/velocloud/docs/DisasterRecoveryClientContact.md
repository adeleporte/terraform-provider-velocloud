# DisasterRecoveryClientContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActiveAddress** | **string** |  | [default to null]
**ActiveLastResponseTime** | [**time.Time**](time.Time.md) |  | [default to null]
**ClientLogicalId** | **string** |  | [default to null]
**ClientType** | **string** |  | [default to null]
**Id** | **int32** |  | [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [default to null]
**StandbyAddress** | **string** |  | [default to null]
**StandbyLastResponseTime** | [**time.Time**](time.Time.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


