# DisasterRecoveryConfigureActiveForReplication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StandbyList** | [**[]StandbyVcoDesignee**](standby_vco_designee.md) |  | [default to null]
**AutoConfigStandby** | **bool** |  | [optional] [default to null]
**DrVCOUser** | **string** |  | [default to null]
**DrVCOPassword** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


