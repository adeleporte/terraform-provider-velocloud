# DisasterRecoveryGetReplicationStatusResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActiveAddress** | **string** |  | [default to null]
**ActiveReplicationAddress** | **string** |  | [optional] [default to null]
**ClientContact** | [**[]DisasterRecoveryClientContact**](disaster_recovery_client_contact.md) |  | [optional] [default to null]
**ClientCount** | [***DisasterRecoveryGetReplicationStatusResultClientCount**](disaster_recovery_get_replication_status_result_clientCount.md) |  | [optional] [default to null]
**DrState** | **string** |  | [default to null]
**DrVCOUser** | **string** |  | [default to null]
**FailureDescription** | **string** |  | [default to null]
**LastDrProtectedTime** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Role** | **string** |  | [default to null]
**RoleTimestamp** | [**time.Time**](time.Time.md) |  | [default to null]
**StandbyList** | [**[]DisasterRecoveryGetReplicationStatusResultStandbyList**](disaster_recovery_get_replication_status_result_standbyList.md) |  | [default to null]
**StateHistory** | [**[]DisasterRecoveryGetReplicationStatusResultStateHistory**](disaster_recovery_get_replication_status_result_stateHistory.md) |  | [optional] [default to null]
**StateTimestamp** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**VcoIp** | **string** |  | [default to null]
**VcoReplicationIp** | **string** |  | [optional] [default to null]
**VcoUuid** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


