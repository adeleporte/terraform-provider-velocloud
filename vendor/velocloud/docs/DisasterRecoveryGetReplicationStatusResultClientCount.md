# DisasterRecoveryGetReplicationStatusResultClientCount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EdgeCount** | **int32** |  | [optional] [default to null]
**GatewayCount** | **int32** |  | [optional] [default to null]
**CurrentActiveEdgeCount** | **int32** |  | [optional] [default to null]
**CurrentStandbyEdgeCount** | **int32** |  | [optional] [default to null]
**CurrentActiveGatewayCount** | **int32** |  | [optional] [default to null]
**CurrentStandbyGatewayCount** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


