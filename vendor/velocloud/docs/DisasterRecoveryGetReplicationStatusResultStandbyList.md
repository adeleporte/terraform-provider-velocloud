# DisasterRecoveryGetReplicationStatusResultStandbyList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StandbyAddress** | **string** |  | [optional] [default to null]
**StandbyReplicationAddress** | **string** |  | [optional] [default to null]
**StandbyUuid** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


