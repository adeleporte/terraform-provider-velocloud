# DnsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Primary** | **string** |  | [optional] [default to null]
**Secondary** | **string** |  | [optional] [default to null]
**IsPrivate** | **bool** |  | [optional] [default to null]
**Domains** | [**[]DnsDataDomains**](dns_data_domains.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


