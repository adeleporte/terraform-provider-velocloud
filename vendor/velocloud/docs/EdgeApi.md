# \EdgeApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**EdgeDeleteEdge**](EdgeApi.md#EdgeDeleteEdge) | **Post** /edge/deleteEdge | Delete an edge
[**EdgeDeleteEdgeBgpNeighborRecords**](EdgeApi.md#EdgeDeleteEdgeBgpNeighborRecords) | **Post** /edge/deleteEdgeBgpNeighborRecords | Delete edge BGP neighbor records
[**EdgeEdgeCancelReactivation**](EdgeApi.md#EdgeEdgeCancelReactivation) | **Post** /edge/edgeCancelReactivation | Cancel a pending edge reactivation request
[**EdgeEdgeProvision**](EdgeApi.md#EdgeEdgeProvision) | **Post** /edge/edgeProvision | Provision an edge
[**EdgeEdgeRequestReactivation**](EdgeApi.md#EdgeEdgeRequestReactivation) | **Post** /edge/edgeRequestReactivation | Reactivate an edge
[**EdgeGetClientVisibilityMode**](EdgeApi.md#EdgeGetClientVisibilityMode) | **Post** /edge/getClientVisibilityMode | Get an edge&#39;s client visibility mode
[**EdgeGetEdge**](EdgeApi.md#EdgeGetEdge) | **Post** /edge/getEdge | Get edge
[**EdgeGetEdgeConfigurationStack**](EdgeApi.md#EdgeGetEdgeConfigurationStack) | **Post** /edge/getEdgeConfigurationStack | Get an edge&#39;s configuration stack
[**EdgeSetEdgeEnterpriseConfiguration**](EdgeApi.md#EdgeSetEdgeEnterpriseConfiguration) | **Post** /edge/setEdgeEnterpriseConfiguration | Apply an enterprise configuration to an Edge
[**EdgeSetEdgeHandOffGateways**](EdgeApi.md#EdgeSetEdgeHandOffGateways) | **Post** /edge/setEdgeHandOffGateways | Set an edge&#39;s on-premise hand off gateways
[**EdgeSetEdgeOperatorConfiguration**](EdgeApi.md#EdgeSetEdgeOperatorConfiguration) | **Post** /edge/setEdgeOperatorConfiguration | Apply an operator configuration to an Edge
[**EdgeUpdateEdgeAdminPassword**](EdgeApi.md#EdgeUpdateEdgeAdminPassword) | **Post** /edge/updateEdgeAdminPassword | Update edge&#39;s local UI authentication credentials
[**EdgeUpdateEdgeAttributes**](EdgeApi.md#EdgeUpdateEdgeAttributes) | **Post** /edge/updateEdgeAttributes | Update edge attributes
[**EdgeUpdateEdgeCredentialsByConfiguration**](EdgeApi.md#EdgeUpdateEdgeCredentialsByConfiguration) | **Post** /edge/updateEdgeCredentialsByConfiguration | Update edge UI credentials by configuration id


# **EdgeDeleteEdge**
> []EdgeDeleteEdgeResultItem EdgeDeleteEdge(ctx, body)
Delete an edge

Delete an edge by id.  Privileges required:  `DELETE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeDeleteEdge**](EdgeDeleteEdge.md)|  | 

### Return type

[**[]EdgeDeleteEdgeResultItem**](edge_delete_edge_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeDeleteEdgeBgpNeighborRecords**
> EdgeDeleteEdgeBgpNeighborRecordsResult EdgeDeleteEdgeBgpNeighborRecords(ctx, body)
Delete edge BGP neighbor records

Deletes BGP record(s) matching the given record keys (neighborIp) on the edges with the given IDs, if they exist.  Privileges required:  `DELETE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeDeleteEdgeBgpNeighborRecords**](EdgeDeleteEdgeBgpNeighborRecords.md)|  | 

### Return type

[**EdgeDeleteEdgeBgpNeighborRecordsResult**](edge_delete_edge_bgp_neighbor_records_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeEdgeCancelReactivation**
> EdgeEdgeCancelReactivationResult EdgeEdgeCancelReactivation(ctx, body)
Cancel a pending edge reactivation request

Cancel a pending reactivation edge reactivation request.  Privileges required:  `CREATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeEdgeCancelReactivation**](EdgeEdgeCancelReactivation.md)|  | 

### Return type

[**EdgeEdgeCancelReactivationResult**](edge_edge_cancel_reactivation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeEdgeProvision**
> EdgeEdgeProvisionResult EdgeEdgeProvision(ctx, body)
Provision an edge

Provision an edge prior to activation.  Privileges required:  `CREATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeEdgeProvision**](EdgeEdgeProvision.md)|  | 

### Return type

[**EdgeEdgeProvisionResult**](edge_edge_provision_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeEdgeRequestReactivation**
> EdgeEdgeRequestReactivationResult EdgeEdgeRequestReactivation(ctx, body)
Reactivate an edge

Update activation state for an edge to REACTIVATION_PENDING.  Privileges required:  `CREATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeEdgeRequestReactivation**](EdgeEdgeRequestReactivation.md)|  | 

### Return type

[**EdgeEdgeRequestReactivationResult**](edge_edge_request_reactivation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeGetClientVisibilityMode**
> EdgeGetClientVisibilityModeResult EdgeGetClientVisibilityMode(ctx, body)
Get an edge's client visibility mode

Retrieve an edge's client visibility mode.  Privileges required:  `READ` `EDGE`  `VIEW_FLOW_STATS` `undefined`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeGetClientVisibilityMode**](EdgeGetClientVisibilityMode.md)|  | 

### Return type

[**EdgeGetClientVisibilityModeResult**](edge_get_client_visibility_mode_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeGetEdge**
> EdgeGetEdgeResult EdgeGetEdge(ctx, body)
Get edge

Gets the specified Edge with optional link, site, configuration, certificate, or enterprise details. Supports queries by Edge `id`, `deviceId`, `activationKey`, and `logicalId`.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeGetEdge**](EdgeGetEdge.md)|  | 

### Return type

[**EdgeGetEdgeResult**](edge_get_edge_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeGetEdgeConfigurationStack**
> []EdgeGetEdgeConfigurationStackResultItem EdgeGetEdgeConfigurationStack(ctx, body)
Get an edge's configuration stack

Retrieve an edge's complete configuration profile, with all modules included.  Privileges required:  `READ` `EDGE`  `READ` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeGetEdgeConfigurationStack**](EdgeGetEdgeConfigurationStack.md)|  | 

### Return type

[**[]EdgeGetEdgeConfigurationStackResultItem**](edge_get_edge_configuration_stack_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeSetEdgeEnterpriseConfiguration**
> EdgeSetEdgeEnterpriseConfigurationResult EdgeSetEdgeEnterpriseConfiguration(ctx, body)
Apply an enterprise configuration to an Edge

Sets the enterprise configuration for the specified Edge (by `edgeId`).  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeSetEdgeEnterpriseConfiguration**](EdgeSetEdgeEnterpriseConfiguration.md)|  | 

### Return type

[**EdgeSetEdgeEnterpriseConfigurationResult**](edge_set_edge_enterprise_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeSetEdgeHandOffGateways**
> EdgeSetEdgeHandOffGatewaysResult EdgeSetEdgeHandOffGateways(ctx, body)
Set an edge's on-premise hand off gateways

Set an edge's on-premise hand off gateways. A primary and secondary gateway are defined, primary is required, secondary is optional. All existing edge-gateway hand off relationships are moved and are replaced by the the specified primary and secondary gateways.  Privileges required:  `UPDATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeSetEdgeHandOffGateways**](EdgeSetEdgeHandOffGateways.md)|  | 

### Return type

[**EdgeSetEdgeHandOffGatewaysResult**](edge_set_edge_hand_off_gateways_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeSetEdgeOperatorConfiguration**
> EdgeSetEdgeOperatorConfigurationResult EdgeSetEdgeOperatorConfiguration(ctx, body)
Apply an operator configuration to an Edge

Set an Edge's operator configuration. This overrides any enterprise-assigned operator configuration and the network default operator configuration.  Privileges required:  `UPDATE` `EDGE`  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeSetEdgeOperatorConfiguration**](EdgeSetEdgeOperatorConfiguration.md)|  | 

### Return type

[**EdgeSetEdgeOperatorConfigurationResult**](edge_set_edge_operator_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeUpdateEdgeAdminPassword**
> EdgeUpdateEdgeAdminPasswordResult EdgeUpdateEdgeAdminPassword(ctx, body)
Update edge's local UI authentication credentials

Request an update to the edge's local UI authentication credentials. On success, returns a JSON object with the ID of the action queued, status for which can be queried using the edgeAction/getEdgeAction API  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_KEYS`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeUpdateEdgeAdminPassword**](EdgeUpdateEdgeAdminPassword.md)|  | 

### Return type

[**EdgeUpdateEdgeAdminPasswordResult**](edge_update_edge_admin_password_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeUpdateEdgeAttributes**
> EdgeUpdateEdgeAttributesResult EdgeUpdateEdgeAttributes(ctx, body)
Update edge attributes

Update basic edge attributes, including edge name, description, site information, or serial number.  Privileges required:  `UPDATE` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeUpdateEdgeAttributes**](EdgeUpdateEdgeAttributes.md)|  | 

### Return type

[**EdgeUpdateEdgeAttributesResult**](edge_update_edge_attributes_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EdgeUpdateEdgeCredentialsByConfiguration**
> EdgeUpdateEdgeCredentialsByConfigurationResult EdgeUpdateEdgeCredentialsByConfiguration(ctx, body)
Update edge UI credentials by configuration id

Request an update to the edge-local UI authentication credentials for all edges belonging to a configuration profile. On success, returns a JSON object containing a list of each of the action IDs queued.  Privileges required:  `UPDATE` `EDGE`  `UPDATE` `ENTERPRISE_KEYS`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EdgeUpdateEdgeCredentialsByConfiguration**](EdgeUpdateEdgeCredentialsByConfiguration.md)|  | 

### Return type

[**EdgeUpdateEdgeCredentialsByConfigurationResult**](edge_update_edge_credentials_by_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

