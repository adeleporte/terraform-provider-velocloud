# EdgeDeviceSettingsData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Bgp** | [***EdgeDeviceSettingsDataBgp**](edgeDeviceSettingsData_bgp.md) |  | [optional] [default to null]
**Lan** | [***EdgeDeviceSettingsDataLan**](edgeDeviceSettingsData_lan.md) |  | [optional] [default to null]
**RoutedInterfaces** | [**[]EdgeDeviceSettingsDataRoutedInterfaces**](edgeDeviceSettingsData_routedInterfaces.md) |  | [optional] [default to null]
**Routes** | [***EdgeDeviceSettingsDataRoutes**](edgeDeviceSettingsData_routes.md) |  | [optional] [default to null]
**Ha** | [***EdgeDeviceSettingsDataHa**](edgeDeviceSettingsData_ha.md) |  | [optional] [default to null]
**Dns** | [***EdgeDeviceSettingsDataDns**](edgeDeviceSettingsData_dns.md) |  | [optional] [default to null]
**Netflow** | [***EdgeDeviceSettingsDataNetflow**](edgeDeviceSettingsData_netflow.md) |  | [optional] [default to null]
**Vqm** | [***EdgeDeviceSettingsDataVqm**](edgeDeviceSettingsData_vqm.md) |  | [optional] [default to null]
**Vrrp** | [***EdgeDeviceSettingsDataVrrp**](edgeDeviceSettingsData_vrrp.md) |  | [optional] [default to null]
**Snmp** | [***EdgeDeviceSettingsDataSnmp**](edgeDeviceSettingsData_snmp.md) |  | [optional] [default to null]
**MultiSourceQos** | [***EdgeDeviceSettingsDataMultiSourceQos**](edgeDeviceSettingsData_multiSourceQos.md) |  | [optional] [default to null]
**Tacacs** | [***EdgeDeviceSettingsDataTacacs**](edgeDeviceSettingsData_tacacs.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


