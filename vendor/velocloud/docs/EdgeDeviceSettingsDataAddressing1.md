# EdgeDeviceSettingsDataAddressing1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CidrIp** | **string** |  | [optional] [default to null]
**CidrPrefix** | **int32** |  | [optional] [default to null]
**Gateway** | **string** |  | [optional] [default to null]
**Netmask** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Username** | **string** |  | [optional] [default to null]
**Password** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


