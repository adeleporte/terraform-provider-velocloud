# EdgeDeviceSettingsDataBgp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ASN** | **string** |  | [optional] [default to null]
**ConnectedRoutes** | **bool** |  | [optional] [default to null]
**DisableASPathCarryOver** | **bool** |  | [optional] [default to null]
**Enabled** | **bool** |  | [optional] [default to null]
**Filters** | [**[]EdgeDeviceSettingsDataBgpFilters**](edgeDeviceSettingsData_bgp_filters.md) |  | [optional] [default to null]
**Holdtime** | **string** |  | [optional] [default to null]
**Keepalive** | **string** |  | [optional] [default to null]
**Neighbors** | [**[]ConfigEdgeBgpNeighbor**](config_edge_bgp_neighbor.md) |  | [optional] [default to null]
**Networks** | [**[]EdgeDeviceSettingsDataBgpNetworks**](edgeDeviceSettingsData_bgp_networks.md) |  | [optional] [default to null]
**OverlayPrefix** | **bool** |  | [optional] [default to null]
**PropagateUplink** | **bool** |  | [optional] [default to null]
**RouterId** | **string** |  | [optional] [default to null]
**UplinkCommunity** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


