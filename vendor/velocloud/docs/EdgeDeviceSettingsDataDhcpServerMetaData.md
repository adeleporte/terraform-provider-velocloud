# EdgeDeviceSettingsDataDhcpServerMetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DataType** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Display** | **bool** |  | [optional] [default to null]
**List** | **bool** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Option** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


