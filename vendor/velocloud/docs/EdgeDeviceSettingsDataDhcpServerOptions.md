# EdgeDeviceSettingsDataDhcpServerOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Option** | **int32** |  | [optional] [default to null]
**Value** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**MetaData** | [***EdgeDeviceSettingsDataDhcpServerMetaData**](edgeDeviceSettingsDataDhcpServer_metaData.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


