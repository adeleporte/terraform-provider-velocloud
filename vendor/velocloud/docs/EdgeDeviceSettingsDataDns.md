# EdgeDeviceSettingsDataDns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PrimaryProvider** | [***EdgeDeviceSettingsDataDnsPrimaryProvider**](edgeDeviceSettingsData_dns_primaryProvider.md) |  | [optional] [default to null]
**BackupProvider** | [***EdgeDeviceSettingsDataDnsPrimaryProvider**](edgeDeviceSettingsData_dns_primaryProvider.md) |  | [optional] [default to null]
**PrivateProviders** | [***EdgeDeviceSettingsDataDnsPrimaryProvider**](edgeDeviceSettingsData_dns_primaryProvider.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


