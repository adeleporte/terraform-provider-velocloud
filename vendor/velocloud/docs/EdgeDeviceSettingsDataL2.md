# EdgeDeviceSettingsDataL2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Autonegotiation** | **bool** |  | [optional] [default to null]
**Speed** | **string** |  | [optional] [default to null]
**Duplex** | **string** |  | [optional] [default to null]
**MTU** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


