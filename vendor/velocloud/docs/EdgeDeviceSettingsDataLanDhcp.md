# EdgeDeviceSettingsDataLanDhcp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**LeaseTimeSeconds** | **int32** |  | [optional] [default to null]
**Override** | **bool** |  | [optional] [default to null]
**DhcpRelay** | [***EdgeDeviceSettingsDataLanDhcpDhcpRelay**](edgeDeviceSettingsData_lan_dhcp_dhcpRelay.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


