# EdgeDeviceSettingsDataLanNetworks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Space** | **string** |  | [optional] [default to null]
**Guest** | **bool** |  | [optional] [default to null]
**Secure** | **bool** |  | [optional] [default to null]
**Advertise** | **bool** |  | [optional] [default to null]
**Cost** | **int32** |  | [optional] [default to null]
**Dhcp** | [***EdgeDeviceSettingsDataLanDhcp**](edgeDeviceSettingsData_lan_dhcp.md) |  | [optional] [default to null]
**StaticReserved** | **int32** |  | [optional] [default to null]
**Netmask** | **string** |  | [optional] [default to null]
**CidrPrefix** | **int32** |  | [optional] [default to null]
**CidrIp** | **string** |  | [optional] [default to null]
**BaseDhcpAddr** | **int32** |  | [optional] [default to null]
**NumDhcpAddr** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Interfaces** | **[]string** |  | [optional] [default to null]
**VlanId** | **int32** |  | [optional] [default to null]
**ManagementIp** | **string** |  | [optional] [default to null]
**Disabled** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


