# EdgeDeviceSettingsDataOspf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Area** | **int32** |  | [optional] [default to null]
**Authentication** | **bool** |  | [optional] [default to null]
**AuthId** | **int32** |  | [optional] [default to null]
**AuthPassphrase** | **string** |  | [optional] [default to null]
**Cost** | **int32** |  | [optional] [default to null]
**DeadTimer** | **int32** |  | [optional] [default to null]
**Enabled** | **bool** |  | [optional] [default to null]
**HelloTimer** | **int32** |  | [optional] [default to null]
**InboundRouteLearning** | [***EdgeDeviceSettingsDataOspfInboundRouteLearning**](edgeDeviceSettingsData_ospf_inboundRouteLearning.md) |  | [optional] [default to null]
**Md5Authentication** | **bool** |  | [optional] [default to null]
**MTU** | **int32** |  | [optional] [default to null]
**OutboundRouteAdvertisement** | [***EdgeDeviceSettingsDataOspfInboundRouteLearning**](edgeDeviceSettingsData_ospf_inboundRouteLearning.md) |  | [optional] [default to null]
**Passive** | **bool** |  | [optional] [default to null]
**VlanId** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


