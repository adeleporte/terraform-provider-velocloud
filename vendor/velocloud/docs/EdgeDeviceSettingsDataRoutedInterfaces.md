# EdgeDeviceSettingsDataRoutedInterfaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Addressing** | [***EdgeDeviceSettingsDataAddressing**](edgeDeviceSettingsData_addressing.md) |  | [optional] [default to null]
**Advertise** | **bool** |  | [optional] [default to null]
**Disabled** | **bool** |  | [optional] [default to null]
**DhcpServer** | [***EdgeDeviceSettingsDataDhcpServer**](edgeDeviceSettingsDataDhcpServer.md) |  | [optional] [default to null]
**EncryptOverlay** | **bool** |  | [optional] [default to null]
**L2** | [***EdgeDeviceSettingsDataL2**](edgeDeviceSettingsData_l2.md) |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**NatDirect** | **bool** |  | [optional] [default to null]
**Ospf** | [***EdgeDeviceSettingsDataOspf**](edgeDeviceSettingsData_ospf.md) |  | [optional] [default to null]
**Override** | **bool** |  | [optional] [default to null]
**Subinterfaces** | [**[]EdgeDeviceSettingsDataSubinterfaces**](edgeDeviceSettingsData_subinterfaces.md) |  | [optional] [default to null]
**VlanId** | **int32** | static only | [optional] [default to null]
**WanOverlay** | **string** |  | [optional] [default to null]
**Trusted** | **bool** |  | [optional] [default to null]
**Rpf** | **string** |  | [optional] [default to null]
**UnderlayAccounting** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


