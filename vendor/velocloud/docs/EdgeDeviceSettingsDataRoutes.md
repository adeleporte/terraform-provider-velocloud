# EdgeDeviceSettingsDataRoutes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IcmpProbes** | [**[]interface{}**](interface{}.md) |  | [optional] [default to null]
**IcmpResponders** | [**[]interface{}**](interface{}.md) |  | [optional] [default to null]
**Static** | [**[]EdgeDeviceSettingsDataRoutesStatic**](edgeDeviceSettingsData_routes_static.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


