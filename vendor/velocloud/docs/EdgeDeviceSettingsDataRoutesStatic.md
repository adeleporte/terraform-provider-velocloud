# EdgeDeviceSettingsDataRoutesStatic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Destination** | **string** |  | [optional] [default to null]
**Netmask** | **string** |  | [optional] [default to null]
**SourceIp** | **string** |  | [optional] [default to null]
**Gateway** | **string** |  | [optional] [default to null]
**Cost** | **int32** |  | [optional] [default to null]
**Preferred** | **bool** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**CidrPrefix** | **string** |  | [optional] [default to null]
**WanInterface** | **string** |  | [optional] [default to null]
**IcmpProbeLogicalId** | **string** |  | [optional] [default to null]
**VlanId** | **int32** |  | [optional] [default to null]
**Advertise** | **bool** |  | [optional] [default to null]
**SubinterfaceId** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


