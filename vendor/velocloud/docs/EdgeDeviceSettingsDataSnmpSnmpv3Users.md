# EdgeDeviceSettingsDataSnmpSnmpv3Users

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] [default to null]
**Passphrase** | **string** |  | [optional] [default to null]
**AuthAlg** | **string** |  | [optional] [default to null]
**Privacy** | **bool** |  | [optional] [default to null]
**EncrAlg** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


