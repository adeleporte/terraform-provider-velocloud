# EdgeDeviceSettingsDataSubinterfaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Addressing** | [***EdgeDeviceSettingsDataAddressing1**](edgeDeviceSettingsData_addressing_1.md) |  | [optional] [default to null]
**Advertise** | **bool** |  | [optional] [default to null]
**DhcpServer** | [***EdgeDeviceSettingsDataDhcpServer**](edgeDeviceSettingsDataDhcpServer.md) |  | [optional] [default to null]
**Disabled** | **bool** |  | [optional] [default to null]
**NatDirect** | **bool** |  | [optional] [default to null]
**Ospf** | [***EdgeDeviceSettingsDataOspf1**](edgeDeviceSettingsData_ospf_1.md) |  | [optional] [default to null]
**Override** | **bool** |  | [optional] [default to null]
**SubinterfaceId** | **int32** |  | [optional] [default to null]
**SubinterfaceType** | **string** |  | [optional] [default to null]
**VlanId** | **int32** | static only | [optional] [default to null]
**Trusted** | **bool** |  | [optional] [default to null]
**Rpf** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


