# EdgeDeviceSettingsDataVqm

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enable** | **bool** |  | [optional] [default to null]
**Enabled** | **bool** |  | [optional] [default to null]
**Protocol** | **string** |  | [optional] [default to null]
**Collectors** | [**[]EdgeDeviceSettingsDataNetflowCollectors**](edgeDeviceSettingsData_netflow_collectors.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


