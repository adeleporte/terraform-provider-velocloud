# EdgeDeviceSettingsDataVrrp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**Data** | [**[]EdgeDeviceSettingsDataVrrpData**](edgeDeviceSettingsData_vrrp_data.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


