# EdgeDeviceSettingsDataVrrpData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CidrIp** | **string** |  | [optional] [default to null]
**Interface_** | **string** |  | [optional] [default to null]
**Interval** | **int32** |  | [optional] [default to null]
**Preempt** | **bool** |  | [optional] [default to null]
**PreemptDelay** | **int32** |  | [optional] [default to null]
**Priority** | **int32** |  | [optional] [default to null]
**SubinterfaceId** | **int32** |  | [optional] [default to null]
**VlanId** | **int32** |  | [optional] [default to null]
**Vrid** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


