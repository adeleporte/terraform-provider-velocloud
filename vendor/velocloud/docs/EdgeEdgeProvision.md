# EdgeEdgeProvision

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**ConfigurationId** | **int32** |  | [default to null]
**Name** | **string** |  | [optional] [default to null]
**SerialNumber** | **string** |  | [optional] [default to null]
**ModelNumber** | **string** |  | [default to null]
**Description** | **string** |  | [optional] [default to null]
**Site** | [***SiteObject**](site_object.md) |  | [optional] [default to null]
**HaEnabled** | **bool** |  | [optional] [default to null]
**GenerateCertificate** | **bool** |  | [optional] [default to null]
**SubjectCN** | **string** |  | [optional] [default to null]
**SubjectO** | **string** |  | [optional] [default to null]
**SubjectOU** | **string** |  | [optional] [default to null]
**ChallengePassword** | **string** |  | [optional] [default to null]
**PrivateKeyPassword** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


