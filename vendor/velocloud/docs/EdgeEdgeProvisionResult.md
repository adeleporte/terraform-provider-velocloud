# EdgeEdgeProvisionResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [default to null]
**ActivationKey** | **string** |  | [default to null]
**GeneratedCertificate** | [***EdgeEdgeProvisionResultGeneratedCertificate**](edge_edge_provision_result_generatedCertificate.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


