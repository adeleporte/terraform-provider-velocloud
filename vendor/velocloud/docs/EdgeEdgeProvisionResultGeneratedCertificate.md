# EdgeEdgeProvisionResultGeneratedCertificate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Certificate** | **string** |  | [optional] [default to null]
**CaCertificate** | **string** |  | [optional] [default to null]
**PrivateKey** | **string** |  | [optional] [default to null]
**PrivateKeyPassword** | **string** |  | [optional] [default to null]
**Csr** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


