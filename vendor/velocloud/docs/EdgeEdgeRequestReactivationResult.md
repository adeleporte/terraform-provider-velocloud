# EdgeEdgeRequestReactivationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActivationKey** | **string** |  | [default to null]
**ActivationKeyExpires** | [**time.Time**](time.Time.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


