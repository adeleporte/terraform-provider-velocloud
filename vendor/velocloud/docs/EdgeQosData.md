# EdgeQosData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Rules** | [**[]EdgeQosDataRules**](edgeQOSData_rules.md) |  | [optional] [default to null]
**Defaults** | [**[]interface{}**](interface{}.md) |  | [optional] [default to null]
**WebProxy** | [***EdgeQosDataWebProxy**](edgeQOSData_webProxy.md) |  | [optional] [default to null]
**ServiceRateLimit** | [***EdgeQosDataServiceRateLimit**](edgeQOSData_serviceRateLimit.md) |  | [optional] [default to null]
**CosMapping** | [***EdgeQosDataCosMapping**](edgeQOSData_cosMapping.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


