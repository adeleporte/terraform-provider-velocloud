# EdgeQosDataAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RouteType** | **string** |  | [optional] [default to null]
**Edge2EdgeRouteAction** | [***RouteActionObject**](route_action_object.md) |  | [optional] [default to null]
**Edge2DataCenterRouteAction** | [***RouteActionObject**](route_action_object.md) |  | [optional] [default to null]
**Edge2CloudRouteAction** | [***RouteActionObject**](route_action_object.md) |  | [optional] [default to null]
**QoS** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**Sla** | [***interface{}**](interface{}.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


