# EdgeQosDataCosMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LsInputType** | **string** |  | [optional] [default to null]
**Bulk** | [***CosMapping**](cos_mapping.md) |  | [optional] [default to null]
**Realtime** | [***CosMapping**](cos_mapping.md) |  | [optional] [default to null]
**Transactional** | [***CosMapping**](cos_mapping.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


