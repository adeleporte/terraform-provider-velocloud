# EdgeQosDataMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Appid** | **int32** |  | [optional] [default to null]
**Classid** | **int32** |  | [optional] [default to null]
**Dscp** | **int32** |  | [optional] [default to null]
**Sip** | **string** |  | [optional] [default to null]
**SportHigh** | **int32** |  | [optional] [default to null]
**SportLow** | **int32** |  | [optional] [default to null]
**Ssm** | **string** |  | [optional] [default to null]
**Svlan** | **int32** |  | [optional] [default to null]
**OsVersion** | **int32** |  | [optional] [default to null]
**Hostname** | **string** |  | [optional] [default to null]
**Dip** | **string** |  | [optional] [default to null]
**DportLow** | **int32** |  | [optional] [default to null]
**DportHigh** | **int32** |  | [optional] [default to null]
**Dsm** | **string** |  | [optional] [default to null]
**Dvlan** | **int32** |  | [optional] [default to null]
**Proto** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


