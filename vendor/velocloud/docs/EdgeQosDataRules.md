# EdgeQosDataRules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] [default to null]
**Match** | [***EdgeQosDataMatch**](edgeQOSData_match.md) |  | [optional] [default to null]
**Action** | [***EdgeQosDataAction**](edgeQOSData_action.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


