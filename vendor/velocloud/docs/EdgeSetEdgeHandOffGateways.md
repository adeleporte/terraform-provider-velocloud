# EdgeSetEdgeHandOffGateways

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EdgeId** | **int32** |  | [optional] [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Gateways** | [***EdgesetEdgeHandOffGatewaysGateways**](edgesetEdgeHandOffGateways_gateways.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


