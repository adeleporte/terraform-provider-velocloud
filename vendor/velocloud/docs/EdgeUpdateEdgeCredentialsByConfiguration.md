# EdgeUpdateEdgeCredentialsByConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConfigurationId** | **int32** |  | [optional] [default to null]
**Credentials** | [***EdgeupdateEdgeCredentialsByConfigurationCredentials**](edgeupdateEdgeCredentialsByConfiguration_credentials.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


