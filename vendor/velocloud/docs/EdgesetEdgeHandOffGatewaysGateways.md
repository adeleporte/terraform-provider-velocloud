# EdgesetEdgeHandOffGatewaysGateways

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Primary** | **int32** |  | [default to null]
**PrimaryIpsecDetail** | [***GatewayHandoffIpsecGatewayDetail**](gateway_handoff_ipsec_gateway_detail.md) |  | [optional] [default to null]
**Secondary** | **int32** |  | [optional] [default to null]
**SecondaryIpsecDetail** | [***GatewayHandoffIpsecGatewayDetail**](gateway_handoff_ipsec_gateway_detail.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


