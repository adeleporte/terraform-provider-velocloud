# EnterpriseAlertNotificationUserData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Email** | **string** |  | [default to null]
**EmailEnabled** | **int32** |  | [default to null]
**Enabled** | **int32** |  | [default to null]
**EnterpriseUserId** | **int32** |  | [default to null]
**MobileEnabled** | **int32** |  | [default to null]
**MobilePhone** | **string** |  | [default to null]
**SmsEnabled** | **int32** |  | [default to null]
**Username** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


