# EnterpriseAlertTrigger

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**TriggerTime** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**EnterpriseAlertConfigurationId** | **int32** |  | [optional] [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**EdgeId** | **int32** |  | [optional] [default to null]
**EdgeName** | **string** |  | [optional] [default to null]
**LinkId** | **int32** |  | [optional] [default to null]
**LinkName** | **string** |  | [optional] [default to null]
**EnterpriseObjectId** | **int32** |  | [optional] [default to null]
**EnterpriseObjectName** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**State** | **string** |  | [optional] [default to null]
**StateSetTime** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**LastContact** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**FirstNotificationSeconds** | **int32** |  | [optional] [default to null]
**MaxNotifications** | **int32** |  | [optional] [default to null]
**NotificationIntervalSeconds** | **int32** |  | [optional] [default to null]
**ResetIntervalSeconds** | **int32** |  | [optional] [default to null]
**Comment** | **string** |  | [optional] [default to null]
**NextNotificationTime** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**RemainingNotifications** | **int32** |  | [optional] [default to null]
**Timezone** | **string** |  | [optional] [default to null]
**Locale** | **string** |  | [optional] [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


