# \EnterpriseApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**EnterpriseDeleteEnterprise**](EnterpriseApi.md#EnterpriseDeleteEnterprise) | **Post** /enterprise/deleteEnterprise | Delete an enterprise
[**EnterpriseDeleteEnterpriseGatewayRecords**](EnterpriseApi.md#EnterpriseDeleteEnterpriseGatewayRecords) | **Post** /enterprise/deleteEnterpriseGatewayRecords | Delete enterprise gateway record(s)
[**EnterpriseDeleteEnterpriseNetworkAllocation**](EnterpriseApi.md#EnterpriseDeleteEnterpriseNetworkAllocation) | **Post** /enterprise/deleteEnterpriseNetworkAllocation | Delete an enterprise network allocation
[**EnterpriseDeleteEnterpriseService**](EnterpriseApi.md#EnterpriseDeleteEnterpriseService) | **Post** /enterprise/deleteEnterpriseService | Delete an enterprise service
[**EnterpriseGetEnterprise**](EnterpriseApi.md#EnterpriseGetEnterprise) | **Post** /enterprise/getEnterprise | Get enterprise
[**EnterpriseGetEnterpriseAddresses**](EnterpriseApi.md#EnterpriseGetEnterpriseAddresses) | **Post** /enterprise/getEnterpriseAddresses | Get enterprise IP address information
[**EnterpriseGetEnterpriseAlertConfigurations**](EnterpriseApi.md#EnterpriseGetEnterpriseAlertConfigurations) | **Post** /enterprise/getEnterpriseAlertConfigurations | Get the enterprise alert configuration
[**EnterpriseGetEnterpriseAlerts**](EnterpriseApi.md#EnterpriseGetEnterpriseAlerts) | **Post** /enterprise/getEnterpriseAlerts | Get triggered enterprise alerts
[**EnterpriseGetEnterpriseAllAlertRecipients**](EnterpriseApi.md#EnterpriseGetEnterpriseAllAlertRecipients) | **Post** /enterprise/getEnterpriseAllAlertsRecipients | List recipients currently receiving ALL enterprise alerts
[**EnterpriseGetEnterpriseCapabilities**](EnterpriseApi.md#EnterpriseGetEnterpriseCapabilities) | **Post** /enterprise/getEnterpriseCapabilities | Get enterprise capabilities
[**EnterpriseGetEnterpriseConfigurations**](EnterpriseApi.md#EnterpriseGetEnterpriseConfigurations) | **Post** /enterprise/getEnterpriseConfigurations | Get enterprise configuration profiles
[**EnterpriseGetEnterpriseEdges**](EnterpriseApi.md#EnterpriseGetEnterpriseEdges) | **Post** /enterprise/getEnterpriseEdges | Get edges associated with an enterprise
[**EnterpriseGetEnterpriseGatewayHandoff**](EnterpriseApi.md#EnterpriseGetEnterpriseGatewayHandoff) | **Post** /enterprise/getEnterpriseGatewayHandoff | Get enterprise gateway handoff configuration
[**EnterpriseGetEnterpriseNetworkAllocation**](EnterpriseApi.md#EnterpriseGetEnterpriseNetworkAllocation) | **Post** /enterprise/getEnterpriseNetworkAllocation | Get an enterprise network allocation
[**EnterpriseGetEnterpriseNetworkAllocations**](EnterpriseApi.md#EnterpriseGetEnterpriseNetworkAllocations) | **Post** /enterprise/getEnterpriseNetworkAllocations | Get all network allocation objects defined on an enterprise
[**EnterpriseGetEnterpriseNetworkSegments**](EnterpriseApi.md#EnterpriseGetEnterpriseNetworkSegments) | **Post** /enterprise/getEnterpriseNetworkSegments | Get all network segment objects defined on an enterprise
[**EnterpriseGetEnterpriseProperty**](EnterpriseApi.md#EnterpriseGetEnterpriseProperty) | **Post** /enterprise/getEnterpriseProperty | Get enterprise property
[**EnterpriseGetEnterpriseRouteConfiguration**](EnterpriseApi.md#EnterpriseGetEnterpriseRouteConfiguration) | **Post** /enterprise/getEnterpriseRouteConfiguration | Get route advertisement and routing preferences policy
[**EnterpriseGetEnterpriseRouteTable**](EnterpriseApi.md#EnterpriseGetEnterpriseRouteTable) | **Post** /enterprise/getEnterpriseRouteTable | Get the enterprise route table
[**EnterpriseGetEnterpriseServices**](EnterpriseApi.md#EnterpriseGetEnterpriseServices) | **Post** /enterprise/getEnterpriseServices | Get enterprise network service detail
[**EnterpriseInsertEnterprise**](EnterpriseApi.md#EnterpriseInsertEnterprise) | **Post** /enterprise/insertEnterprise | Create enterprise
[**EnterpriseInsertEnterpriseNetworkAllocation**](EnterpriseApi.md#EnterpriseInsertEnterpriseNetworkAllocation) | **Post** /enterprise/insertEnterpriseNetworkAllocation | Insert an enterprise network allocation
[**EnterpriseInsertEnterpriseNetworkSegment**](EnterpriseApi.md#EnterpriseInsertEnterpriseNetworkSegment) | **Post** /enterprise/insertEnterpriseNetworkSegment | Insert an enterprise network segment
[**EnterpriseInsertEnterpriseService**](EnterpriseApi.md#EnterpriseInsertEnterpriseService) | **Post** /enterprise/insertEnterpriseService | Insert a new enterprise service
[**EnterpriseInsertOrUpdateEnterpriseAlertConfigurations**](EnterpriseApi.md#EnterpriseInsertOrUpdateEnterpriseAlertConfigurations) | **Post** /enterprise/insertOrUpdateEnterpriseAlertConfigurations | Insert, update, or delete enterprise alert configurations
[**EnterpriseInsertOrUpdateEnterpriseCapability**](EnterpriseApi.md#EnterpriseInsertOrUpdateEnterpriseCapability) | **Post** /enterprise/insertOrUpdateEnterpriseCapability | Insert or update an enterprise capability
[**EnterpriseInsertOrUpdateEnterpriseGatewayHandoff**](EnterpriseApi.md#EnterpriseInsertOrUpdateEnterpriseGatewayHandoff) | **Post** /enterprise/insertOrUpdateEnterpriseGatewayHandoff | Insert or update an enterprise gateway handoff configuration
[**EnterpriseInsertOrUpdateEnterpriseProperty**](EnterpriseApi.md#EnterpriseInsertOrUpdateEnterpriseProperty) | **Post** /enterprise/insertOrUpdateEnterpriseProperty | Insert or update an enterprise property
[**EnterpriseSetEnterpriseAllAlertRecipients**](EnterpriseApi.md#EnterpriseSetEnterpriseAllAlertRecipients) | **Post** /enterprise/setEnterpriseAllAlertsRecipients | Set the recipients who should receive all alerts for an enterprise
[**EnterpriseUpdateEnterprise**](EnterpriseApi.md#EnterpriseUpdateEnterprise) | **Post** /enterprise/updateEnterprise | Update an enterprise
[**EnterpriseUpdateEnterpriseNetworkAllocation**](EnterpriseApi.md#EnterpriseUpdateEnterpriseNetworkAllocation) | **Post** /enterprise/updateEnterpriseNetworkAllocation | Update an enterprise network allocation
[**EnterpriseUpdateEnterpriseNetworkSegment**](EnterpriseApi.md#EnterpriseUpdateEnterpriseNetworkSegment) | **Post** /enterprise/updateEnterpriseNetworkSegment | Update an enterprise network segment
[**EnterpriseUpdateEnterpriseRoute**](EnterpriseApi.md#EnterpriseUpdateEnterpriseRoute) | **Post** /enterprise/updateEnterpriseRoute | Update an enterprise route
[**EnterpriseUpdateEnterpriseRouteConfiguration**](EnterpriseApi.md#EnterpriseUpdateEnterpriseRouteConfiguration) | **Post** /enterprise/updateEnterpriseRouteConfiguration | Update enterprise routing configuration
[**EnterpriseUpdateEnterpriseSecurityPolicy**](EnterpriseApi.md#EnterpriseUpdateEnterpriseSecurityPolicy) | **Post** /enterprise/updateEnterpriseSecurityPolicy | Update enterprise security policy
[**EnterpriseUpdateEnterpriseService**](EnterpriseApi.md#EnterpriseUpdateEnterpriseService) | **Post** /enterprise/updateEnterpriseService | Update an enterprise service


# **EnterpriseDeleteEnterprise**
> EnterpriseDeleteEnterpriseResult EnterpriseDeleteEnterprise(ctx, body)
Delete an enterprise

Delete the enterprise specified by the given id parameter. enterpriseId is also a valid alias for id.  Privileges required:  `DELETE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseDeleteEnterprise**](EnterpriseDeleteEnterprise.md)|  | 

### Return type

[**EnterpriseDeleteEnterpriseResult**](enterprise_delete_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseDeleteEnterpriseGatewayRecords**
> EnterpriseDeleteEnterpriseGatewayRecordsResult EnterpriseDeleteEnterpriseGatewayRecords(ctx, body)
Delete enterprise gateway record(s)

Delete the enterprise gateway record(s) matching the given gateway id(s) and neighbor IP addresses.  Privileges required:  `DELETE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseDeleteEnterpriseGatewayRecords**](EnterpriseDeleteEnterpriseGatewayRecords.md)|  | 

### Return type

[**EnterpriseDeleteEnterpriseGatewayRecordsResult**](enterprise_delete_enterprise_gateway_records_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseDeleteEnterpriseNetworkAllocation**
> EnterpriseDeleteEnterpriseNetworkAllocationResult EnterpriseDeleteEnterpriseNetworkAllocation(ctx, body)
Delete an enterprise network allocation

Delete an enterprise network allocation, by id.  Privileges required:  `DELETE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseDeleteEnterpriseNetworkAllocation**](EnterpriseDeleteEnterpriseNetworkAllocation.md)|  | 

### Return type

[**EnterpriseDeleteEnterpriseNetworkAllocationResult**](enterprise_delete_enterprise_network_allocation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseDeleteEnterpriseService**
> EnterpriseDeleteEnterpriseServiceResult EnterpriseDeleteEnterpriseService(ctx, body)
Delete an enterprise service

Delete an enterprise service, by id.  Privileges required:  `DELETE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseDeleteEnterpriseService**](EnterpriseDeleteEnterpriseService.md)|  | 

### Return type

[**EnterpriseDeleteEnterpriseServiceResult**](enterprise_delete_enterprise_service_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterprise**
> EnterpriseGetEnterpriseResult EnterpriseGetEnterprise(ctx, body)
Get enterprise

Retrieve enterprise data, with optional proxy (partner) detail.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterprise**](EnterpriseGetEnterprise.md)|  | 

### Return type

[**EnterpriseGetEnterpriseResult**](enterprise_get_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseAddresses**
> []EnterpriseGetEnterpriseAddressesResultItem EnterpriseGetEnterpriseAddresses(ctx, body)
Get enterprise IP address information

Retrieve the public IP address information for the management and control entities associated with this enterprise, including Orchestrator(s), Gateway(s), and datacenter(s).  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseAddresses**](EnterpriseGetEnterpriseAddresses.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseAddressesResultItem**](enterprise_get_enterprise_addresses_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseAlertConfigurations**
> []EnterpriseGetEnterpriseAlertConfigurationsResultItem EnterpriseGetEnterpriseAlertConfigurations(ctx, body)
Get the enterprise alert configuration

Get the alert configurations associated with an enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseAlertConfigurations**](EnterpriseGetEnterpriseAlertConfigurations.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseAlertConfigurationsResultItem**](enterprise_get_enterprise_alert_configurations_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseAlerts**
> EnterpriseGetEnterpriseAlertsResult EnterpriseGetEnterpriseAlerts(ctx, body)
Get triggered enterprise alerts

Gets past triggered alerts for the specified enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseAlerts**](EnterpriseGetEnterpriseAlerts.md)|  | 

### Return type

[**EnterpriseGetEnterpriseAlertsResult**](enterprise_get_enterprise_alerts_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseAllAlertRecipients**
> EnterpriseGetEnterpriseAllAlertRecipientsResult EnterpriseGetEnterpriseAllAlertRecipients(ctx, body)
List recipients currently receiving ALL enterprise alerts

List the recipients currently configured to receive all alerts for an enterprise.  Privileges required:  `READ` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseAllAlertRecipients**](EnterpriseGetEnterpriseAllAlertRecipients.md)|  | 

### Return type

[**EnterpriseGetEnterpriseAllAlertRecipientsResult**](enterprise_get_enterprise_all_alert_recipients_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseCapabilities**
> EnterpriseGetEnterpriseCapabilitiesResult EnterpriseGetEnterpriseCapabilities(ctx, body)
Get enterprise capabilities

Retrieve a list of the enterprise capabilities currently enabled/disabled on an enterprise (e.g. BGP, COS mapping, PKI, etc.)  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseCapabilities**](EnterpriseGetEnterpriseCapabilities.md)|  | 

### Return type

[**EnterpriseGetEnterpriseCapabilitiesResult**](enterprise_get_enterprise_capabilities_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseConfigurations**
> []EnterpriseGetEnterpriseConfigurationsResultItem EnterpriseGetEnterpriseConfigurations(ctx, body)
Get enterprise configuration profiles

Retrieve a list of configuration profiles existing on an enterprise, with optional edge and/or module detail.  Privileges required:  `READ` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseConfigurations**](EnterpriseGetEnterpriseConfigurations.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseConfigurationsResultItem**](enterprise_get_enterprise_configurations_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseEdges**
> []EnterpriseGetEnterpriseEdgesResultItem EnterpriseGetEnterpriseEdges(ctx, body)
Get edges associated with an enterprise

Gets all Edges associated with the specified enterprise, including optional site, link, and configuration details.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseEdges**](EnterpriseGetEnterpriseEdges.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseEdgesResultItem**](enterprise_get_enterprise_edges_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseGatewayHandoff**
> EnterpriseGetEnterpriseGatewayHandoffResult EnterpriseGetEnterpriseGatewayHandoff(ctx, body)
Get enterprise gateway handoff configuration

Get enterprise gateway handoff configuration.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseGatewayHandoff**](EnterpriseGetEnterpriseGatewayHandoff.md)|  | 

### Return type

[**EnterpriseGetEnterpriseGatewayHandoffResult**](enterprise_get_enterprise_gateway_handoff_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseNetworkAllocation**
> EnterpriseGetEnterpriseNetworkAllocationResult EnterpriseGetEnterpriseNetworkAllocation(ctx, body)
Get an enterprise network allocation

Retrieve a network allocation object by id.  Privileges required:  `READ` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseNetworkAllocation**](EnterpriseGetEnterpriseNetworkAllocation.md)|  | 

### Return type

[**EnterpriseGetEnterpriseNetworkAllocationResult**](enterprise_get_enterprise_network_allocation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseNetworkAllocations**
> []EnterpriseGetEnterpriseNetworkAllocationsResultItem EnterpriseGetEnterpriseNetworkAllocations(ctx, body)
Get all network allocation objects defined on an enterprise

Retrieve a list of all of the network allocations defined onthe given enterprise.  Privileges required:  `READ` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseNetworkAllocations**](EnterpriseGetEnterpriseNetworkAllocations.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseNetworkAllocationsResultItem**](enterprise_get_enterprise_network_allocations_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseNetworkSegments**
> []EnterpriseGetEnterpriseNetworkSegmentsResultItem EnterpriseGetEnterpriseNetworkSegments(ctx, body)
Get all network segment objects defined on an enterprise

Retrieve a list of all of the network segments defined forthe given enterprise.  Privileges required:  `READ` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseNetworkSegments**](EnterpriseGetEnterpriseNetworkSegments.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseNetworkSegmentsResultItem**](enterprise_get_enterprise_network_segments_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseProperty**
> EnterpriseGetEnterprisePropertyResult EnterpriseGetEnterpriseProperty(ctx, body)
Get enterprise property

Get a enterprise property by object id or other attribute.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseProperty**](EnterpriseGetEnterpriseProperty.md)|  | 

### Return type

[**EnterpriseGetEnterprisePropertyResult**](enterprise_get_enterprise_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseRouteConfiguration**
> EnterpriseGetEnterpriseRouteConfigurationResult EnterpriseGetEnterpriseRouteConfiguration(ctx, body)
Get route advertisement and routing preferences policy

Get enterprise route advertisement, routing peferences and OSPF, BGP advertisement policy as configured in the Overlay Flow Control table.  Privileges required:  `READ` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseRouteConfiguration**](EnterpriseGetEnterpriseRouteConfiguration.md)|  | 

### Return type

[**EnterpriseGetEnterpriseRouteConfigurationResult**](enterprise_get_enterprise_route_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseRouteTable**
> EnterpriseGetEnterpriseRouteTableResult EnterpriseGetEnterpriseRouteTable(ctx, body)
Get the enterprise route table

Get composite enterprise route table, optionally scoped by profile(s). The returned routes include static routes, directly connected routes and learned routes.  Privileges required:  `READ` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseRouteTable**](EnterpriseGetEnterpriseRouteTable.md)|  | 

### Return type

[**EnterpriseGetEnterpriseRouteTableResult**](enterprise_get_enterprise_route_table_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseGetEnterpriseServices**
> []EnterpriseGetEnterpriseServicesResultItem EnterpriseGetEnterpriseServices(ctx, body)
Get enterprise network service detail

Get the network service JSON objects defined for an enterprise.  Privileges required:  `READ` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseServices**](EnterpriseGetEnterpriseServices.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseServicesResultItem**](enterprise_get_enterprise_services_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterprise**
> EnterpriseInsertEnterpriseResult EnterpriseInsertEnterprise(ctx, body)
Create enterprise

Creates a new enterprise, which is owned by the operator.  Privileges required:  `CREATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterprise**](EnterpriseInsertEnterprise.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseResult**](enterprise_insert_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterpriseNetworkAllocation**
> EnterpriseInsertEnterpriseNetworkAllocationResult EnterpriseInsertEnterpriseNetworkAllocation(ctx, body)
Insert an enterprise network allocation

Insert a new enterprise network allocation.  Privileges required:  `CREATE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterpriseNetworkAllocation**](EnterpriseInsertEnterpriseNetworkAllocation.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseNetworkAllocationResult**](enterprise_insert_enterprise_network_allocation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterpriseNetworkSegment**
> EnterpriseInsertEnterpriseNetworkSegmentResult EnterpriseInsertEnterpriseNetworkSegment(ctx, body)
Insert an enterprise network segment

Insert a new enterprise network segment.  Privileges required:  `CREATE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterpriseNetworkSegment**](EnterpriseInsertEnterpriseNetworkSegment.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseNetworkSegmentResult**](enterprise_insert_enterprise_network_segment_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterpriseService**
> EnterpriseInsertEnterpriseServiceResult EnterpriseInsertEnterpriseService(ctx, body)
Insert a new enterprise service

Insert a new enterprise service.  Privileges required:  `CREATE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterpriseService**](EnterpriseInsertEnterpriseService.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseServiceResult**](enterprise_insert_enterprise_service_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertOrUpdateEnterpriseAlertConfigurations**
> EnterpriseInsertOrUpdateEnterpriseAlertConfigurationsResult EnterpriseInsertOrUpdateEnterpriseAlertConfigurations(ctx, body)
Insert, update, or delete enterprise alert configurations

Insert, update, or delete enterprise alert configurations. Returns the array of alert configurations submitted, with ids added for the entries that have been successfully inserted. If an entry is not successfully inserted or updated, an `error` property is included in the .  Privileges required:  `CREATE` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertOrUpdateEnterpriseAlertConfigurations**](EnterpriseInsertOrUpdateEnterpriseAlertConfigurations.md)|  | 

### Return type

[**EnterpriseInsertOrUpdateEnterpriseAlertConfigurationsResult**](enterprise_insert_or_update_enterprise_alert_configurations_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertOrUpdateEnterpriseCapability**
> EnterpriseInsertOrUpdateEnterpriseCapabilityResult EnterpriseInsertOrUpdateEnterpriseCapability(ctx, body)
Insert or update an enterprise capability

Insert or update an enterprise capability.  Privileges required:  `UPDATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertOrUpdateEnterpriseCapability**](EnterpriseInsertOrUpdateEnterpriseCapability.md)|  | 

### Return type

[**EnterpriseInsertOrUpdateEnterpriseCapabilityResult**](enterprise_insert_or_update_enterprise_capability_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertOrUpdateEnterpriseGatewayHandoff**
> EnterpriseInsertOrUpdateEnterpriseGatewayHandoffResult EnterpriseInsertOrUpdateEnterpriseGatewayHandoff(ctx, body)
Insert or update an enterprise gateway handoff configuration

Insert or update an enterprise gateway handoff configuration.  Privileges required:  `UPDATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertOrUpdateEnterpriseGatewayHandoff**](EnterpriseInsertOrUpdateEnterpriseGatewayHandoff.md)|  | 

### Return type

[**EnterpriseInsertOrUpdateEnterpriseGatewayHandoffResult**](enterprise_insert_or_update_enterprise_gateway_handoff_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertOrUpdateEnterpriseProperty**
> EnterpriseInsertOrUpdateEnterprisePropertyResult EnterpriseInsertOrUpdateEnterpriseProperty(ctx, body)
Insert or update an enterprise property

Insert a enterprise property. If property with the given name already exists, the property will be updated.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertOrUpdateEnterpriseProperty**](EnterpriseInsertOrUpdateEnterpriseProperty.md)|  | 

### Return type

[**EnterpriseInsertOrUpdateEnterprisePropertyResult**](enterprise_insert_or_update_enterprise_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseSetEnterpriseAllAlertRecipients**
> EnterpriseSetEnterpriseAllAlertRecipientsResult EnterpriseSetEnterpriseAllAlertRecipients(ctx, body)
Set the recipients who should receive all alerts for an enterprise

Set the recipients who should receive all alerts for an enterprise.  Privileges required:  `UPDATE` `ENTERPRISE_ALERT`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseSetEnterpriseAllAlertRecipients**](EnterpriseSetEnterpriseAllAlertRecipients.md)|  | 

### Return type

[**EnterpriseSetEnterpriseAllAlertRecipientsResult**](enterprise_set_enterprise_all_alert_recipients_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterprise**
> EnterpriseUpdateEnterpriseResult EnterpriseUpdateEnterprise(ctx, body)
Update an enterprise

Update an enterprise provided an object id or name, and an _update object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterprise**](EnterpriseUpdateEnterprise.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseResult**](enterprise_update_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseNetworkAllocation**
> EnterpriseUpdateEnterpriseNetworkAllocationResult EnterpriseUpdateEnterpriseNetworkAllocation(ctx, body)
Update an enterprise network allocation

Update an enterprise network allocation, provided an object id and an _update object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseNetworkAllocation**](EnterpriseUpdateEnterpriseNetworkAllocation.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseNetworkAllocationResult**](enterprise_update_enterprise_network_allocation_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseNetworkSegment**
> EnterpriseUpdateEnterpriseNetworkSegmentResult EnterpriseUpdateEnterpriseNetworkSegment(ctx, body)
Update an enterprise network segment

Update an enterprise network segment.  Privileges required:  `UPDATE` `NETWORK_ALLOCATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseInsertEnterpriseNetworkSegment1**](EnterpriseInsertEnterpriseNetworkSegment1.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseNetworkSegmentResult**](enterprise_update_enterprise_network_segment_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseRoute**
> EnterpriseUpdateEnterpriseRouteResult EnterpriseUpdateEnterpriseRoute(ctx, body)
Update an enterprise route

Update an enterprise route, set advertisement and cost values. Required parameters include the original route, as returned by enterprise/getEnterpriseRouteTable and the updated route with modified advertisement and route preference ordering.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseRoute**](EnterpriseUpdateEnterpriseRoute.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseRouteResult**](enterprise_update_enterprise_route_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseRouteConfiguration**
> EnterpriseUpdateEnterpriseRouteConfigurationResult EnterpriseUpdateEnterpriseRouteConfiguration(ctx, body)
Update enterprise routing configuration

Update enterprise routing configuration, by configuration id or logicalId.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseRouteConfiguration**](EnterpriseUpdateEnterpriseRouteConfiguration.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseRouteConfigurationResult**](enterprise_update_enterprise_route_configuration_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseSecurityPolicy**
> EnterpriseUpdateEnterpriseSecurityPolicyResult EnterpriseUpdateEnterpriseSecurityPolicy(ctx, body)
Update enterprise security policy

Update enterprise security policy in accordance with to the passed ipsec settings.  Privileges required:  `UPDATE` `ENTERPRISE_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseSecurityPolicy**](EnterpriseUpdateEnterpriseSecurityPolicy.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseSecurityPolicyResult**](enterprise_update_enterprise_security_policy_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUpdateEnterpriseService**
> EnterpriseUpdateEnterpriseServiceResult EnterpriseUpdateEnterpriseService(ctx, body)
Update an enterprise service

Update the enterprise service with the given id according to the settings specified by the _update field.  Privileges required:  `UPDATE` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUpdateEnterpriseService**](EnterpriseUpdateEnterpriseService.md)|  | 

### Return type

[**EnterpriseUpdateEnterpriseServiceResult**](enterprise_update_enterprise_service_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

