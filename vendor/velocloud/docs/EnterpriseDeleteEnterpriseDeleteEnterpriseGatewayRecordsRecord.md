# EnterpriseDeleteEnterpriseDeleteEnterpriseGatewayRecordsRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GatewayId** | **int32** |  | [optional] [default to null]
**NeighborIp** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


