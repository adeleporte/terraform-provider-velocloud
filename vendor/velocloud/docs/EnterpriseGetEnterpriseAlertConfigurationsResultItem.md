# EnterpriseGetEnterpriseAlertConfigurationsResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**AlertDefinitionId** | **int32** |  | [optional] [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Enabled** | **bool** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Definition** | [***EnterpriseAlertDefinition**](enterprise_alert_definition.md) |  | [optional] [default to null]
**FirstNotificationSeconds** | **int32** |  | [optional] [default to null]
**MaxNotifications** | **int32** |  | [optional] [default to null]
**NotificationIntervalSeconds** | **int32** |  | [optional] [default to null]
**ResetIntervalSeconds** | **int32** |  | [optional] [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


