# EnterpriseGetEnterpriseAlerts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [default to null]
**Interval** | [***Interval**](interval.md) |  | [default to null]
**Filter** | [***EnterprisegetEnterpriseAlertsFilter**](enterprisegetEnterpriseAlerts_filter.md) |  | [optional] [default to null]
**With** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


