# EnterpriseGetEnterpriseCapabilitiesResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnableBGP** | **bool** |  | [optional] [default to null]
**EnableCosMapping** | **bool** |  | [optional] [default to null]
**EnableFwLogs** | **bool** |  | [optional] [default to null]
**EnableOSPF** | **bool** |  | [optional] [default to null]
**EnablePKI** | **bool** |  | [optional] [default to null]
**EnablePremium** | **bool** |  | [optional] [default to null]
**EnableServiceRateLimiting** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


