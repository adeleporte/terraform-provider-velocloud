# EnterpriseGetEnterpriseEdgesResultItemConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Operator** | [***EnterpriseGetEnterpriseEdgesResultItemConfigurationOperator**](enterprise_get_enterprise_edges_result_item_configuration_operator.md) |  | [optional] [default to null]
**Enterprise** | [***EnterpriseGetEnterpriseEdgesResultItemConfigurationEnterprise**](enterprise_get_enterprise_edges_result_item_configuration_enterprise.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


