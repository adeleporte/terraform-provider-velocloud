# EnterpriseGetEnterpriseEdgesResultItemConfigurationEnterpriseModules

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EdgeSpecificData** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**IsEdgeSpecific** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Version** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


