# EnterpriseGetEnterpriseEdgesResultItemConfigurationOperator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Modules** | [**[]EnterpriseGetEnterpriseEdgesResultItemConfigurationOperatorModules**](enterprise_get_enterprise_edges_result_item_configuration_operator_modules.md) |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


