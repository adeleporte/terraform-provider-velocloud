# EnterpriseGetEnterpriseRouteTable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Profiles** | [**[]EnterprisegetEnterpriseRouteTableProfiles**](enterprisegetEnterpriseRouteTable_profiles.md) |  | [optional] [default to null]
**Subnets** | [**[]EnterprisegetEnterpriseRouteTableSubnets**](enterprisegetEnterpriseRouteTable_subnets.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


