# EnterpriseInsertEnterpriseNetworkSegment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [default to null]
**Description** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [default to null]
**Data** | [***EnterpriseinsertEnterpriseNetworkSegmentData**](enterpriseinsertEnterpriseNetworkSegment_data.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


