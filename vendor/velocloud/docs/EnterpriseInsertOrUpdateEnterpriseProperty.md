# EnterpriseInsertOrUpdateEnterpriseProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [default to null]
**Name** | **string** |  | [default to null]
**Value** | **string** |  | [default to null]
**DataType** | **string** |  | [optional] [default to null]
**IsPassword** | **bool** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


