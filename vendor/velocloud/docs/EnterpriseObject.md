# EnterpriseObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**City** | **string** |  | [optional] [default to null]
**ContactEmail** | **string** |  | [optional] [default to null]
**ContactMobile** | **string** |  | [optional] [default to null]
**ContactName** | **string** |  | [optional] [default to null]
**ContactPhone** | **string** |  | [optional] [default to null]
**Country** | **string** |  | [optional] [default to null]
**Lat** | **float32** |  | [optional] [default to null]
**Lon** | **float32** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**PostalCode** | **string** |  | [optional] [default to null]
**State** | **string** |  | [optional] [default to null]
**StreetAddress** | **string** |  | [optional] [default to null]
**StreetAddress2** | **string** |  | [optional] [default to null]
**GatewayPoolId** | **int32** |  | [optional] [default to null]
**NetworkId** | **int32** |  | [optional] [default to null]
**ReturnData** | **bool** |  | [optional] [default to null]
**User** | [***AuthObject**](auth_object.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


