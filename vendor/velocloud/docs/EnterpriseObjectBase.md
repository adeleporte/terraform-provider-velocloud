# EnterpriseObjectBase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [default to null]
**OperatorId** | **int32** |  | [default to null]
**NetworkId** | **int32** |  | [default to null]
**EnterpriseId** | **int32** |  | [default to null]
**EdgeId** | **int32** |  | [default to null]
**GatewayId** | **int32** |  | [default to null]
**ParentGroupId** | **int32** |  | [default to null]
**Description** | **string** |  | [default to null]
**Object** | **string** |  | [default to null]
**Name** | **string** |  | [default to null]
**Type_** | **string** |  | [default to null]
**LogicalId** | **string** |  | [default to null]
**AlertsEnabled** | **bool** |  | [default to null]
**OperatorAlertsEnabled** | **bool** |  | [default to null]
**Status** | **string** |  | [default to null]
**StatusModified** | [**time.Time**](time.Time.md) |  | [default to null]
**PreviousData** | **string** |  | [default to null]
**PreviousCreated** | [**time.Time**](time.Time.md) |  | [default to null]
**DraftData** | **string** |  | [default to null]
**DraftCreated** | [**time.Time**](time.Time.md) |  | [default to null]
**DraftComment** | **string** |  | [default to null]
**Data** | [***interface{}**](interface{}.md) |  | [default to null]
**LastContact** | [**time.Time**](time.Time.md) |  | [default to null]
**Version** | **string** |  | [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


