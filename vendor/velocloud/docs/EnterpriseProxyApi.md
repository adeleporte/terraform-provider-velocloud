# \EnterpriseProxyApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**EnterpriseProxyGetEnterpriseProxyEdgeInventory**](EnterpriseProxyApi.md#EnterpriseProxyGetEnterpriseProxyEdgeInventory) | **Post** /enterpriseProxy/getEnterpriseProxyEdgeInventory | Get a list of all partner enterprises and edge inventory associated with each enterprise
[**EnterpriseProxyGetEnterpriseProxyEnterprises**](EnterpriseProxyApi.md#EnterpriseProxyGetEnterpriseProxyEnterprises) | **Post** /enterpriseProxy/getEnterpriseProxyEnterprises | Get a list of all partner enterprises
[**EnterpriseProxyGetEnterpriseProxyGatewayPools**](EnterpriseProxyApi.md#EnterpriseProxyGetEnterpriseProxyGatewayPools) | **Post** /enterpriseProxy/getEnterpriseProxyGatewayPools | Get list of gateway pools
[**EnterpriseProxyGetEnterpriseProxyOperatorProfiles**](EnterpriseProxyApi.md#EnterpriseProxyGetEnterpriseProxyOperatorProfiles) | **Post** /enterpriseProxy/getEnterpriseProxyOperatorProfiles | Get the operator profiles associated with a partner
[**EnterpriseProxyInsertEnterpriseProxyEnterprise**](EnterpriseProxyApi.md#EnterpriseProxyInsertEnterpriseProxyEnterprise) | **Post** /enterpriseProxy/insertEnterpriseProxyEnterprise | Insert a new enterprise owned by an MSP


# **EnterpriseProxyGetEnterpriseProxyEdgeInventory**
> []EnterpriseProxyGetEnterpriseProxyEdgeInventoryResultItem EnterpriseProxyGetEnterpriseProxyEdgeInventory(ctx, body)
Get a list of all partner enterprises and edge inventory associated with each enterprise

Get  partner enterprises and their edge inventory.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyEdgeInventory**](EnterpriseProxyGetEnterpriseProxyEdgeInventory.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyEdgeInventoryResultItem**](enterprise_proxy_get_enterprise_proxy_edge_inventory_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyEnterprises**
> []EnterpriseProxyGetEnterpriseProxyEnterprisesResultItem EnterpriseProxyGetEnterpriseProxyEnterprises(ctx, body)
Get a list of all partner enterprises

Get all partner enterprises, optionally including all edges or edge counts.  Privileges required:  `READ` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyEnterprises**](EnterpriseProxyGetEnterpriseProxyEnterprises.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyEnterprisesResultItem**](enterprise_proxy_get_enterprise_proxy_enterprises_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyGatewayPools**
> []EnterpriseProxyGetEnterpriseProxyGatewayPoolsResultItem EnterpriseProxyGetEnterpriseProxyGatewayPools(ctx, body)
Get list of gateway pools

Get list of gateway pools associated with an enterprise proxy, optionally with lists of gateways or enterprises belonging to each pool.  Privileges required:  `READ` `GATEWAY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyGatewayPools**](EnterpriseProxyGetEnterpriseProxyGatewayPools.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyGatewayPoolsResultItem**](enterprise_proxy_get_enterprise_proxy_gateway_pools_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyOperatorProfiles**
> []EnterpriseProxyGetEnterpriseProxyOperatorProfilesResultItem EnterpriseProxyGetEnterpriseProxyOperatorProfiles(ctx, body)
Get the operator profiles associated with a partner

Get the operator profiles associated with a proxy (MSP), as assigned by the operator.  Privileges required:  `READ` `OPERATOR_PROFILE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyOperatorProfiles**](EnterpriseProxyGetEnterpriseProxyOperatorProfiles.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyOperatorProfilesResultItem**](enterprise_proxy_get_enterprise_proxy_operator_profiles_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyInsertEnterpriseProxyEnterprise**
> EnterpriseProxyInsertEnterpriseProxyEnterpriseResult EnterpriseProxyInsertEnterpriseProxyEnterprise(ctx, body)
Insert a new enterprise owned by an MSP

Insert an enterprise owned by an MSP. Whereas the `insertEnterprise` method will create an enterprise in the global or network context with no MSP association, this method will create one owned by an MSP, as determined by the credentials of the caller.  Privileges required:  `CREATE` `ENTERPRISE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyInsertEnterpriseProxyEnterprise**](EnterpriseProxyInsertEnterpriseProxyEnterprise.md)|  | 

### Return type

[**EnterpriseProxyInsertEnterpriseProxyEnterpriseResult**](enterprise_proxy_insert_enterprise_proxy_enterprise_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

