# EnterpriseProxyGetEnterpriseProxyEdgeInventoryResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseName** | **string** |  | [default to null]
**EnterpriseId** | **int32** |  | [default to null]
**EdgeName** | **string** |  | [default to null]
**EdgeId** | **int32** |  | [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [default to null]
**EdgeState** | **string** |  | [default to null]
**SerialNumber** | **string** |  | [default to null]
**HaSerialNumber** | **string** |  | [default to null]
**ActivationState** | **string** |  | [default to null]
**ActivationTime** | [**time.Time**](time.Time.md) |  | [default to null]
**ModelNumber** | **string** |  | [default to null]
**SoftwareVersion** | **string** |  | [default to null]
**SoftwareUpdated** | [**time.Time**](time.Time.md) |  | [default to null]
**LastContact** | [**time.Time**](time.Time.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


