# EnterpriseProxyGetEnterpriseProxyOperatorProfilesResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**EdgeCount** | **int32** |  | [optional] [default to null]
**Effective** | **string** |  | [optional] [default to null]
**Id** | **int32** |  | [optional] [default to null]
**Modified** | **string** |  | [optional] [default to null]
**Modules** | [**[]ConfigurationModule**](configuration_module.md) |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Version** | **string** |  | [optional] [default to null]
**Edges** | [**[]EdgeObject**](edge_object.md) |  | [optional] [default to null]
**Enterprises** | [**[]ConfigurationEnterprise**](configuration_enterprise.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


