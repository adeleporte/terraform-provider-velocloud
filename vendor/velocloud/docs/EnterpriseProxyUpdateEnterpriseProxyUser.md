# EnterpriseProxyUpdateEnterpriseProxyUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Update** | [***EnterpriseUserWithRoleInfo**](enterprise_user_with_role_info.md) |  | [default to null]
**Id** | **int32** |  | [optional] [default to null]
**EnterpriseProxyId** | **int32** |  | [optional] [default to null]
**Username** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


