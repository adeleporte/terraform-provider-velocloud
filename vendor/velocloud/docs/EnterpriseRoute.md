# EnterpriseRoute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type_** | **string** |  | [optional] [default to null]
**ExitType** | **string** |  | [optional] [default to null]
**EdgeId** | **int32** |  | [optional] [default to null]
**EdgeName** | **string** |  | [optional] [default to null]
**ProfileId** | **int32** |  | [optional] [default to null]
**CidrIp** | **string** |  | [optional] [default to null]
**Cost** | **int32** |  | [optional] [default to null]
**Advertise** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


