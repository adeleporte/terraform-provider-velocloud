# EnterpriseService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**EnterpriseObjectId** | **int32** |  | [optional] [default to null]
**ConfigurationId** | **int32** |  | [optional] [default to null]
**ModuleId** | **int32** |  | [optional] [default to null]
**Ref** | **string** |  | [optional] [default to null]
**Data** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**Version** | **string** |  | [optional] [default to null]
**Object** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [default to null]
**LogicalId** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


