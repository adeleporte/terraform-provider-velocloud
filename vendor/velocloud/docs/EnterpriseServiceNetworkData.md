# EnterpriseServiceNetworkData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Zone** | **string** |  | [optional] [default to null]
**Spaces** | [**[]EnterpriseServiceNetworkDataSpaces**](enterprise_service_network_data_spaces.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


