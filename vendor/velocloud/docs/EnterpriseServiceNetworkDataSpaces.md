# EnterpriseServiceNetworkDataSpaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CidrIp** | **string** |  | [optional] [default to null]
**CidrPrefix** | **int32** |  | [optional] [default to null]
**MaxVlans** | **int32** |  | [optional] [default to null]
**Mode** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**BranchCidrPrefix** | **int32** |  | [optional] [default to null]
**Guest** | **bool** |  | [optional] [default to null]
**Vlans** | [**[]EnterpriseServiceNetworkDataVlans**](enterprise_service_network_data_vlans.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


