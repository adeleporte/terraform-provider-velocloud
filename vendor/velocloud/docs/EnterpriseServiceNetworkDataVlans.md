# EnterpriseServiceNetworkDataVlans

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] [default to null]
**VlanId** | **int32** |  | [optional] [default to null]
**Advertise** | **bool** |  | [optional] [default to null]
**Cost** | **int32** |  | [optional] [default to null]
**StaticReserved** | **int32** |  | [optional] [default to null]
**Dhcp** | [***EnterpriseServiceNetworkDataDhcp**](enterprise_service_network_data_dhcp.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


