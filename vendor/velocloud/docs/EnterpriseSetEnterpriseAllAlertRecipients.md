# EnterpriseSetEnterpriseAllAlertRecipients

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**EnterpriseUsers** | [**[]EnterprisesetEnterpriseAllAlertsRecipientsEnterpriseUsers**](enterprisesetEnterpriseAllAlertsRecipients_enterpriseUsers.md) |  | [optional] [default to null]
**SmsEnabled** | **bool** |  | [optional] [default to null]
**SmsList** | [**[][]interface{}**](array.md) |  | [optional] [default to null]
**EmailEnabled** | **bool** |  | [optional] [default to null]
**EmailList** | **[]string** |  | [optional] [default to null]
**MobileEnabled** | **bool** |  | [optional] [default to null]
**MobileList** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


