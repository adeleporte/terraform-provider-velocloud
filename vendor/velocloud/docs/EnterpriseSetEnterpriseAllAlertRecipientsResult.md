# EnterpriseSetEnterpriseAllAlertRecipientsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EmailEnabled** | **bool** |  | [default to null]
**EmailList** | **[]string** |  | [default to null]
**EnterpriseUsers** | [**[]EnterpriseAlertNotificationUserData**](enterprise_alert_notification_user_data.md) |  | [default to null]
**MobileEnabled** | **bool** |  | [default to null]
**MobileList** | **[]string** |  | [default to null]
**SmsEnabled** | **bool** |  | [default to null]
**SmsList** | **[]string** |  | [default to null]
**SnmpEnabled** | **bool** |  | [optional] [default to null]
**SnmpList** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


