# EnterpriseUpdateEnterpriseRoute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Original** | [***EnterpriseRouteCollection**](enterprise_route_collection.md) |  | [optional] [default to null]
**Updated** | [***EnterpriseRouteCollection**](enterprise_route_collection.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


