# EnterpriseUpdateEnterpriseRouteConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Id** | **int32** |  | [optional] [default to null]
**Data** | [***EnterpriseupdateEnterpriseRouteConfigurationData**](enterpriseupdateEnterpriseRouteConfiguration_data.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


