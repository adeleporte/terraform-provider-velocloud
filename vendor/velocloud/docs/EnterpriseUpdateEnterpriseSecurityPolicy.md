# EnterpriseUpdateEnterpriseSecurityPolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Ipsec** | [***EnterpriseupdateEnterpriseSecurityPolicyIpsec**](enterpriseupdateEnterpriseSecurityPolicy_ipsec.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


