# EnterpriseUpdateEnterpriseService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Id** | **int32** |  | [default to null]
**Update** | [***EnterpriseupdateEnterpriseServiceUpdate**](enterpriseupdateEnterpriseService__update.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


