# EnterpriseUserWithRoleInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**UserType** | **string** |  | [optional] [default to null]
**Username** | **string** |  | [optional] [default to null]
**Domain** | **string** |  | [optional] [default to null]
**Password** | **string** |  | [optional] [default to null]
**FirstName** | **string** |  | [optional] [default to null]
**LastName** | **string** |  | [optional] [default to null]
**OfficePhone** | **string** |  | [optional] [default to null]
**MobilePhone** | **string** |  | [optional] [default to null]
**IsNative** | **bool** |  | [optional] [default to null]
**IsActive** | **bool** |  | [optional] [default to null]
**IsLocked** | **bool** |  | [optional] [default to null]
**Email** | **string** |  | [optional] [default to null]
**LastLogin** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**RoleId** | **int32** |  | [optional] [default to null]
**RoleName** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


