# EnterprisegetEnterpriseRouteTableSubnets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Subnet** | **string** |  | [optional] [default to null]
**PreferredExits** | [**[]EnterpriseRoute**](enterprise_route.md) |  | [optional] [default to null]
**EligableExits** | [**[]EnterpriseRoute**](enterprise_route.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


