# EnterpriseinsertEnterpriseNetworkAllocationData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Spaces** | [**[]EnterpriseNetworkSpace**](enterprise_network_space.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


