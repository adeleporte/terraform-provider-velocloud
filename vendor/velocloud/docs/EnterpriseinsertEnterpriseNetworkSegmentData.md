# EnterpriseinsertEnterpriseNetworkSegmentData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**DelegateToEnterprise** | **bool** |  | [optional] [default to null]
**DelegateToEnterpriseProxy** | **bool** |  | [optional] [default to null]
**ServiceVlan** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


