# EnterpriseupdateEnterpriseRouteConfigurationData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Edge** | [***EnterpriseupdateEnterpriseRouteConfigurationDataEdge**](enterpriseupdateEnterpriseRouteConfiguration_data_edge.md) |  | [default to null]
**Hub** | [***EnterpriseupdateEnterpriseRouteConfigurationDataEdge**](enterpriseupdateEnterpriseRouteConfiguration_data_edge.md) |  | [default to null]
**PartnerGateway** | [***EnterpriseupdateEnterpriseRouteConfigurationDataPartnerGateway**](enterpriseupdateEnterpriseRouteConfiguration_data_partnerGateway.md) |  | [default to null]
**RoutingPreference** | [**[]EnterpriseupdateEnterpriseRouteConfigurationDataRoutingPreference**](enterpriseupdateEnterpriseRouteConfiguration_data_routingPreference.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


