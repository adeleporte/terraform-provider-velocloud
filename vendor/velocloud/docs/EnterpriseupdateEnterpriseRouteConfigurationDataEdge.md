# EnterpriseupdateEnterpriseRouteConfigurationDataEdge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Ospf** | [***EnterpriseupdateEnterpriseRouteConfigurationDataEdgeOspf**](enterpriseupdateEnterpriseRouteConfiguration_data_edge_ospf.md) |  | [default to null]
**Bgp** | [***EnterpriseupdateEnterpriseRouteConfigurationDataEdgeBgp**](enterpriseupdateEnterpriseRouteConfiguration_data_edge_bgp.md) |  | [default to null]
**Assigned** | [***EnterpriseupdateEnterpriseRouteConfigurationDataEdgeAssigned**](enterpriseupdateEnterpriseRouteConfiguration_data_edge_assigned.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


