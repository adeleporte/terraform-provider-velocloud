# EnterpriseupdateEnterpriseRouteConfigurationDataEdgeAssigned

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AdvertiseStatic** | **bool** |  | [default to null]
**AdvertiseConnected** | **bool** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


