# EnterpriseupdateEnterpriseRouteConfigurationDataEdgeBgp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AdvertiseInternal** | **bool** |  | [default to null]
**AdvertiseExternal** | **bool** |  | [default to null]
**AdvertiseNeighborTags** | [***EnterpriseupdateEnterpriseRouteConfigurationDataEdgeBgpAdvertiseNeighborTags**](enterpriseupdateEnterpriseRouteConfiguration_data_edge_bgp_advertiseNeighborTags.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


