# EnterpriseupdateEnterpriseRouteConfigurationDataEdgeOspf

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AdvertiseExternal** | **bool** |  | [default to null]
**AdvertiseInterArea** | **bool** |  | [default to null]
**AdvertiseIntraArea** | **bool** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


