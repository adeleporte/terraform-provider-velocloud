# EnterpriseupdateEnterpriseRouteConfigurationDataPartnerGateway

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Bgp** | [***EnterpriseupdateEnterpriseRouteConfigurationDataPartnerGatewayBgp**](enterpriseupdateEnterpriseRouteConfiguration_data_partnerGateway_bgp.md) |  | [default to null]
**Assigned** | [***EnterpriseupdateEnterpriseRouteConfigurationDataPartnerGatewayAssigned**](enterpriseupdateEnterpriseRouteConfiguration_data_partnerGateway_assigned.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


