# EnterpriseupdateEnterpriseSecurityPolicyIpsec

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hash** | **string** |  | [optional] [default to null]
**Encryption** | **string** |  | [optional] [default to null]
**DiffieHellmanGroup** | **string** |  | [optional] [default to null]
**PerfectForwardSecrecy** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


