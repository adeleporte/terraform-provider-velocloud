# EnterpriseupdateEnterpriseServiceUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [***interface{}**](interface{}.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


