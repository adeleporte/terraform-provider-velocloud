# EventGetEnterpriseEvents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Interval** | [***Interval**](interval.md) |  | [optional] [default to null]
**Filter** | [***EnterprisegetEnterpriseAlertsFilter**](enterprisegetEnterpriseAlerts_filter.md) |  | [optional] [default to null]
**EdgeId** | **[]int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


