# EventGetEnterpriseEventsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**[]EnterpriseEvent**](enterprise_event.md) |  | [default to null]
**MetaData** | [***ListMetadata**](list_metadata.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


