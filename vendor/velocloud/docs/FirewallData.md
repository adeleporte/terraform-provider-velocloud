# FirewallData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FirewallEnabled** | **bool** |  | [default to null]
**FirewallLoggingEnabled** | **bool** |  | [default to null]
**Inbound** | [**[]FirewallInboundRule**](firewall_inbound_rule.md) |  | [default to null]
**Outbound** | [**[]FirewallOutboundRule**](firewall_outbound_rule.md) |  | [default to null]
**Services** | [***FirewallDataServices**](firewall_data_services.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


