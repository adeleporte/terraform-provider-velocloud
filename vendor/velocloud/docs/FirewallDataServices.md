# FirewallDataServices

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LoggingEnabled** | **bool** |  | [default to null]
**Ssh** | [***FirewallDataServicesSsh**](firewall_data_services_ssh.md) |  | [optional] [default to null]
**LocalUi** | [***FirewallDataServicesLocalUi**](firewall_data_services_localUi.md) |  | [optional] [default to null]
**Snmp** | [***FirewallDataServicesSnmp**](firewall_data_services_snmp.md) |  | [optional] [default to null]
**Icmp** | [***FirewallDataServicesIcmp**](firewall_data_services_icmp.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


