# FirewallDataServicesIcmp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [default to null]
**AllowSelectedIp** | **[]string** | List of IP addresses allowed ICMP access | [optional] [default to null]
**RuleLogicalId** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


