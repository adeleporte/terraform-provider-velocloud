# FirewallGetEnterpriseFirewallLogs

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Interval** | [***Interval**](interval.md) |  | [optional] [default to null]
**Filter** | [***EnterprisegetEnterpriseAlertsFilter**](enterprisegetEnterpriseAlerts_filter.md) |  | [optional] [default to null]
**Rules** | **[]string** |  | [optional] [default to null]
**SourceIps** | **[]string** |  | [optional] [default to null]
**DestIps** | **[]string** |  | [optional] [default to null]
**Edges** | **[]int32** |  | [optional] [default to null]
**With** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


