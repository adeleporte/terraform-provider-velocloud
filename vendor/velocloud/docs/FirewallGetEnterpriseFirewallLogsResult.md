# FirewallGetEnterpriseFirewallLogsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MetaData** | [***ListMetadata**](list_metadata.md) |  | [optional] [default to null]
**Data** | [**[]FirewallLogData**](firewall_log_data.md) |  | [optional] [default to null]
**Rules** | [**map[string]FirewallRule**](firewall_rule.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


