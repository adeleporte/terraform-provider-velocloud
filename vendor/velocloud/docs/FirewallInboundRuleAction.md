# FirewallInboundRuleAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type_** | **string** |  | [default to null]
**Nat** | [***FirewallInboundRuleActionNat**](firewall_inbound_rule_action_nat.md) |  | [default to null]
**Interface_** | **string** | The name of the interface from which traffic should be forwarded | [default to null]
**SubinterfaceId** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


