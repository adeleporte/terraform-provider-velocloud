# FirewallInboundRuleActionNat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LanIp** | **string** |  | [default to null]
**LanPort** | **int32** |  | [optional] [default to null]
**Outbound** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


