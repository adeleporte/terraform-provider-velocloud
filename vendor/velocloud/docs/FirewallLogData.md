# FirewallLogData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**EdgeId** | **int32** |  | [optional] [default to null]
**Timestamp** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Allow** | **int32** |  | [optional] [default to null]
**RuleLogicalId** | **string** |  | [optional] [default to null]
**Interface_** | **string** |  | [optional] [default to null]
**Protocol** | **string** |  | [optional] [default to null]
**SourceIp** | **string** |  | [optional] [default to null]
**SourcePort** | **int32** |  | [optional] [default to null]
**DestIp** | **string** |  | [optional] [default to null]
**DestPort** | **int32** |  | [optional] [default to null]
**IcmpType** | **string** |  | [optional] [default to null]
**Length** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


