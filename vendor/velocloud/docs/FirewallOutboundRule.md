# FirewallOutboundRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] [default to null]
**Match** | [***FirewallRuleMatch**](firewall_rule_match.md) |  | [default to null]
**Action** | [***FirewallOutboundRuleAction**](firewall_outbound_rule_action.md) |  | [default to null]
**RuleLogicalId** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


