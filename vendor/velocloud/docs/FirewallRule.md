# FirewallRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Data** | **string** |  | [optional] [default to null]
**Deactivated** | **string** |  | [optional] [default to null]
**EdgeLogicalId** | **string** |  | [optional] [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Id** | **int32** |  | [optional] [default to null]
**LogicalId** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


