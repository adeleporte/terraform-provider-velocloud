# FirewallRuleMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Appid** | **int32** | Integer ID corresponding to an application in the network-level application map | [optional] [default to null]
**Classid** | **int32** | Integer ID corresponding to an application class in the network-level application map | [optional] [default to null]
**Dscp** | **int32** | Integer ID indicating DSCP classification | [optional] [default to null]
**Sip** | **string** | Source IP address | [optional] [default to null]
**SportHigh** | **int32** | Upper bound of a source port range | [optional] [default to null]
**SportLow** | **int32** | Lower bound of a source port range | [optional] [default to null]
**Ssm** | **string** | Source subnet mask, e.g. 255.255.255.0 | [optional] [default to null]
**Smac** | **string** | Source MAC address | [optional] [default to null]
**Svlan** | **int32** | Integer ID for the source VLAN | [optional] [default to null]
**OsVersion** | **int32** |  | [optional] [default to null]
**Hostname** | **string** |  | [optional] [default to null]
**Dip** | **string** | Destination IP address | [optional] [default to null]
**DportLow** | **int32** | Lower bound of a destination port range | [optional] [default to null]
**DportHigh** | **int32** | Upper bound of a destination port range | [optional] [default to null]
**Dsm** | **string** | Destination subnet mask e.g. 255.255.255.0 | [optional] [default to null]
**Dmac** | **string** | Destination MAC address | [optional] [default to null]
**Dvlan** | **int32** | Integer ID for the destination VLAN | [optional] [default to null]
**Proto** | **int32** | Integer ID corresponding to a protocol | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


