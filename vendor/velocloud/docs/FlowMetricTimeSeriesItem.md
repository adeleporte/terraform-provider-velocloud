# FlowMetricTimeSeriesItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | **[]int32** |  | [default to null]
**Max** | **int32** |  | [default to null]
**Metric** | [***BasicMetric**](basic_metric.md) |  | [default to null]
**Min** | **int32** |  | [default to null]
**StartTime** | [**time.Time**](time.Time.md) |  | [default to null]
**TickInterval** | **int32** |  | [default to null]
**Total** | **int32** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


