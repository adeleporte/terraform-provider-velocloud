# GatewayCertificate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**CsrId** | **int32** |  | [optional] [default to null]
**GatewayId** | **int32** |  | [optional] [default to null]
**NetworkId** | **int32** |  | [optional] [default to null]
**Certificate** | **string** |  | [optional] [default to null]
**SerialNumber** | **string** |  | [optional] [default to null]
**SubjectKeyId** | **string** |  | [optional] [default to null]
**FingerPrint** | **string** |  | [optional] [default to null]
**ValidFrom** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**ValidTo** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


