# GatewayGatewayProvision

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NetworkId** | **int32** |  | [default to null]
**IpAddress** | **string** |  | [default to null]
**GatewayPoolId** | **int32** |  | [optional] [default to null]
**DnsName** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**IsLoadBalanced** | **bool** |  | [optional] [default to null]
**PrivateIpAddress** | **string** |  | [optional] [default to null]
**Roles** | [***GatewayRolesObject**](gateway_roles_object.md) |  | [optional] [default to null]
**Site** | [***SiteObject**](site_object.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


