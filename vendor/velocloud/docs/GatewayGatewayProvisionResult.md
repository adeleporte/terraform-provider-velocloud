# GatewayGatewayProvisionResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActivationKey** | **string** |  | [default to null]
**Id** | **int32** |  | [default to null]
**LogicalId** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


