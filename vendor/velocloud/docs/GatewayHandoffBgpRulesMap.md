# GatewayHandoffBgpRulesMap

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Rules** | [**[]GatewayHandoffBgpRule**](gateway_handoff_bgp_rule.md) |  | [optional] [default to null]
**Override** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


