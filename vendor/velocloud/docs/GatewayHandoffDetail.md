# GatewayHandoffDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type_** | **string** |  | [optional] [default to null]
**Subnets** | [**[]GatewayHandoffDetailSubnets**](gateway_handoff_detail_subnets.md) |  | [optional] [default to null]
**IcmpProbe** | [***GatewayHandoffDetailIcmpProbe**](gateway_handoff_detail_icmpProbe.md) |  | [optional] [default to null]
**IcmpResponder** | [***GatewayHandoffDetailIcmpResponder**](gateway_handoff_detail_icmpResponder.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


