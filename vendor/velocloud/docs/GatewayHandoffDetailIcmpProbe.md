# GatewayHandoffDetailIcmpProbe

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**ProbeType** | **string** |  | [optional] [default to null]
**CTag** | **int32** |  | [optional] [default to null]
**STag** | **int32** |  | [optional] [default to null]
**DestinationIp** | **string** |  | [optional] [default to null]
**FrequencySeconds** | **int32** |  | [optional] [default to null]
**Threshold** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


