# GatewayHandoffDetailSubnets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] [default to null]
**RouteCost** | **int32** |  | [optional] [default to null]
**CidrIp** | **string** |  | [optional] [default to null]
**CidrPrefix** | **int32** |  | [optional] [default to null]
**Encrypt** | **bool** |  | [optional] [default to null]
**HandOffType** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


