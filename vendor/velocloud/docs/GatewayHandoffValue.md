# GatewayHandoffValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BgpPrioritySetup** | [***GatewayHandoffValueBgpPrioritySetup**](gateway_handoff_value_bgpPrioritySetup.md) |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Override** | **bool** |  | [optional] [default to null]
**CTag** | **int32** |  | [optional] [default to null]
**STag** | **int32** |  | [optional] [default to null]
**LocalAddress** | [***GatewayHandoffValueLocalAddress**](gateway_handoff_value_localAddress.md) |  | [optional] [default to null]
**StaticRoutes** | [***GatewayHandoffValueStaticRoutes**](gateway_handoff_value_staticRoutes.md) |  | [optional] [default to null]
**Bgp** | [***GatewayHandoffValueBgp**](gateway_handoff_value_bgp.md) |  | [optional] [default to null]
**BgpInboundMap** | [***GatewayHandoffBgpRulesMap**](gateway_handoff_bgp_rules_map.md) |  | [optional] [default to null]
**BgpOutboundMap** | [***GatewayHandoffBgpRulesMap**](gateway_handoff_bgp_rules_map.md) |  | [optional] [default to null]
**Overrides** | [***GatewayHandoffValueOverrides**](gateway_handoff_value_overrides.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


