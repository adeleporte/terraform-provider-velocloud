# GatewayHandoffValueBgp

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**ASN** | **string** |  | [optional] [default to null]
**NeighborIp** | **string** |  | [optional] [default to null]
**NeighborASN** | **string** |  | [optional] [default to null]
**Encryption** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


