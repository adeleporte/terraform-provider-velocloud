# GatewayHandoffValueBgpPrioritySetup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AutoAs** | [***GatewayHandoffValueBgpPrioritySetupAutoAs**](gateway_handoff_value_bgpPrioritySetup_autoAs.md) |  | [optional] [default to null]
**AutoMed** | [***GatewayHandoffValueBgpPrioritySetupAutoAs**](gateway_handoff_value_bgpPrioritySetup_autoAs.md) |  | [optional] [default to null]
**CommunityMapping** | [***GatewayHandoffValueBgpPrioritySetupCommunityMapping**](gateway_handoff_value_bgpPrioritySetup_communityMapping.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


