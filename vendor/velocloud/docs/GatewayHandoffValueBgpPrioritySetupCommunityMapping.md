# GatewayHandoffValueBgpPrioritySetupCommunityMapping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Enabled** | **bool** |  | [optional] [default to null]
**Priorities** | [**[]GatewayHandoffValueBgpPrioritySetupCommunityMappingPriorities**](gateway_handoff_value_bgpPrioritySetup_communityMapping_priorities.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


