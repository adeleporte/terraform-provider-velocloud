# GatewayHandoffValueBgpPrioritySetupCommunityMappingPriorities

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Community** | **string** |  | [optional] [default to null]
**Community2** | **string** |  | [optional] [default to null]
**Priority** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


