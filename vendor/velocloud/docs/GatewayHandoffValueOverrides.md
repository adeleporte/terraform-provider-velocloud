# GatewayHandoffValueOverrides

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**VLAN** | [**map[string]GatewayHandoffValueOverridesVlan**](gateway_handoff_value_overrides_VLAN.md) |  | [optional] [default to null]
**Bgp** | [**map[string]GatewayHandoffValueBgp**](gateway_handoff_value_bgp.md) |  | [optional] [default to null]
**BgpInboundMap** | [**map[string]GatewayHandoffBgpRulesMap**](gateway_handoff_bgp_rules_map.md) |  | [optional] [default to null]
**BgpOutboundMap** | [**map[string]GatewayHandoffBgpRulesMap**](gateway_handoff_bgp_rules_map.md) |  | [optional] [default to null]
**LocalAddress** | [**map[string]GatewayHandoffValueOverridesLocalAddress**](gateway_handoff_value_overrides_localAddress.md) |  | [optional] [default to null]
**Subnets** | [**map[string][]interface{}**](array.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


