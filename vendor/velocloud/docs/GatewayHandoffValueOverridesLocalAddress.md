# GatewayHandoffValueOverridesLocalAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CidrIp** | **string** |  | [optional] [default to null]
**CidrPrefix** | **int32** |  | [optional] [default to null]
**UseForPrivate** | **bool** |  | [optional] [default to null]
**AdvertiseViaBgp** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


