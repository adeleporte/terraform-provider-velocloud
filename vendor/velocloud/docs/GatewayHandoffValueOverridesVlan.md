# GatewayHandoffValueOverridesVlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type_** | **string** |  | [optional] [default to null]
**CTag** | **int32** |  | [optional] [default to null]
**STag** | **int32** |  | [optional] [default to null]
**TransportLanVLAN** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


