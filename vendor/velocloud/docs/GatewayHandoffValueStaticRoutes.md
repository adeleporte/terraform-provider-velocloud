# GatewayHandoffValueStaticRoutes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Override** | **bool** |  | [optional] [default to null]
**Subnets** | [**[]GatewayHandoffValueStaticRoutesSubnets**](gateway_handoff_value_staticRoutes_subnets.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


