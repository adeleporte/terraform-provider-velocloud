# GatewayPoolGatewayIpsecGatewayDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StrictHostCheck** | **bool** |  | [optional] [default to null]
**StrictHostCheckDN** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


