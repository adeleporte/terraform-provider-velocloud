# GatewayPoolGatewayUtilizationDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Load** | **float32** |  | [optional] [default to null]
**Overall** | **float32** |  | [optional] [default to null]
**Cpu** | **float32** |  | [optional] [default to null]
**Memory** | **float32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


