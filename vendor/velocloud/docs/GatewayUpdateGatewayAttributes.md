# GatewayUpdateGatewayAttributes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [default to null]
**Name** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Site** | [***SiteObject**](site_object.md) |  | [optional] [default to null]
**IpsecGatewayDetail** | [***GatewayupdateGatewayAttributesIpsecGatewayDetail**](gatewayupdateGatewayAttributes_ipsecGatewayDetail.md) |  | [optional] [default to null]
**HandOffDetail** | [***GatewayHandoffDetail**](gateway_handoff_detail.md) |  | [optional] [default to null]
**Roles** | [***GatewayRolesObject**](gateway_roles_object.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


