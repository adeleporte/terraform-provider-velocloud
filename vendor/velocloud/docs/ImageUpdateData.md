# ImageUpdateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BuildNumber** | **string** |  | [optional] [default to null]
**ProfileDeviceFamily** | **string** |  | [optional] [default to null]
**ProfileVersion** | **string** |  | [optional] [default to null]
**SoftwarePackageId** | **int32** |  | [optional] [default to null]
**SoftwarePackageName** | **string** |  | [optional] [default to null]
**Version** | **string** |  | [optional] [default to null]
**WindowDurationMins** | **int32** |  | [optional] [default to null]
**Windowed** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


