# Interval

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**End** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Start** | [**time.Time**](time.Time.md) |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


