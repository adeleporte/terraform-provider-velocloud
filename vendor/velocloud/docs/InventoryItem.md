# InventoryItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**DeviceSerialNumber** | **string** |  | [optional] [default to null]
**DeviceUuid** | **string** |  | [optional] [default to null]
**ModelNumber** | **string** |  | [optional] [default to null]
**SiteId** | **int32** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Acknowledged** | **int32** |  | [optional] [default to null]
**EdgeId** | **int32** |  | [optional] [default to null]
**Edge** | [***InventoryItemEdge**](inventory_item_edge.md) |  | [optional] [default to null]
**InventoryState** | **string** |  | [optional] [default to null]
**InventoryEdgeState** | **string** |  | [optional] [default to null]
**InventoryAction** | **string** |  | [optional] [default to null]
**VcoOwnerId** | **int32** |  | [optional] [default to null]
**VcoOwner** | [***InventoryItemVcoOwner**](inventory_item_vcoOwner.md) |  | [optional] [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


