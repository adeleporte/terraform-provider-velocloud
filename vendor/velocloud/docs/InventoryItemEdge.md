# InventoryItemEdge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Edge** | **string** |  | [optional] [default to null]
**Site** | [***InventoryItemEdgeSite**](inventory_item_edge_site.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


