# \InventoryManagementApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**VcoInventoryGetInventoryItems**](InventoryManagementApi.md#VcoInventoryGetInventoryItems) | **Post** /vcoInventory/getInventoryItems | Return inventory items available at this VCO


# **VcoInventoryGetInventoryItems**
> VcoInventoryGetInventoryItemsResult VcoInventoryGetInventoryItems(ctx, body)
Return inventory items available at this VCO

Retrieve all the inventory information available with this VCO. This method does not have required parameters. The optional parameters are  enterpriseId - Return inventory items belonging to that enterprise. If the caller context is an enterprise, this value will be taken from token itself. modifiedSince - Used to retrieve inventory items that have been modified in the last modifiedSince hours. with - an array containing the string \"edge\" to get details about details about the provisioned edge if any.  Privileges required:  `READ` `INVENTORY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**VcoInventoryGetInventoryItems**](VcoInventoryGetInventoryItems.md)|  | 

### Return type

[**VcoInventoryGetInventoryItemsResult**](vco_inventory_get_inventory_items_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

