# Link

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [default to null]
**EdgeId** | **int32** |  | [default to null]
**LogicalId** | **string** |  | [default to null]
**InternalId** | **string** |  | [default to null]
**Interface_** | **string** |  | [default to null]
**MacAddress** | **string** |  | [default to null]
**IpAddress** | **string** |  | [default to null]
**Netmask** | **string** |  | [default to null]
**NetworkSide** | **string** |  | [default to null]
**NetworkType** | **string** |  | [default to null]
**DisplayName** | **string** |  | [default to null]
**Isp** | **string** |  | [default to null]
**Org** | **string** |  | [default to null]
**Lat** | **float32** |  | [default to null]
**Lon** | **float32** |  | [default to null]
**LastActive** | [**time.Time**](time.Time.md) |  | [default to null]
**State** | **string** |  | [default to null]
**BackupState** | **string** |  | [default to null]
**VpnState** | **string** |  | [default to null]
**LastEvent** | [**time.Time**](time.Time.md) |  | [default to null]
**LastEventState** | **string** |  | [default to null]
**AlertsEnabled** | **int32** |  | [default to null]
**OperatorAlertsEnabled** | **int32** |  | [default to null]
**ServiceState** | **string** |  | [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [default to null]
**ServiceGroups** | [***LinkServiceGroups**](link_service_groups.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


