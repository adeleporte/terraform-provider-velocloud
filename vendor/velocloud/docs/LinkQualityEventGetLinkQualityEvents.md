# LinkQualityEventGetLinkQualityEvents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EdgeId** | **int32** |  | [default to null]
**Interval** | [***Interval**](interval.md) |  | [default to null]
**MaxSamples** | **int32** |  | [optional] [default to null]
**IndividualScores** | **bool** |  | [optional] [default to null]
**MinutesPerSample** | **int32** |  | [optional] [default to null]
**Debug** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


