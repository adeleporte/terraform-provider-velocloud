# LinkQualityObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Distribution** | [***LinkQualityObjectDistribution**](link_quality_object_distribution.md) |  | [default to null]
**SampleCount** | **int32** |  | [default to null]
**SampleLength** | **int32** |  | [default to null]
**Score** | **map[string]float32** |  | [default to null]
**Timeseries** | [**[]LinkQualityObjectTimeseriesData**](link_quality_object_timeseries_data.md) |  | [default to null]
**TotalScore** | **float32** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


