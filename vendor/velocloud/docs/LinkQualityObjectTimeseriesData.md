# LinkQualityObjectTimeseriesData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Before** | **map[string]int32** |  | [optional] [default to null]
**After** | **map[string]int32** |  | [default to null]
**Metadata** | [***LinkQualityObjectTimeseriesDataMetadata**](link_quality_object_timeseries_data_metadata.md) |  | [default to null]
**Timestamp** | **int32** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


