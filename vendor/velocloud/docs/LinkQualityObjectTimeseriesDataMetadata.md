# LinkQualityObjectTimeseriesDataMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Detail** | [***LinkQualityObjectTimeseriesDataMetadataDetail**](link_quality_object_timeseries_data_metadata_detail.md) |  | [optional] [default to null]
**Metrics** | [**[]LinkQualityObjectTimeseriesDataMetadataMetrics**](link_quality_object_timeseries_data_metadata_metrics.md) |  | [optional] [default to null]
**StateMap** | [**map[string]map[string]interface{}**](map.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


