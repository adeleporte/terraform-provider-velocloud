# LinkQualityObjectTimeseriesDataMetadataDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LatencyMsRx** | **int32** |  | [optional] [default to null]
**LatencyMsTx** | **int32** |  | [optional] [default to null]
**LossPctRx** | **int32** |  | [optional] [default to null]
**LossPctTx** | **int32** |  | [optional] [default to null]
**JitterMsRx** | **int32** |  | [optional] [default to null]
**JitterMsTx** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


