# LinkQualityObjectTimeseriesDataMetadataMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TrafficType** | **int32** |  | [optional] [default to null]
**Action** | **int32** |  | [optional] [default to null]
**Metric** | **int32** |  | [optional] [default to null]
**BeforeState** | **int32** |  | [optional] [default to null]
**AfterState** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


