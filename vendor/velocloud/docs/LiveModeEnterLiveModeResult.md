# LiveModeEnterLiveModeResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActionId** | **int32** | The ID of the action queued | [optional] [default to null]
**AlreadyLive** | **bool** |  | [optional] [default to null]
**LastContact** | **int32** |  | [optional] [default to null]
**RefreshIntervalMs** | **int32** |  | [optional] [default to null]
**Token** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


