# LiveModeReadLiveDataResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [***LiveModeData**](live_mode_data.md) |  | [default to null]
**Status** | [***LiveModeStatus**](live_mode_status.md) |  | [default to null]
**Timestamp** | **int32** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


