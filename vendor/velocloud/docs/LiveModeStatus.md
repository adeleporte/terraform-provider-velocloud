# LiveModeStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IsActive** | **bool** |  | [default to null]
**LastContact** | **int32** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


