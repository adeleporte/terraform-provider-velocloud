# \LoginApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**LoginEnterpriseLogin**](LoginApi.md#LoginEnterpriseLogin) | **Post** /login/enterpriseLogin | Authentication for non-operator users
[**LoginOperatorLogin**](LoginApi.md#LoginOperatorLogin) | **Post** /login/operatorLogin | Authentication for an operator user
[**Logout**](LoginApi.md#Logout) | **Post** /logout | Deactivate a given authorization cookie


# **LoginEnterpriseLogin**
> LoginEnterpriseLogin(ctx, authorization)
Authentication for non-operator users

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **authorization** | [**AuthObject**](AuthObject.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **LoginOperatorLogin**
> LoginOperatorLogin(ctx, authorization)
Authentication for an operator user

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **authorization** | [**AuthObject**](AuthObject.md)|  | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Logout**
> Logout(ctx, )
Deactivate a given authorization cookie

### Required Parameters
This endpoint does not need any parameter.

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

