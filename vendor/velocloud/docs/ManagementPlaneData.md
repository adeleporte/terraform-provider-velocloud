# ManagementPlaneData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**HeartBeatSeconds** | **int32** |  | [optional] [default to null]
**ManagementPlaneProxy** | [***ManagementPlaneDataManagementPlaneProxy**](management_plane_data_managementPlaneProxy.md) |  | [optional] [default to null]
**StatsUploadSeconds** | **int32** |  | [optional] [default to null]
**TimeSliceSeconds** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


