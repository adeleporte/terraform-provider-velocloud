# MetadataData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Applications** | [***MetadataDataApplications**](metadata_data_applications.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


