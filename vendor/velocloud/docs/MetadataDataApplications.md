# MetadataDataApplications

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LogicalId** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Version** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


