# MetricsGetEdgeCategoryMetricsResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BytesRx** | **int32** |  | [optional] [default to null]
**BytesTx** | **int32** |  | [optional] [default to null]
**FlowCount** | **int32** |  | [optional] [default to null]
**PacketsRx** | **int32** |  | [optional] [default to null]
**PacketsTx** | **int32** |  | [optional] [default to null]
**TotalBytes** | **int32** |  | [optional] [default to null]
**TotalPackets** | **int32** |  | [optional] [default to null]
**Category** | **int32** |  | [default to null]
**Name** | **int32** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


