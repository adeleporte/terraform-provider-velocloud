# MetricsGetEdgeDeviceMetricsDeviceEdgeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EdgeId** | **int32** |  | [optional] [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**HostName** | **string** |  | [optional] [default to null]
**Id** | **int32** |  | [optional] [default to null]
**IpAddress** | **string** |  | [optional] [default to null]
**LastContact** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**MacAddress** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


