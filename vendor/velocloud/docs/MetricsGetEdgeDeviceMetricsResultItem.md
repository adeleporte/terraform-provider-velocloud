# MetricsGetEdgeDeviceMetricsResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BytesRx** | **int32** |  | [optional] [default to null]
**BytesTx** | **int32** |  | [optional] [default to null]
**FlowCount** | **int32** |  | [optional] [default to null]
**PacketsRx** | **int32** |  | [optional] [default to null]
**PacketsTx** | **int32** |  | [optional] [default to null]
**TotalBytes** | **int32** |  | [optional] [default to null]
**TotalPackets** | **int32** |  | [optional] [default to null]
**EdgeInfo** | [***MetricsGetEdgeDeviceMetricsDeviceEdgeInfo**](metrics_get_edge_device_metrics_device_edge_info.md) |  | [default to null]
**Info** | [***ClientDevice**](client_device.md) |  | [default to null]
**Name** | **string** |  | [default to null]
**SourceMac** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


