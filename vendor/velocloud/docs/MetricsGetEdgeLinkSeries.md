# MetricsGetEdgeLinkSeries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Interval** | [***Interval**](interval.md) |  | [default to null]
**Metrics** | [**[]EdgeLinkMetric**](edge_link_metric.md) |  | [optional] [default to null]
**Sort** | [***EdgeLinkMetric**](edge_link_metric.md) |  | [optional] [default to null]
**Limit** | **int32** |  | [optional] [default to null]
**Links** | **[]int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


