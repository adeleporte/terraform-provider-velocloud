# MetricsGetEdgeLinkSeriesResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Series** | [**[]FlowMetricTimeSeriesItem**](flow_metric_time_series_item.md) |  | [default to null]
**EdgeId** | **int32** |  | [default to null]
**Link** | [***Link**](link.md) |  | [default to null]
**LinkId** | **int32** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


