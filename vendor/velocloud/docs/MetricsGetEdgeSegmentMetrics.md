# MetricsGetEdgeSegmentMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**Interval** | [***Interval**](interval.md) |  | [default to null]
**Metrics** | [***BasicMetrics**](basic_metrics.md) |  | [optional] [default to null]
**Sort** | [***BasicMetric**](basic_metric.md) |  | [optional] [default to null]
**Limit** | **int32** |  | [optional] [default to null]
**Segments** | **[]int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


