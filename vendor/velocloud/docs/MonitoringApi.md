# \MonitoringApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**MonitoringGetAggregateEdgeLinkMetrics**](MonitoringApi.md#MonitoringGetAggregateEdgeLinkMetrics) | **Post** /monitoring/getAggregateEdgeLinkMetrics | Get aggregate Edge link metrics across enterprises
[**MonitoringGetAggregateEnterpriseEvents**](MonitoringApi.md#MonitoringGetAggregateEnterpriseEvents) | **Post** /monitoring/getAggregateEnterpriseEvents | Get events across all enterprises
[**MonitoringGetAggregates**](MonitoringApi.md#MonitoringGetAggregates) | **Post** /monitoring/getAggregates | Get aggregate enterprise and edge information
[**MonitoringGetEnterpriseBgpPeerStatus**](MonitoringApi.md#MonitoringGetEnterpriseBgpPeerStatus) | **Post** /monitoring/getEnterpriseBgpPeerStatus | Get gateway BGP peer status for all enterprise gateways
[**MonitoringGetEnterpriseEdgeBgpPeerStatus**](MonitoringApi.md#MonitoringGetEnterpriseEdgeBgpPeerStatus) | **Post** /monitoring/getEnterpriseEdgeBgpPeerStatus | Get edge BGP peer status for all enterprise edges
[**MonitoringGetEnterpriseEdgeLinkStatus**](MonitoringApi.md#MonitoringGetEnterpriseEdgeLinkStatus) | **Post** /monitoring/getEnterpriseEdgeLinkStatus | Get edge and link status data


# **MonitoringGetAggregateEdgeLinkMetrics**
> []MonitoringGetAggregateEdgeLinkMetricsResultItem MonitoringGetAggregateEdgeLinkMetrics(ctx, body)
Get aggregate Edge link metrics across enterprises

Gets aggregate link metrics for the request interval for all active links across all enterprises, where a link is considered to be active if an Edge has reported any activity for it in the last 24 hours. On success, returns an array of network utilization metrics, one per link.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MonitoringGetAggregateEdgeLinkMetrics**](MonitoringGetAggregateEdgeLinkMetrics.md)|  | 

### Return type

[**[]MonitoringGetAggregateEdgeLinkMetricsResultItem**](monitoring_get_aggregate_edge_link_metrics_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetAggregateEnterpriseEvents**
> MonitoringGetAggregateEnterpriseEventsResult MonitoringGetAggregateEnterpriseEvents(ctx, body)
Get events across all enterprises

Gets events across all enterprises in a paginated list. When called in the MSP/Partner context, queries only enterprises managed by the MSP.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MonitoringGetAggregateEnterpriseEvents**](MonitoringGetAggregateEnterpriseEvents.md)|  | 

### Return type

[**MonitoringGetAggregateEnterpriseEventsResult**](monitoring_get_aggregate_enterprise_events_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetAggregates**
> MonitoringGetAggregatesResult MonitoringGetAggregates(ctx, body)
Get aggregate enterprise and edge information

Retrieve a comprehensive listing of all enterprises and edges on a network. Returns an object containing an aggregate `edgeCount`, a list (`enterprises`) containing enterprise objects, and a map (`edges`) which gives edge counts per enterprise.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**MonitoringGetAggregates**](MonitoringGetAggregates.md)|  | 

### Return type

[**MonitoringGetAggregatesResult**](monitoring_get_aggregates_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetEnterpriseBgpPeerStatus**
> []MonitoringGetEnterpriseBgpPeerStatusResultItem MonitoringGetEnterpriseBgpPeerStatus(ctx, body)
Get gateway BGP peer status for all enterprise gateways

Returns an array where each entry corresponds to a gateway and contains an associated set of BGP peers with state records.  Privileges required:  `READ` `NETWORK_SERVICE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body**](Body.md)|  | 

### Return type

[**[]MonitoringGetEnterpriseBgpPeerStatusResultItem**](monitoring_get_enterprise_bgp_peer_status_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetEnterpriseEdgeBgpPeerStatus**
> []MonitoringGetEnterpriseEdgeBgpPeerStatusResultItem MonitoringGetEnterpriseEdgeBgpPeerStatus(ctx, body)
Get edge BGP peer status for all enterprise edges

Returns an array where each entry corresponds to an edge and contains an associated set of BGP peers and state records.  Privileges required:  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body1**](Body1.md)|  | 

### Return type

[**[]MonitoringGetEnterpriseEdgeBgpPeerStatusResultItem**](monitoring_get_enterprise_edge_bgp_peer_status_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MonitoringGetEnterpriseEdgeLinkStatus**
> []MonitoringGetEnterpriseEdgeLinkStatusResultItem MonitoringGetEnterpriseEdgeLinkStatus(ctx, body)
Get edge and link status data

Get current edge and edge-link status for all enterprise edges.  Privileges required:  `READ` `ENTERPRISE`  `READ` `EDGE`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body2**](Body2.md)|  | 

### Return type

[**[]MonitoringGetEnterpriseEdgeLinkStatusResultItem**](monitoring_get_enterprise_edge_link_status_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

