# MonitoringGetAggregateEdgeLinkMetricsResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BestJitterMsRx** | **int32** |  | [optional] [default to null]
**BestJitterMsTx** | **int32** |  | [optional] [default to null]
**BestLatencyMsRx** | **int32** |  | [optional] [default to null]
**BestLatencyMsTx** | **int32** |  | [optional] [default to null]
**BestLossPctRx** | **int32** |  | [optional] [default to null]
**BestLossPctTx** | **int32** |  | [optional] [default to null]
**BpsOfBestPathRx** | **int32** |  | [optional] [default to null]
**BpsOfBestPathTx** | **int32** |  | [optional] [default to null]
**BytesRx** | **int32** |  | [optional] [default to null]
**BytesTx** | **int32** |  | [optional] [default to null]
**ControlBytesRx** | **int32** |  | [optional] [default to null]
**ControlBytesTx** | **int32** |  | [optional] [default to null]
**ControlPacketsRx** | **int32** |  | [optional] [default to null]
**ControlPacketsTx** | **int32** |  | [optional] [default to null]
**Link** | [***MonitoringGetAggregateEdgeLinkMetricsResultItemLink**](monitoring_get_aggregate_edge_link_metrics_result_item_link.md) |  | [default to null]
**LinkId** | **int32** |  | [default to null]
**Name** | **string** |  | [default to null]
**P1BytesRx** | **int32** |  | [optional] [default to null]
**P1BytesTx** | **int32** |  | [optional] [default to null]
**P1PacketsRx** | **int32** |  | [optional] [default to null]
**P1PacketsTx** | **int32** |  | [optional] [default to null]
**P2BytesRx** | **int32** |  | [optional] [default to null]
**P2BytesTx** | **int32** |  | [optional] [default to null]
**P2PacketsRx** | **int32** |  | [optional] [default to null]
**P2PacketsTx** | **int32** |  | [optional] [default to null]
**P3BytesRx** | **int32** |  | [optional] [default to null]
**P3BytesTx** | **int32** |  | [optional] [default to null]
**P3PacketsRx** | **int32** |  | [optional] [default to null]
**P3PacketsTx** | **int32** |  | [optional] [default to null]
**ScoreRx** | **int32** |  | [optional] [default to null]
**ScoreTx** | **int32** |  | [optional] [default to null]
**SignalStrength** | **int32** |  | [optional] [default to null]
**State** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


