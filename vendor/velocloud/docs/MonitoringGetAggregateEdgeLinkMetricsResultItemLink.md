# MonitoringGetAggregateEdgeLinkMetricsResultItemLink

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseName** | **string** |  | [optional] [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**EdgeName** | **string** |  | [optional] [default to null]
**EdgeSerialNumber** | **string** |  | [optional] [default to null]
**EdgeHASerialNumber** | **string** |  | [optional] [default to null]
**EdgeState** | **string** |  | [optional] [default to null]
**EdgeLastContact** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**EdgeId** | **int32** |  | [default to null]
**EdgeSystemUpSince** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**EdgeServiceUpSince** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**EdgeModelNumber** | **string** |  | [optional] [default to null]
**Isp** | **string** |  | [default to null]
**DisplayName** | **string** |  | [default to null]
**Interface_** | **string** |  | [default to null]
**LinkId** | **int32** |  | [optional] [default to null]
**LinkState** | **string** |  | [optional] [default to null]
**LinkLastActive** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**LinkVpnState** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


