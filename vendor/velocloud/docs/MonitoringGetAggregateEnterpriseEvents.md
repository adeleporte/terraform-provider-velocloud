# MonitoringGetAggregateEnterpriseEvents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Detail** | **bool** |  | [optional] [default to null]
**Interval** | [***MonitoringgetAggregateEnterpriseEventsInterval**](monitoringgetAggregateEnterpriseEvents_interval.md) |  | [optional] [default to null]
**Filter** | [***MonitoringgetAggregateEnterpriseEventsFilter**](monitoringgetAggregateEnterpriseEvents_filter.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


