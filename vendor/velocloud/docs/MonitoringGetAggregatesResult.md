# MonitoringGetAggregatesResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EdgeCount** | **int32** |  | [optional] [default to null]
**Edges** | **map[string]int32** |  | [optional] [default to null]
**Enterprises** | [**[]EnterpriseWithProxyAttributes**](enterprise_with_proxy_attributes.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


