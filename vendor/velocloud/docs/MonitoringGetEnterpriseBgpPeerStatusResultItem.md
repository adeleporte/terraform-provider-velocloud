# MonitoringGetEnterpriseBgpPeerStatusResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GatewayName** | **string** |  | [optional] [default to null]
**GatewayLogicalId** | **string** |  | [optional] [default to null]
**Neighbors** | [**[]BgpPeerStatus**](bgp_peer_status.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


