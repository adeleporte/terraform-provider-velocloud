# MonitoringGetEnterpriseEdgeBgpPeerStatusResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EdgeName** | **string** |  | [optional] [default to null]
**EdgeId** | **int32** |  | [optional] [default to null]
**EdgeLogicalId** | **string** |  | [optional] [default to null]
**Neighbors** | [**[]BgpPeerStatus**](bgp_peer_status.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


