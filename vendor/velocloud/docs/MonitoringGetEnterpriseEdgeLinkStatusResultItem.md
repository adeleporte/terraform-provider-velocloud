# MonitoringGetEnterpriseEdgeLinkStatusResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EdgeId** | **int32** |  | [default to null]
**EdgeLastContact** | [**time.Time**](time.Time.md) |  | [default to null]
**EdgeModelNumber** | **string** |  | [default to null]
**EdgeName** | **string** |  | [default to null]
**EdgeServiceUpSince** | [**time.Time**](time.Time.md) |  | [default to null]
**EdgeState** | **string** |  | [default to null]
**EdgeSystemUpSince** | [**time.Time**](time.Time.md) |  | [default to null]
**EnterpriseId** | **int32** |  | [default to null]
**EnterpriseName** | **string** |  | [default to null]
**Interface_** | **string** |  | [default to null]
**Isp** | **string** |  | [default to null]
**LinkId** | **int32** |  | [default to null]
**LinkLastActive** | [**time.Time**](time.Time.md) |  | [default to null]
**LinkState** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


