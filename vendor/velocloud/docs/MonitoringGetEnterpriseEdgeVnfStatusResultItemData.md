# MonitoringGetEnterpriseEdgeVnfStatusResultItemData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type_** | **string** |  | [optional] [default to null]
**Vendor** | **string** |  | [optional] [default to null]
**VendorSpecificData** | [***interface{}**](interface{}.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


