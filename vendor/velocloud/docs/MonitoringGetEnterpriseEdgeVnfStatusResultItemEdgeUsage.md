# MonitoringGetEnterpriseEdgeVnfStatusResultItemEdgeUsage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ConfigurationId** | **int32** |  | [optional] [default to null]
**EdgeSpecificId** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**LogicalId** | **string** |  | [optional] [default to null]
**ProfileId** | **int32** |  | [optional] [default to null]
**VnfStatus** | [***MonitoringGetEnterpriseEdgeVnfStatusResultItemVnfStatus**](monitoring_get_enterprise_edge_vnf_status_result_item_vnfStatus.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


