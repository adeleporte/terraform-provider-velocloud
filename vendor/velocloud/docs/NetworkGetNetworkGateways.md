# NetworkGetNetworkGateways

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NetworkId** | **int32** |  | [default to null]
**With** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


