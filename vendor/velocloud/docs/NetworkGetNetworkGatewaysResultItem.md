# NetworkGetNetworkGatewaysResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ActivationKey** | **string** |  | [optional] [default to null]
**ActivationState** | **string** |  | [optional] [default to null]
**ActivationTime** | **string** |  | [optional] [default to null]
**AlertsEnabled** | **bool** |  | [optional] [default to null]
**BuildNumber** | **string** |  | [optional] [default to null]
**Certificates** | [**[]GatewayCertificate**](gateway_certificate.md) |  | [optional] [default to null]
**ConnectedEdges** | **int32** |  | [optional] [default to null]
**ConnectedEdgeList** | [**[]GatewayPoolGatewayConnectedEdgeList**](gateway_pool_gateway_connectedEdgeList.md) |  | [optional] [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**DataCenters** | [**[]EnterpriseObjectBase**](enterprise_object_base.md) |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**DeviceId** | **string** |  | [optional] [default to null]
**DnsName** | **string** |  | [optional] [default to null]
**EndpointPkiMode** | **string** |  | [optional] [default to null]
**EnterpriseAssociations** | [**[]GatewayEnterpriseAssoc**](gateway_enterprise_assoc.md) |  | [optional] [default to null]
**Enterprises** | [**[]Enterprise**](enterprise.md) |  | [optional] [default to null]
**EnterpriseProxyId** | **int32** |  | [optional] [default to null]
**GatewayState** | **string** |  | [optional] [default to null]
**HandOffDetail** | [***GatewayHandoffDetail**](gateway_handoff_detail.md) |  | [optional] [default to null]
**HandOffEdges** | [**[]GatewayHandoffEdge**](gateway_handoff_edge.md) |  | [optional] [default to null]
**Id** | **int32** |  | [optional] [default to null]
**IpAddress** | **string** |  | [optional] [default to null]
**IpsecGatewayDetail** | [***GatewayPoolGatewayIpsecGatewayDetail**](gateway_pool_gateway_ipsecGatewayDetail.md) |  | [optional] [default to null]
**IsLoadBalanced** | **bool** |  | [optional] [default to null]
**LastContact** | **string** |  | [optional] [default to null]
**LogicalId** | **string** |  | [optional] [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**NetworkId** | **int32** |  | [optional] [default to null]
**Pools** | [**[]GatewayGatewayPool**](gateway_gateway_pool.md) |  | [optional] [default to null]
**PrivateIpAddress** | **string** |  | [optional] [default to null]
**Roles** | [**[]GatewayRole**](gateway_role.md) |  | [optional] [default to null]
**ServiceState** | **string** |  | [optional] [default to null]
**ServiceUpSince** | **string** |  | [optional] [default to null]
**Site** | [***GatewaySite**](gateway_site.md) |  | [optional] [default to null]
**SiteId** | **int32** |  | [optional] [default to null]
**SoftwareVersion** | **string** |  | [optional] [default to null]
**SystemUpSince** | **string** |  | [optional] [default to null]
**Utilization** | **float32** |  | [optional] [default to null]
**UtilizationDetail** | [***GatewayPoolGatewayUtilizationDetail**](gateway_pool_gateway_utilizationDetail.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


