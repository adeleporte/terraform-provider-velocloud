# NetworkInsertNetworkGatewayPool

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**NetworkId** | **int32** |  | [default to null]
**EnterpriseProxyId** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [default to null]
**Description** | **string** |  | [optional] [default to null]
**HandOffType** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


