# NetworkSegmentData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SegmentId** | **int32** |  | [optional] [default to null]
**ServiceVlan** | **int32** |  | [optional] [default to null]
**DelegateToEnterprise** | **bool** |  | [optional] [default to null]
**DelegateToEnterpriseProxy** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


