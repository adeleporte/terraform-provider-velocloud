# OperatorEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**EventTime** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Event** | **string** |  | [optional] [default to null]
**Category** | **string** |  | [optional] [default to null]
**Severity** | **string** |  | [optional] [default to null]
**Message** | **string** |  | [optional] [default to null]
**Detail** | **string** |  | [default to null]
**OperatorUsername** | **string** |  | [default to null]
**NetworkName** | **string** |  | [default to null]
**GatewayName** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


