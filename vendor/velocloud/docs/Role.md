# Role

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**OperatorId** | **int32** |  | [optional] [default to null]
**NetworkId** | **int32** |  | [optional] [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**EnterpriseProxyId** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**UserType** | **string** |  | [optional] [default to null]
**FromUserType** | **string** |  | [optional] [default to null]
**IsSuper** | **int32** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Precedence** | **int32** |  | [optional] [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


