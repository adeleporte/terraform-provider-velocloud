# \RoleApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**RoleCreateRoleCustomization**](RoleApi.md#RoleCreateRoleCustomization) | **Post** /role/createRoleCustomization | Create a role customization
[**RoleDeleteRoleCustomization**](RoleApi.md#RoleDeleteRoleCustomization) | **Post** /role/deleteRoleCustomization | Delete a role customization
[**RoleGetUserTypeRoles**](RoleApi.md#RoleGetUserTypeRoles) | **Post** /role/getUserTypeRoles | Get the roles defined for a user type
[**RoleSetEnterpriseDelegatedToEnterpriseProxy**](RoleApi.md#RoleSetEnterpriseDelegatedToEnterpriseProxy) | **Post** /role/setEnterpriseDelegatedToEnterpriseProxy | Grant enterprise access to partner
[**RoleSetEnterpriseDelegatedToOperator**](RoleApi.md#RoleSetEnterpriseDelegatedToOperator) | **Post** /role/setEnterpriseDelegatedToOperator | Grant enterprise access to network operator
[**RoleSetEnterpriseProxyDelegatedToOperator**](RoleApi.md#RoleSetEnterpriseProxyDelegatedToOperator) | **Post** /role/setEnterpriseProxyDelegatedToOperator | Grant enterprise proxy access to network operator
[**RoleSetEnterpriseUserManagementDelegatedToOperator**](RoleApi.md#RoleSetEnterpriseUserManagementDelegatedToOperator) | **Post** /role/setEnterpriseUserManagementDelegatedToOperator | Grant enterprise user access to the network operator


# **RoleCreateRoleCustomization**
> RoleCreateRoleCustomizationResult RoleCreateRoleCustomization(ctx, body)
Create a role customization

Create a role customization given a roleId and an array of privilegeIds.  Privileges required:  `UPDATE` `NETWORK`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**RoleCreateRoleCustomization**](RoleCreateRoleCustomization.md)|  | 

### Return type

[**RoleCreateRoleCustomizationResult**](role_create_role_customization_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleDeleteRoleCustomization**
> RoleDeleteRoleCustomizationResult RoleDeleteRoleCustomization(ctx, body)
Delete a role customization

Delete a role customization, given a role customization name or forRoleId.  Privileges required:  `UPDATE` `NETWORK`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**RoleDeleteRoleCustomization**](RoleDeleteRoleCustomization.md)|  | 

### Return type

[**RoleDeleteRoleCustomizationResult**](role_delete_role_customization_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleGetUserTypeRoles**
> []RoleGetUserTypeRolesResultItem RoleGetUserTypeRoles(ctx, body)
Get the roles defined for a user type

Return the defined roles for a specified user type.  Privileges required:  `READ` `ENTERPRISE_USER`, or  `READ` `PROXY_USER`, or  `READ` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**RoleGetUserTypeRoles**](RoleGetUserTypeRoles.md)|  | 

### Return type

[**[]RoleGetUserTypeRolesResultItem**](role_get_user_type_roles_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleSetEnterpriseDelegatedToEnterpriseProxy**
> RoleSetEnterpriseDelegatedToEnterpriseProxyResult RoleSetEnterpriseDelegatedToEnterpriseProxy(ctx, body)
Grant enterprise access to partner

Grants enterprise access to the specified enterprise proxy (partner). When an enterprise is delegated to a proxy, proxy users are granted access to view, configure, and troubleshoot Edges owned by the enterprise. As a security consideration, proxy Support users cannot view personally identifiable information.  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body3**](Body3.md)|  | 

### Return type

[**RoleSetEnterpriseDelegatedToEnterpriseProxyResult**](role_set_enterprise_delegated_to_enterprise_proxy_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleSetEnterpriseDelegatedToOperator**
> RoleSetEnterpriseDelegatedToOperatorResult RoleSetEnterpriseDelegatedToOperator(ctx, body)
Grant enterprise access to network operator

Grants enterprise access to the network operator. When an enterprise is delegated to the operator, operator users are permitted to view, configure, and troubleshoot Edges owned by the enterprise. As a security consideration, operator users cannot view personally identifiable information.  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body4**](Body4.md)|  | 

### Return type

[**RoleSetEnterpriseDelegatedToOperatorResult**](role_set_enterprise_delegated_to_operator_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleSetEnterpriseProxyDelegatedToOperator**
> RoleSetEnterpriseProxyDelegatedToOperatorResult RoleSetEnterpriseProxyDelegatedToOperator(ctx, body)
Grant enterprise proxy access to network operator

Grants enterprise proxy access to the network operator. When an enterprise proxy is delegated to the operator, operator users are granted access to view, configure and troubleshoot objects owned by the proxy.  Privileges required:  `UPDATE` `ENTERPRISE_PROXY_DELEGATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body5**](Body5.md)|  | 

### Return type

[**RoleSetEnterpriseProxyDelegatedToOperatorResult**](role_set_enterprise_proxy_delegated_to_operator_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **RoleSetEnterpriseUserManagementDelegatedToOperator**
> RoleSetEnterpriseUserManagementDelegatedToOperatorResult RoleSetEnterpriseUserManagementDelegatedToOperator(ctx, body)
Grant enterprise user access to the network operator

When enterprise user management is delegated to the operator, operator users are granted enterprise-level user management capabilities (user creation, password resets, etc.).  Privileges required:  `UPDATE` `ENTERPRISE_DELEGATION`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**Body6**](Body6.md)|  | 

### Return type

[**RoleSetEnterpriseUserManagementDelegatedToOperatorResult**](role_set_enterprise_user_management_delegated_to_operator_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

