# RoleDeleteRoleCustomization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ForRoleId** | **int32** |  | [default to null]
**Name** | **string** |  | [optional] [default to null]
**UserId** | **int32** |  | [optional] [default to null]
**EnterpriseId** | **int32** |  | [optional] [default to null]
**OperatorId** | **int32** |  | [optional] [default to null]
**NetworkId** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


