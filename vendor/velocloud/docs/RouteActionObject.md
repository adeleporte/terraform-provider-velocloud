# RouteActionObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Interface_** | **string** |  | [optional] [default to null]
**LinkInternalLogicalId** | **string** |  | [optional] [default to null]
**LinkPolicy** | **string** |  | [optional] [default to null]
**RouteCfg** | [***interface{}**](interface{}.md) |  | [optional] [default to null]
**RoutePolicy** | **string** |  | [optional] [default to null]
**ServiceGroup** | **string** |  | [optional] [default to null]
**VlanId** | **int32** |  | [optional] [default to null]
**Wanlink** | **string** |  | [optional] [default to null]
**LinkCosLogicalId** | **string** |  | [optional] [default to null]
**LinkOuterDscpTag** | **string** |  | [optional] [default to null]
**LinkInnerDscpTag** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


