# SetClientDeviceHostName

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EnterpriseId** | **int32** |  | [default to null]
**ClientDeviceId** | **int32** |  | [optional] [default to null]
**HostName** | **string** |  | [default to null]
**MacAddress** | **string** |  | [optional] [default to null]
**IpAddress** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


