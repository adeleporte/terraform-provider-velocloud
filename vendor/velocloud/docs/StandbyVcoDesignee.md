# StandbyVcoDesignee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**StandbyAddress** | **string** |  | [default to null]
**StandbyReplicationAddress** | **string** |  | [optional] [default to null]
**StandbyUuid** | **string** |  | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


