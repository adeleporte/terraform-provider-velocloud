# SystemProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Created** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Value** | **string** |  | [optional] [default to null]
**DefaultValue** | **string** |  | [optional] [default to null]
**IsReadOnly** | **bool** |  | [optional] [default to null]
**IsPassword** | **bool** |  | [optional] [default to null]
**DataType** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Modified** | [**time.Time**](time.Time.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


