# \SystemPropertyApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SystemPropertyGetSystemProperties**](SystemPropertyApi.md#SystemPropertyGetSystemProperties) | **Post** /systemProperty/getSystemProperties | Get all system properties
[**SystemPropertyGetSystemProperty**](SystemPropertyApi.md#SystemPropertyGetSystemProperty) | **Post** /systemProperty/getSystemProperty | Get system property
[**SystemPropertyInsertOrUpdateSystemProperty**](SystemPropertyApi.md#SystemPropertyInsertOrUpdateSystemProperty) | **Post** /systemProperty/insertOrUpdateSystemProperty | Insert or update a system property
[**SystemPropertyInsertSystemProperty**](SystemPropertyApi.md#SystemPropertyInsertSystemProperty) | **Post** /systemProperty/insertSystemProperty | Insert a system property
[**SystemPropertyUpdateSystemProperty**](SystemPropertyApi.md#SystemPropertyUpdateSystemProperty) | **Post** /systemProperty/updateSystemProperty | Update a system property


# **SystemPropertyGetSystemProperties**
> []SystemPropertyGetSystemPropertiesResultItem SystemPropertyGetSystemProperties(ctx, body)
Get all system properties

Get a list of all configured system properties.  Privileges required:  `READ` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyGetSystemProperties**](SystemPropertyGetSystemProperties.md)|  | 

### Return type

[**[]SystemPropertyGetSystemPropertiesResultItem**](system_property_get_system_properties_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyGetSystemProperty**
> SystemPropertyGetSystemPropertyResult SystemPropertyGetSystemProperty(ctx, body)
Get system property

Get a system property by object id or other attribute.  Privileges required:  `READ` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyGetSystemProperty**](SystemPropertyGetSystemProperty.md)|  | 

### Return type

[**SystemPropertyGetSystemPropertyResult**](system_property_get_system_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyInsertOrUpdateSystemProperty**
> SystemPropertyInsertOrUpdateSystemPropertyResult SystemPropertyInsertOrUpdateSystemProperty(ctx, body)
Insert or update a system property

Insert a system property. If property with the given name already exists, the property will be updated.  Privileges required:  `CREATE` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyInsertOrUpdateSystemProperty**](SystemPropertyInsertOrUpdateSystemProperty.md)|  | 

### Return type

[**SystemPropertyInsertOrUpdateSystemPropertyResult**](system_property_insert_or_update_system_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyInsertSystemProperty**
> SystemPropertyInsertSystemPropertyResult SystemPropertyInsertSystemProperty(ctx, body)
Insert a system property

Insert a new system property.  Privileges required:  `CREATE` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyInsertSystemProperty**](SystemPropertyInsertSystemProperty.md)|  | 

### Return type

[**SystemPropertyInsertSystemPropertyResult**](system_property_insert_system_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SystemPropertyUpdateSystemProperty**
> SystemPropertyUpdateSystemPropertyResult SystemPropertyUpdateSystemProperty(ctx, body)
Update a system property

Update an existing system property, provided an object `id` or other identifying attributes  Privileges required:  `UPDATE` `SYSTEM_PROPERTY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**SystemPropertyUpdateSystemProperty**](SystemPropertyUpdateSystemProperty.md)|  | 

### Return type

[**SystemPropertyUpdateSystemPropertyResult**](system_property_update_system_property_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

