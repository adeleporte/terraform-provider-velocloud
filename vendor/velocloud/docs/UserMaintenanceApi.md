# \UserMaintenanceApi

All URIs are relative to *https://localhost/portal/rest*

Method | HTTP request | Description
------------- | ------------- | -------------
[**EnterpriseGetEnterpriseUsers**](UserMaintenanceApi.md#EnterpriseGetEnterpriseUsers) | **Post** /enterprise/getEnterpriseUsers | Get list of enterprise users by enterprise id
[**EnterpriseInsertEnterpriseUser**](UserMaintenanceApi.md#EnterpriseInsertEnterpriseUser) | **Post** /enterprise/insertEnterpriseUser | Insert an enterprise user
[**EnterpriseProxyDeleteEnterpriseProxyUser**](UserMaintenanceApi.md#EnterpriseProxyDeleteEnterpriseProxyUser) | **Post** /enterpriseProxy/deleteEnterpriseProxyUser | Delete an enterprise proxy admin user
[**EnterpriseProxyGetEnterpriseProxyUser**](UserMaintenanceApi.md#EnterpriseProxyGetEnterpriseProxyUser) | **Post** /enterpriseProxy/getEnterpriseProxyUser | Get an enterprise proxy user
[**EnterpriseProxyGetEnterpriseProxyUsers**](UserMaintenanceApi.md#EnterpriseProxyGetEnterpriseProxyUsers) | **Post** /enterpriseProxy/getEnterpriseProxyUsers | Get all enterprise proxy admin users
[**EnterpriseProxyInsertEnterpriseProxyUser**](UserMaintenanceApi.md#EnterpriseProxyInsertEnterpriseProxyUser) | **Post** /enterpriseProxy/insertEnterpriseProxyUser | Create a new partner admin user
[**EnterpriseProxyUpdateEnterpriseProxyUser**](UserMaintenanceApi.md#EnterpriseProxyUpdateEnterpriseProxyUser) | **Post** /enterpriseProxy/updateEnterpriseProxyUser | Update an enterprise proxy admin user
[**EnterpriseUserDeleteEnterpriseUser**](UserMaintenanceApi.md#EnterpriseUserDeleteEnterpriseUser) | **Post** /enterpriseUser/deleteEnterpriseUser | Delete an enterprise user.
[**EnterpriseUserGetEnterpriseUser**](UserMaintenanceApi.md#EnterpriseUserGetEnterpriseUser) | **Post** /enterpriseUser/getEnterpriseUser | Get an enterprise user
[**EnterpriseUserUpdateEnterpriseUser**](UserMaintenanceApi.md#EnterpriseUserUpdateEnterpriseUser) | **Post** /enterpriseUser/updateEnterpriseUser | Update an enterprise user
[**OperatorUserDeleteOperatorUser**](UserMaintenanceApi.md#OperatorUserDeleteOperatorUser) | **Post** /operatorUser/deleteOperatorUser | Delete an operator user
[**OperatorUserGetOperatorUser**](UserMaintenanceApi.md#OperatorUserGetOperatorUser) | **Post** /operatorUser/getOperatorUser | Get an operator user
[**OperatorUserInsertOperatorUser**](UserMaintenanceApi.md#OperatorUserInsertOperatorUser) | **Post** /operatorUser/insertOperatorUser | Insert an operator user
[**OperatorUserUpdateOperatorUser**](UserMaintenanceApi.md#OperatorUserUpdateOperatorUser) | **Post** /operatorUser/updateOperatorUser | Update an operator user


# **EnterpriseGetEnterpriseUsers**
> []EnterpriseGetEnterpriseUsersResultItem EnterpriseGetEnterpriseUsers(ctx, body)
Get list of enterprise users by enterprise id

undefined  Privileges required:  `READ` `ENTERPRISE`  `READ` `ENTERPRISE_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseGetEnterpriseUsers**](EnterpriseGetEnterpriseUsers.md)|  | 

### Return type

[**[]EnterpriseGetEnterpriseUsersResultItem**](enterprise_get_enterprise_users_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseInsertEnterpriseUser**
> EnterpriseInsertEnterpriseUserResult EnterpriseInsertEnterpriseUser(ctx, body)
Insert an enterprise user

Insert an enterprise user.  Privileges required:  `CREATE` `ENTERPRISE_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NewEnterpriseUser**](NewEnterpriseUser.md)|  | 

### Return type

[**EnterpriseInsertEnterpriseUserResult**](enterprise_insert_enterprise_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyDeleteEnterpriseProxyUser**
> EnterpriseProxyDeleteEnterpriseProxyUserResult EnterpriseProxyDeleteEnterpriseProxyUser(ctx, body)
Delete an enterprise proxy admin user

Delete an enterprise proxy user by id or username. Note that `enterpriseProxyId` is a required parameter when invoking this method as an operator or partner user.  Privileges required:  `DELETE` `PROXY_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyDeleteEnterpriseProxyUser**](EnterpriseProxyDeleteEnterpriseProxyUser.md)|  | 

### Return type

[**EnterpriseProxyDeleteEnterpriseProxyUserResult**](enterprise_proxy_delete_enterprise_proxy_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyUser**
> EnterpriseProxyGetEnterpriseProxyUser EnterpriseProxyGetEnterpriseProxyUser(ctx, body)
Get an enterprise proxy user

Get an enterprise proxy user by id or username.  Privileges required:  `READ` `PROXY_USER`  `READ` `PROXY`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyUser**](EnterpriseProxyGetEnterpriseProxyUser.md)|  | 

### Return type

[**EnterpriseProxyGetEnterpriseProxyUser**](enterprise_proxy_get_enterprise_proxy_user.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyGetEnterpriseProxyUsers**
> []EnterpriseProxyGetEnterpriseProxyUsersResultItem EnterpriseProxyGetEnterpriseProxyUsers(ctx, body)
Get all enterprise proxy admin users

undefined  Privileges required:  `READ` `ENTERPRISE`  `READ` `PROXY_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyGetEnterpriseProxyUsers**](EnterpriseProxyGetEnterpriseProxyUsers.md)|  | 

### Return type

[**[]EnterpriseProxyGetEnterpriseProxyUsersResultItem**](enterprise_proxy_get_enterprise_proxy_users_result_item.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyInsertEnterpriseProxyUser**
> EnterpriseProxyInsertEnterpriseProxyUserResult EnterpriseProxyInsertEnterpriseProxyUser(ctx, body)
Create a new partner admin user

Create a new partner admin user  Privileges required:  `CREATE` `PROXY_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**NewEnterpriseProxyUser**](NewEnterpriseProxyUser.md)|  | 

### Return type

[**EnterpriseProxyInsertEnterpriseProxyUserResult**](enterprise_proxy_insert_enterprise_proxy_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseProxyUpdateEnterpriseProxyUser**
> EnterpriseProxyUpdateEnterpriseProxyUserResult EnterpriseProxyUpdateEnterpriseProxyUser(ctx, body)
Update an enterprise proxy admin user

Update an enterprise proxy admin user  Privileges required:  `UPDATE` `PROXY_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseProxyUpdateEnterpriseProxyUser**](EnterpriseProxyUpdateEnterpriseProxyUser.md)|  | 

### Return type

[**EnterpriseProxyUpdateEnterpriseProxyUserResult**](enterprise_proxy_update_enterprise_proxy_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUserDeleteEnterpriseUser**
> EnterpriseUserDeleteEnterpriseUserResult EnterpriseUserDeleteEnterpriseUser(ctx, body)
Delete an enterprise user.

Delete an enterprise user by id or username. Note that `enterpriseId` is a required parameter when invoking this method as an operator or partner user.  Privileges required:  `DELETE` `ENTERPRISE_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUserDeleteEnterpriseUser**](EnterpriseUserDeleteEnterpriseUser.md)|  | 

### Return type

[**EnterpriseUserDeleteEnterpriseUserResult**](enterprise_user_delete_enterprise_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUserGetEnterpriseUser**
> EnterpriseUserGetEnterpriseUserResult EnterpriseUserGetEnterpriseUser(ctx, body)
Get an enterprise user

Get an enterprise user by id or username.  Privileges required:  `READ` `ENTERPRISE_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUserGetEnterpriseUser**](EnterpriseUserGetEnterpriseUser.md)|  | 

### Return type

[**EnterpriseUserGetEnterpriseUserResult**](enterprise_user_get_enterprise_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **EnterpriseUserUpdateEnterpriseUser**
> EnterpriseUserUpdateEnterpriseUserResult EnterpriseUserUpdateEnterpriseUser(ctx, body)
Update an enterprise user

Update an enterprise user provided an object `id` or other identifying attributes, and an `_update` object with the names and values of columns to be updated.  Privileges required:  `UPDATE` `ENTERPRISE_USER`, or  `UPDATE` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**EnterpriseUserUpdateEnterpriseUser**](EnterpriseUserUpdateEnterpriseUser.md)|  | 

### Return type

[**EnterpriseUserUpdateEnterpriseUserResult**](enterprise_user_update_enterprise_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **OperatorUserDeleteOperatorUser**
> OperatorUserDeleteOperatorUserResult OperatorUserDeleteOperatorUser(ctx, body)
Delete an operator user

Delete an operator user object by `id` or `username`.  Privileges required:  `DELETE` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**OperatorUserDeleteOperatorUser**](OperatorUserDeleteOperatorUser.md)|  | 

### Return type

[**OperatorUserDeleteOperatorUserResult**](operator_user_delete_operator_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **OperatorUserGetOperatorUser**
> OperatorUserGetOperatorUserResult OperatorUserGetOperatorUser(ctx, body)
Get an operator user

Get an operator user object by `id` or `username`.  Privileges required:  `READ` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**OperatorUserGetOperatorUser**](OperatorUserGetOperatorUser.md)|  | 

### Return type

[**OperatorUserGetOperatorUserResult**](operator_user_get_operator_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **OperatorUserInsertOperatorUser**
> OperatorUserGetOperatorUserResult OperatorUserInsertOperatorUser(ctx, body)
Insert an operator user

Insert an operator user and associate with an operator's network.  Privileges required:  `CREATE` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**OperatorUserInsertOperatorUser**](OperatorUserInsertOperatorUser.md)|  | 

### Return type

[**OperatorUserGetOperatorUserResult**](operator_user_get_operator_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **OperatorUserUpdateOperatorUser**
> OperatorUserUpdateOperatorUserResult OperatorUserUpdateOperatorUser(ctx, body)
Update an operator user

Update an operator user provided an object `id` or `username`, and an `_update` object containing the names and values, of columns to be updated.  Privileges required:  `UPDATE` `OPERATOR_USER`

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for logging, tracing, authentication, etc.
  **body** | [**OperatorUserUpdateOperatorUser**](OperatorUserUpdateOperatorUser.md)|  | 

### Return type

[**OperatorUserUpdateOperatorUserResult**](operator_user_update_operator_user_result.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

