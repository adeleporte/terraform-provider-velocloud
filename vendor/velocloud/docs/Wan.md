# Wan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Name** | **string** |  | [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**ConfigurationId** | **int32** |  | [optional] [default to null]
**Data** | [***WanData**](WAN_data.md) |  | [optional] [default to null]
**Refs** | [***GenericRefs**](genericRefs.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


