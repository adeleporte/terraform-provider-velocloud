# WanData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**[]WanDataLinks**](WAN_data_links.md) |  | [optional] [default to null]
**Networks** | [**[]WanDataNetworks**](WAN_data_networks.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


