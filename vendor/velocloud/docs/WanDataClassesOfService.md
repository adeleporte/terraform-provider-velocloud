# WanDataClassesOfService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ClassId** | **int32** |  | [optional] [default to null]
**ClassesOfService** | [**[]WanDataClassesOfServiceClassesOfService**](WAN_data_classesOfService_classesOfService.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


