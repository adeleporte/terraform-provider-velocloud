# WanDataClassesOfServiceClassesOfService

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**DscpTags** | **[]string** |  | [optional] [default to null]
**StaticSLA** | [***WanDataClassesOfServiceStaticSla**](WAN_data_classesOfService_staticSLA.md) |  | [optional] [default to null]
**BandwidthPct** | **int32** |  | [optional] [default to null]
**BandwidthGuaranteed** | **bool** |  | [optional] [default to null]
**DefaultClassOfService** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


