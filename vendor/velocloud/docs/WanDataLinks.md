# WanDataLinks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LogicalId** | **string** |  | [optional] [default to null]
**InternalId** | **string** |  | [optional] [default to null]
**Discovery** | **string** |  | [optional] [default to null]
**Mode** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Isp** | **string** |  | [optional] [default to null]
**PublicIpAddress** | **string** |  | [optional] [default to null]
**SourceIpAddress** | **string** |  | [optional] [default to null]
**NextHopIpAddress** | **string** |  | [optional] [default to null]
**CustomVlanId** | **bool** |  | [optional] [default to null]
**VlanId** | **int32** |  | [optional] [default to null]
**VirtualIpAddress** | **string** |  | [optional] [default to null]
**DynamicBwAdjustmentEnabled** | **bool** |  | [optional] [default to null]
**BwMeasurement** | **string** |  | [optional] [default to null]
**UpstreamMbps** | **string** |  | [optional] [default to null]
**DownstreamMbps** | **string** |  | [optional] [default to null]
**BackupOnly** | **bool** |  | [optional] [default to null]
**OverheadBytes** | **int32** |  | [optional] [default to null]
**UdpHolePunching** | **bool** |  | [optional] [default to null]
**MTU** | **int32** |  | [optional] [default to null]
**MplsNetwork** | **string** |  | [optional] [default to null]
**DscpTag** | **string** |  | [optional] [default to null]
**StaticSlaEnabled** | **bool** |  | [optional] [default to null]
**ClassesofServiceEnabled** | **bool** |  | [optional] [default to null]
**EncryptOverlay** | **bool** |  | [optional] [default to null]
**StaticSLA** | [***WanDataStaticSla**](WAN_data_staticSLA.md) |  | [optional] [default to null]
**ClassesOfService** | [***WanDataClassesOfService**](WAN_data_classesOfService.md) |  | [optional] [default to null]
**Interfaces** | **[]string** |  | [optional] [default to null]
**LastActive** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


