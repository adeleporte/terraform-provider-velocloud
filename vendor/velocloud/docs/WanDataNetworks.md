# WanDataNetworks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Mode** | **string** |  | [optional] [default to null]
**Type_** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**LogicalId** | **string** |  | [optional] [default to null]
**Interface_** | **string** |  | [optional] [default to null]
**InternalId** | **string** |  | [optional] [default to null]
**IpAddress** | **string** |  | [optional] [default to null]
**Isp** | **string** |  | [optional] [default to null]
**LastActive** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


