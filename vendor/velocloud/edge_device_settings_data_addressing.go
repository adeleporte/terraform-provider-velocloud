/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EdgeDeviceSettingsDataAddressing struct {

	Type_ string `json:"type,omitempty"`

	CidrPrefix int32 `json:"cidrPrefix,omitempty"`

	CidrIp string `json:"cidrIp,omitempty"`

	Netmask string `json:"netmask,omitempty"`

	Gateway string `json:"gateway,omitempty"`

	Username string `json:"username,omitempty"`

	Password string `json:"password,omitempty"`
}
