/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger


type EdgeOspf struct {
	Area string `json:"area"`

	Enabled bool `json:"enabled"`

	PassiveInterface bool `json:"passiveInterface"`
}

type EdgeDeviceSettingsDataLanNetworks struct {
	Space string `json:"space,omitempty"`

	Guest bool `json:"guest,omitempty"`

	Secure bool `json:"secure,omitempty"`

	Advertise bool `json:"advertise,omitempty"`

	Cost int32 `json:"cost,omitempty"`

	Dhcp *EdgeDeviceSettingsDataLanDhcp `json:"dhcp,omitempty"`

	StaticReserved int32 `json:"staticReserved,omitempty"`

	Netmask string `json:"netmask,omitempty"`

	CidrPrefix int32 `json:"cidrPrefix,omitempty"`

	CidrIp string `json:"cidrIp,omitempty"`

	BaseDhcpAddr int32 `json:"baseDhcpAddr,omitempty"`

	NumDhcpAddr int32 `json:"numDhcpAddr,omitempty"`

	Name string `json:"name,omitempty"`

	Interfaces []string `json:"interfaces,omitempty"`

	VlanId int32 `json:"vlanId,omitempty"`

	ManagementIp string `json:"managementIp,omitempty"`

	Disabled bool `json:"disabled,omitempty"`

	SegmentId int `json:"segmentId"`

	Ospf EdgeOspf `json:"ospf,omitempty"`
}
