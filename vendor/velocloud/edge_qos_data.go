/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type Scheduler struct {
	Bandwidth       int32  `json:"bandwidth"`
	BandwidthCapPct int32  `json:"bandwidthCapPct"`
	Burst           int32  `json:"burst"`
	Latency         int32  `json:"latency"`
	Priority        string `json:"priority"`
	QueueLen        int32  `json:"queueLen"`
}

type QoS struct {
	RxScheduler Scheduler `json:"rxScheduler"`

	TxScheduler Scheduler `json:"txScheduler"`

	Type string `json:"type"`
}

type EdgeQosDataSegment struct {
	Name string `json:"name,omitempty"`

	SegmentId int32 `json:"segmentId"`

	SegmentLogicalId string `json:"segmentLogicalId,omitempty"`

	Type string `json:"type,omitempty"`
}

type EdgeQosData struct {
	Rules []EdgeQosDataRules `json:"rules,omitempty"`

	Defaults []interface{} `json:"defaults,omitempty"`

	WebProxy *EdgeQosDataWebProxy `json:"webProxy,omitempty"`

	ServiceRateLimit *EdgeQosDataServiceRateLimit `json:"serviceRateLimit,omitempty"`

	CosMapping *EdgeQosDataCosMapping `json:"cosMapping,omitempty"`

	Segment EdgeQosDataSegment `json:"segment,omitempty"`
}
