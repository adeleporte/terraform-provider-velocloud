/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EdgeQosDataServiceRateLimit struct {

	Enabled bool `json:"enabled,omitempty"`

	InputType string `json:"inputType,omitempty"`

	Value int32 `json:"value,omitempty"`
}
