/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EdgeSetEdgeEnterpriseConfigurationResult struct {

	// The id of the newly-created object.
	Id int32 `json:"id,omitempty"`

	// The number of rows modified
	Rows int32 `json:"rows"`

	// An error message explaining why the method failed
	Error_ string `json:"error,omitempty"`
}
