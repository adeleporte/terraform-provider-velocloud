/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EdgeSetEdgeOperatorConfiguration struct {

	EdgeId int32 `json:"edgeId"`

	EnterpriseId int32 `json:"enterpriseId,omitempty"`

	ConfigurationId int32 `json:"configurationId"`
}
