/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EnterpriseGetEnterpriseEdges struct {

	Id int32 `json:"id,omitempty"`

	EnterpriseId int32 `json:"enterpriseId,omitempty"`

	With []string `json:"with,omitempty"`
}
