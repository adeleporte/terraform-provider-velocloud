/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EnterpriseGetEnterpriseEdgesResultItem struct {

	ActivationKey string `json:"activationKey,omitempty"`

	ActivationKeyExpires string `json:"activationKeyExpires,omitempty"`

	ActivationState string `json:"activationState,omitempty"`

	ActivationTime string `json:"activationTime,omitempty"`

	AlertsEnabled int32 `json:"alertsEnabled,omitempty"`

	BuildNumber string `json:"buildNumber,omitempty"`

	Created string `json:"created,omitempty"`

	Description string `json:"description,omitempty"`

	DeviceFamily string `json:"deviceFamily,omitempty"`

	DeviceId string `json:"deviceId,omitempty"`

	DnsName string `json:"dnsName,omitempty"`

	EdgeHardwareId string `json:"edgeHardwareId,omitempty"`

	EdgeState string `json:"edgeState,omitempty"`

	EdgeStateTime string `json:"edgeStateTime,omitempty"`

	EndpointPkiMode string `json:"endpointPkiMode,omitempty"`

	EnterpriseId int32 `json:"enterpriseId,omitempty"`

	HaLastContact string `json:"haLastContact,omitempty"`

	HaPreviousState string `json:"haPreviousState,omitempty"`

	HaSerialNumber string `json:"haSerialNumber,omitempty"`

	HaState string `json:"haState,omitempty"`

	Id int32 `json:"id,omitempty"`

	IsLive int32 `json:"isLive,omitempty"`

	LastContact string `json:"lastContact,omitempty"`

	LogicalId string `json:"logicalId,omitempty"`

	ModelNumber string `json:"modelNumber,omitempty"`

	Modified string `json:"modified,omitempty"`

	Name string `json:"name,omitempty"`

	OperatorAlertsEnabled int32 `json:"operatorAlertsEnabled,omitempty"`

	SelfMacAddress string `json:"selfMacAddress,omitempty"`

	SerialNumber string `json:"serialNumber,omitempty"`

	ServiceState string `json:"serviceState,omitempty"`

	ServiceUpSince string `json:"serviceUpSince,omitempty"`

	SiteId int32 `json:"siteId,omitempty"`

	SoftwareUpdated string `json:"softwareUpdated,omitempty"`

	SoftwareVersion string `json:"softwareVersion,omitempty"`

	SystemUpSince string `json:"systemUpSince,omitempty"`

	Configuration *EnterpriseGetEnterpriseEdgesResultItemConfiguration `json:"configuration,omitempty"`

	Links []Link `json:"links,omitempty"`

	RecentLinks []Link `json:"recentLinks,omitempty"`

	Site *Site `json:"site,omitempty"`
}
