/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EnterpriseServiceNetworkDataVlans struct {

	Name string `json:"name,omitempty"`

	VlanId int32 `json:"vlanId,omitempty"`

	Advertise bool `json:"advertise,omitempty"`

	Cost int32 `json:"cost,omitempty"`

	StaticReserved int32 `json:"staticReserved,omitempty"`

	Dhcp *EnterpriseServiceNetworkDataDhcp `json:"dhcp,omitempty"`
}
