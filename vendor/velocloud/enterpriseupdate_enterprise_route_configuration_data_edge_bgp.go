/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EnterpriseupdateEnterpriseRouteConfigurationDataEdgeBgp struct {

	AdvertiseInternal bool `json:"advertiseInternal"`

	AdvertiseExternal bool `json:"advertiseExternal"`

	AdvertiseNeighborTags *EnterpriseupdateEnterpriseRouteConfigurationDataEdgeBgpAdvertiseNeighborTags `json:"advertiseNeighborTags"`
}
