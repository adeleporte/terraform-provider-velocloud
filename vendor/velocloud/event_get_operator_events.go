/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type EventGetOperatorEvents struct {

	NetworkId int32 `json:"networkId,omitempty"`

	Interval *Interval `json:"interval,omitempty"`

	Filter *EnterprisegetEnterpriseAlertsFilter `json:"filter,omitempty"`

	GatewayId []int32 `json:"gatewayId,omitempty"`
}
