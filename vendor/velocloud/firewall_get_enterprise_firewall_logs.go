/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type FirewallGetEnterpriseFirewallLogs struct {

	EnterpriseId int32 `json:"enterpriseId,omitempty"`

	Interval *Interval `json:"interval,omitempty"`

	Filter *EnterprisegetEnterpriseAlertsFilter `json:"filter,omitempty"`

	Rules []string `json:"rules,omitempty"`

	SourceIps []string `json:"sourceIps,omitempty"`

	DestIps []string `json:"destIps,omitempty"`

	Edges []int32 `json:"edges,omitempty"`

	With []string `json:"with,omitempty"`
}
