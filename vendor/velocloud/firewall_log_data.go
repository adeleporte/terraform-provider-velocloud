/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

import (
	"time"
)

type FirewallLogData struct {

	Id int32 `json:"id,omitempty"`

	EdgeId int32 `json:"edgeId,omitempty"`

	Timestamp time.Time `json:"timestamp,omitempty"`

	Allow int32 `json:"allow,omitempty"`

	RuleLogicalId string `json:"ruleLogicalId,omitempty"`

	Interface_ string `json:"interface,omitempty"`

	Protocol string `json:"protocol,omitempty"`

	SourceIp string `json:"sourceIp,omitempty"`

	SourcePort int32 `json:"sourcePort,omitempty"`

	DestIp string `json:"destIp,omitempty"`

	DestPort int32 `json:"destPort,omitempty"`

	IcmpType string `json:"icmpType,omitempty"`

	Length int32 `json:"length,omitempty"`
}
