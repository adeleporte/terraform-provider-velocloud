/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type FirewallRuleMatch struct {

	// Integer ID corresponding to an application in the network-level application map
	Appid int32 `json:"appid"`

	// Integer ID corresponding to an application class in the network-level application map
	Classid int32 `json:"classid"`

	// Integer ID indicating DSCP classification
	Dscp int32 `json:"dscp"`

	// Source IP address
	Sip string `json:"sip"`

	// Upper bound of a source port range
	SportHigh int32 `json:"sport_high"`

	// Lower bound of a source port range
	SportLow int32 `json:"sport_low"`

	// Source subnet mask, e.g. 255.255.255.0
	Ssm string `json:"ssm"`

	// Source MAC address
	Smac string `json:"smac"`

	// Integer ID for the source VLAN
	Svlan int32 `json:"svlan"`

	OsVersion int32 `json:"os_version"`

	Hostname string `json:"hostname"`

	// Destination IP address
	Dip string `json:"dip"`

	// Lower bound of a destination port range
	DportLow int32 `json:"dport_low"`

	// Upper bound of a destination port range
	DportHigh int32 `json:"dport_high"`

	// Destination subnet mask e.g. 255.255.255.0
	Dsm string `json:"dsm"`

	// Destination MAC address
	Dmac string `json:"dmac"`

	// Integer ID for the destination VLAN
	Dvlan int32 `json:"dvlan"`

	// Integer ID corresponding to a protocol
	Proto int32 `json:"proto"`
}
