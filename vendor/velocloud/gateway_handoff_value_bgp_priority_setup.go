/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type GatewayHandoffValueBgpPrioritySetup struct {

	AutoAs *GatewayHandoffValueBgpPrioritySetupAutoAs `json:"autoAs,omitempty"`

	AutoMed *GatewayHandoffValueBgpPrioritySetupAutoAs `json:"autoMed,omitempty"`

	CommunityMapping *GatewayHandoffValueBgpPrioritySetupCommunityMapping `json:"communityMapping,omitempty"`
}
