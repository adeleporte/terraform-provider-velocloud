/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type GatewayUpdateGatewayAttributes struct {

	Id int32 `json:"id"`

	Name string `json:"name,omitempty"`

	Description string `json:"description,omitempty"`

	Site *SiteObject `json:"site,omitempty"`

	IpsecGatewayDetail *GatewayupdateGatewayAttributesIpsecGatewayDetail `json:"ipsecGatewayDetail,omitempty"`

	HandOffDetail *GatewayHandoffDetail `json:"handOffDetail,omitempty"`

	Roles *GatewayRolesObject `json:"roles,omitempty"`
}
