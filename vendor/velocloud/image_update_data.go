/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type ImageUpdateData struct {

	BuildNumber string `json:"buildNumber,omitempty"`

	ProfileDeviceFamily string `json:"profileDeviceFamily,omitempty"`

	ProfileVersion string `json:"profileVersion,omitempty"`

	SoftwarePackageId int32 `json:"softwarePackageId,omitempty"`

	SoftwarePackageName string `json:"softwarePackageName,omitempty"`

	Version string `json:"version,omitempty"`

	WindowDurationMins int32 `json:"windowDurationMins,omitempty"`

	Windowed bool `json:"windowed,omitempty"`
}
