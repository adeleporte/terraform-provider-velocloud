/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type MonitoringGetEnterpriseEdgeVnfStatusResultItemVnfStatus struct {

	Description string `json:"description,omitempty"`

	Status string `json:"status,omitempty"`

	VmStatus string `json:"vmStatus,omitempty"`

	VnfInsertionEnabled string `json:"vnfInsertionEnabled,omitempty"`
}
