/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

type NetworkUpdateNetworkGatewayPoolAttributes struct {

	NetworkId int32 `json:"networkId,omitempty"`

	EnterpriseProxyId int32 `json:"enterpriseProxyId,omitempty"`

	// alias for `gatewayPoolId`
	Id int32 `json:"id"`

	GatewayPoolId int32 `json:"gatewayPoolId,omitempty"`

	Name string `json:"name,omitempty"`

	Description string `json:"description,omitempty"`

	HandOffType string `json:"handOffType,omitempty"`
}
