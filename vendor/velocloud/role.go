/*
 * Velocloud API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * API version: 3.2.20
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

import (
	"time"
)

type Role struct {

	Id int32 `json:"id,omitempty"`

	Created time.Time `json:"created,omitempty"`

	OperatorId int32 `json:"operatorId,omitempty"`

	NetworkId int32 `json:"networkId,omitempty"`

	EnterpriseId int32 `json:"enterpriseId,omitempty"`

	EnterpriseProxyId int32 `json:"enterpriseProxyId,omitempty"`

	Name string `json:"name,omitempty"`

	UserType string `json:"userType,omitempty"`

	FromUserType string `json:"fromUserType,omitempty"`

	IsSuper int32 `json:"isSuper,omitempty"`

	Description string `json:"description,omitempty"`

	Precedence int32 `json:"precedence,omitempty"`

	Modified time.Time `json:"modified,omitempty"`
}
